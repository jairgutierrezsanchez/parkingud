<?php
require 'persistencia/TransporteEnParqueaderoDAO.php';
require_once 'persistencia/Conexion.php';

class TransporteEnParqueadero {
    private $idtransportesEnParqueadero;
    private $idTransporte;
    private $idParqueadero;
    private $idHistorialTransporte;
    private $transporteEnParqueaderoDAO;
    private $conexion;	
   

    public function setIdTransporte($idTransporte)
    {
        $this->idTransporte = $idTransporte;
    }
    public function getIdParqueadero(){
        return $this->idParqueadero;
    }

    function TransporteEnParqueadero($idtransportesEnParqueadero= "" , $idTransporte= "", $idParqueadero= "",$idHistorialTransporte=""){
        $this -> idtransportesEnParqueadero = $idtransportesEnParqueadero;
        $this -> idTransporte = $idTransporte;
        $this -> idParqueadero = $idParqueadero;
        $this -> idHistorialTransporte = $idHistorialTransporte;
        $this -> conexion = new Conexion();
        $this -> transporteEnParqueaderoDAO = new TransporteEnParqueaderoDAO($idtransportesEnParqueadero , $idTransporte, $idParqueadero,$idHistorialTransporte);

    }

    function registrar($idTransporte,$parqueadero,$idHistorialParqueadero){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteEnParqueaderoDAO -> registrar($idTransporte,$parqueadero,$idHistorialParqueadero));
        $this -> conexion -> cerrar();
    }
    function expulsar($idTransporte,$parqueadero,$idHistorialParqueadero){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteEnParqueaderoDAO -> expulsar($idTransporte,$parqueadero,$idHistorialParqueadero));
        $this -> conexion -> cerrar();
    }
    function consultarParqueadero(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteEnParqueaderoDAO -> consultarExistenciaParqueadero());
        if($this -> conexion -> numFilas()!=0){
            $resultado = $this -> conexion -> extraer();
            $this -> idParqueadero = $resultado[0];
            return true;
        }else {
            return false;
        }
        $this -> conexion -> cerrar();
    }

    
    
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteEnParqueaderoDAO -> consultarExistenciaParqueadero());
        if($this -> conexion -> numFilas()!=0){
            $resultado = $this -> conexion -> extraer();
            $this -> idParqueadero = $resultado[0];
            return true;
        }else {
            return false;
        }
        $this -> conexion -> cerrar();
    }
 
        
}
    

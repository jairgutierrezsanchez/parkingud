<?php
require 'persistencia/ProyectoDAO.php';
require_once 'persistencia/Conexion.php';

class Proyecto {
    private $id;
    private $nombre;
    private $idFacultad;
    private $proyectoDAO;
    private $conexion;
    
    public function getId()
    {
        return $this->id;
    }
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getIdFacultad()
    {
        return $this->idFacultad;
    }

    public function getProyectoDAO()
    {
        return $this->proyectoDAO;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    function Proyecto($id="", $nombre="", $idFacultad=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> idFacultad = $idFacultad;
        $this -> conexion = new Conexion();
        $this -> proyectoDAO = new ProyectoDAO($id, $nombre, $idFacultad);        
    
    }
    
    function  consultar($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultar($id));
        $resultado = $this -> conexion -> extraer();
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> idFacultad = $resultado[2];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodosPorFacultad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarTodosPorFacultad());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Proyecto($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    public function consultarUsuariosPorProyecto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proyectoDAO -> consultarUsuariosPorProyecto());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}
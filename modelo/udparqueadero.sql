-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2023 a las 00:09:17
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `udparqueadero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `foto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`idadmin`, `nombre`, `apellido`, `correo`, `password`, `foto`) VALUES
(1, 'Jorge Enrique', 'Otalvaro Guzman', '123@123.com', '202cb962ac59075b964b07152d234b70', 'imgFotosCelador/202301170955281.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `celador`
--

CREATE TABLE `celador` (
  `idcelador` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `numeroID` int(15) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idFacultad` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `celador`
--

INSERT INTO `celador` (`idcelador`, `nombre`, `apellido`, `correo`, `password`, `fechaNacimiento`, `direccion`, `telefono`, `numeroID`, `foto`, `estado`, `idTipoIdentificacion`, `idFacultad`, `idGenero`) VALUES
(1, 'Sin', 'Asignar', 'saCelador@saCelador.com', '39ebcd5e8e67465259163994a86274dc', '2010-09-12', 'sin asignar', '00', 0, NULL, 0, 1, 1, 1),
(2, 'Peter Pedro', 'Parker Parque', 'pp@pp.com', 'c483f6ce851c9ecd9fb835ff7551737c', '1955-07-13', 'Calle 30', '3123123123', 1036456979, 'imgFotosCelador/202301160853021.png', 0, 3, 1, 3),
(3, 'Diego', 'Amaru', 'da@da.com', '5ca2aa845c8cd5ace6b016841f100d82', '2010-09-03', 'Diagonal 101 sur 2b 56', '3124544005', 1976005646, '', 0, 3, 1, 2),
(4, 'Manuela', 'Lopez', 'ml@ml.com', '9830e1f81f623b33106acc186b93374e', '1989-07-27', 'Diagonal 101 sur 2b 56', '3124544005', 1434235345, NULL, 0, 3, 1, 3),
(5, 'Mayers', 'Sanchez', 'ms@ms.com', 'ee33e909372d935d190f4fcb2a92d542', '2010-09-12', 'HOLA', '3025444788', 1233132132, NULL, 0, 3, 1, 3),
(10, 'Hola', 'Como ', 'as@asd.com', '7815696ecbf1c96e6894b779456d330e', '2022-12-09', 'Calle 80', '3178799656', 1236969797, NULL, 1, 3, 1, 4),
(11, 'Joji', 'Joji', '', 'd41d8cd98f00b204e9800998ecf8427e', '1959-11-04', 'Carrera Japan', '3129899949', 1036979965, NULL, 0, 3, 1, 3),
(13, 'lil', 'peep', 'lil@lil.com', '82d35f9b891c987a8082b2a18f2e00fe', '2022-12-16', NULL, NULL, NULL, NULL, 0, 1, 1, 3),
(14, 'vinicius', 'junior', 'vj@vj.com', '8af70c5f31d956a7f0ae021529a11728', '2011-06-09', 'brasil', '3125448878', 1399989898, NULL, 0, 3, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE `color` (
  `idcolor` int(11) NOT NULL,
  `color` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`idcolor`, `color`) VALUES
(1, 'Sin Asignar'),
(2, 'Rojo'),
(3, 'Negro'),
(4, 'Café'),
(5, 'Azul '),
(6, 'Amarillo'),
(7, 'Verde'),
(8, 'Morado'),
(9, 'Rosado'),
(12, 'Platinado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `idfacultad` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`idfacultad`, `nombre`) VALUES
(1, 'Sin Asignar'),
(2, 'Facultad Ciencia y Educación'),
(3, 'Facultad Ingeniería'),
(4, 'Facultad Tecnológica'),
(5, 'Facultad de Artes ASAB'),
(6, 'Facultad de Medio Ambiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idGenero` int(3) NOT NULL,
  `nombreGenero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idGenero`, `nombreGenero`) VALUES
(1, 'Sin asignar'),
(2, 'Feménino'),
(3, 'Masculino'),
(4, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialparqueadero`
--

CREATE TABLE `historialparqueadero` (
  `idhistorialParqueadero` int(11) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `horaIngreso` time NOT NULL,
  `fechaSalida` date DEFAULT NULL,
  `horaSalida` time DEFAULT NULL,
  `idTransporte` int(11) NOT NULL,
  `idParqueadero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `historialparqueadero`
--

INSERT INTO `historialparqueadero` (`idhistorialParqueadero`, `fechaIngreso`, `horaIngreso`, `fechaSalida`, `horaSalida`, `idTransporte`, `idParqueadero`) VALUES
(1, '2022-08-01', '08:36:25', '2022-08-01', '19:36:25', 1, 1),
(40, '2022-08-29', '21:28:39', '2022-08-29', '21:28:48', 2, 2),
(41, '2022-08-29', '21:29:05', '2022-08-29', '21:29:15', 2, 2),
(42, '2022-08-29', '21:29:30', '2022-09-26', '21:12:19', 2, 2),
(44, '2022-09-26', '21:11:19', '2022-09-26', '21:12:19', 2, 2),
(45, '2022-09-26', '21:11:36', '2022-09-26', '21:24:08', 3, 6),
(47, '2022-09-26', '21:29:15', '2022-09-26', '21:48:12', 12, 6),
(48, '2022-09-26', '21:41:39', '2022-09-26', '21:48:12', 12, 6),
(50, '2022-10-03', '17:24:19', '2022-10-21', '19:41:01', 2, 2),
(51, '2022-10-21', '19:41:15', '2022-10-25', '09:46:15', 2, 2),
(52, '2022-10-25', '08:14:04', '2022-10-25', '09:46:15', 2, 2),
(53, '2022-10-25', '19:43:53', '2022-10-25', '19:44:41', 2, 17),
(55, '2022-12-05', '21:28:47', '2023-01-16', '14:05:52', 14, 2),
(56, '2022-12-07', '13:55:46', '2023-01-06', '19:26:15', 10, 2),
(57, '2022-12-21', '21:23:19', '2022-12-21', '21:26:11', 16, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`, `idTipo`) VALUES
(1, 'Sin Asignar', 1),
(2, 'Benotto', 2),
(3, 'GW Bicycle', 2),
(4, 'Pulsar', 3),
(5, 'Yamaha', 3),
(6, 'KTM', 3),
(7, 'AKT', 3),
(8, 'Domitador', 3),
(9, 'Bajaj', 3),
(10, 'Benelli', 3),
(11, 'Ducati', 3),
(12, 'BMW', 3),
(13, 'Hero', 3),
(14, 'Honda', 3),
(15, 'Kymco', 3),
(16, 'Royal Enfield', 3),
(17, 'Suzuki', 3),
(18, 'Shimano', 2),
(19, 'Scott', 2),
(20, 'Venzo', 2),
(21, 'Ontrail', 2),
(22, 'Specialized', 2),
(23, 'Cannondale', 2),
(24, 'Canyon', 2),
(25, 'Leader', 2),
(26, '2Uno', 2),
(27, 'Skream', 2),
(28, 'Innova', 2),
(29, 'Optimus', 2),
(30, 'Orbea', 2),
(31, 'Pinarello', 2),
(32, 'Trek', 2),
(33, 'Urban', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parqueadero`
--

CREATE TABLE `parqueadero` (
  `idparqueadero` int(11) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `estado` int(11) NOT NULL,
  `puestosOcupados` int(11) NOT NULL,
  `puestosMaximos` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `parqueadero`
--

INSERT INTO `parqueadero` (`idparqueadero`, `numero`, `estado`, `puestosOcupados`, `puestosMaximos`, `idTipo`) VALUES
(1, 'Sin Asignar', 0, 0, 0, 1),
(2, 'Parqueadero A', 1, 0, 51, 2),
(5, 'Parqueadero Motos A', 1, 0, 3, 3),
(6, 'Parqueadero Motos B', 1, 0, 3, 3),
(10, 'Parqueadero Moto Prueba', 1, 0, 100, 3),
(11, '1', 1, 0, 200, 2),
(12, '2', 1, 0, 100, 2),
(13, '3', 1, 0, 21, 2),
(14, '4', 1, 0, 12, 2),
(15, '5', 1, 0, 123, 2),
(16, '6', 1, 0, 122, 2),
(17, '122', 1, 0, 31, 2),
(21, 'Motos Motos', 1, 0, 100, 3),
(22, '123', 1, 0, 123, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idproyecto` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `idFacultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`idproyecto`, `nombre`, `idFacultad`) VALUES
(1, 'Sin Asignar', 1),
(2, 'Licenciatura en Español', 2),
(3, 'Ingeniería de Mecánica', 3),
(4, 'Tecnología en Sistematización de Datos (PCP)', 4),
(5, 'Ingeniería Catastral y Geodesia', 3),
(6, 'Ingeniería Eléctrica', 3),
(7, 'Ingeniería Electrónica', 3),
(8, 'Ingeniería Industrial ', 3),
(9, 'Especialización en Avalúos', 3),
(10, 'Especialización en Bioingeniería', 3),
(11, 'Especialización en Gestión de Proyectos de In', 3),
(12, 'Especialización en Higiene, Seguridad y Salud', 3),
(13, 'Especialización en Informática y Automática I', 3),
(14, 'Especialización en Ingeniería de Software', 3),
(20, 'Especialización en Proyectos Informáticos', 3),
(21, 'Especialización en Sistemas de Información Ge', 3),
(22, 'Especialización en Telecomunicaciones Móviles', 3),
(23, 'Especialización en Teleinformática', 3),
(24, 'Maestría en Ciencias de la Información y las ', 3),
(25, 'Maestría en Ingeniería', 3),
(49, 'Maestría en Ingeniería Industrial', 3),
(50, 'Maestría en Telecomunicaciones Móviles', 3),
(51, 'Maestría en Gerencia Integral de Proyectos', 3),
(53, 'Maestría en Telecomunicaciones Móviles', 3),
(55, 'Musicales', 5),
(56, 'Arte Danzario', 5),
(57, 'Artes Escenicas', 5),
(58, 'Plasticas y Visuales', 5),
(59, 'Maestria en Estudios Artisticos', 5),
(60, 'Doctorado en Estudios Artisticos', 5),
(61, 'Ingeniería Civil (PCP)', 4),
(62, 'Ingeniería de Producción (PCP)', 4),
(63, ' Ingeniería Eléctrica (PCP)', 4),
(64, 'Ingeniería en Control y Automatización (PCP)', 4),
(65, 'Ingeniería en Telecomunicaciones (PCP)', 4),
(66, 'Ingeniería en Telemática (PCP)', 4),
(67, 'Ingeniería Mecánica (PCP)', 4),
(68, 'Tec. en Construcciones Civiles (PCP)', 4),
(69, 'Tec. en Electricidad de Media y Baja Tensión ', 4),
(70, 'Tec. en Electrónica Industrial (PCP)', 4),
(81, 'Tec. en Gestión de la Producción Industrial (', 4),
(82, 'Tec. en Mecánica Industrial (PCP)', 4),
(84, 'Tec. en Gestión de la Producción Industrial (', 4),
(85, 'Tec. en Mecánica Industrial (PCP)', 4),
(87, 'Esp. en Gerencia de la Construcción', 4),
(88, 'Esp, en Interventoría y Supervisión de Obras ', 4),
(89, 'Maestría en Gestión y Seguridad de la Informa', 4),
(90, 'Maestría en Ingeniería Civil', 4),
(91, 'Comunicación Social y Periodismo', 2),
(92, 'Archivística y Gestión de la Información Digi', 2),
(93, 'Licenciatura en Biología', 2),
(94, 'Licenciatura en Ciencias Sociales', 2),
(95, 'Licenciatura en Educación Artística', 2),
(96, 'Licenciatura en Educación Infantil', 2),
(97, 'Licenciatura en Física', 2),
(98, 'Lic. en Humanidades y Castellana', 2),
(99, 'Lic. en Lenguas Extranjeras', 2),
(100, 'Licenciatura en Matemáticas', 2),
(101, 'Licenciatura en Química', 2),
(102, 'Doctorado en Estudios Sociales', 2),
(103, 'Doctorado Interinstitucional en Educación', 2),
(104, 'Esp. en Desarrollo Humano ', 2),
(105, 'Esp. en Educación en Tecnología', 2),
(106, 'Esp. en Educación y Gestión Ambiental', 2),
(107, 'Esp. Gerencia de Proyectos Educativos Institu', 2),
(108, 'Esp. en Infancia, Cultura y Desarrollo', 2),
(109, 'Maestría en Comunicación-Educación', 2),
(110, 'Maestría en Desarrollo Humano', 2),
(111, 'Maestría en Educación', 2),
(112, 'Maestría en Educación (Ext. la Guajira)', 2),
(113, 'Maestría en Educación en Tecnología', 2),
(114, 'Maestría en Educación para la Paz', 2),
(115, 'Maestría en Educación y Gestión Ambiental', 2),
(116, 'Maestría en Infancia y Cultura', 2),
(117, 'Maestría en Investigación Social Interdiscipl', 2),
(118, 'Maestría en Lingüística Aplicada a la Enseñan', 2),
(119, 'Maestría en Pedagogía de la Lengua Materna', 2),
(120, 'Tec. en Gestión Ambiental y Servicios Público', 6),
(121, 'Tec. en Saneamiento Ambiental', 6),
(122, 'Tec. en Levantamientos Topográficos', 6),
(123, 'Ingeniería Ambiental', 6),
(124, 'Administración Deportiva', 6),
(125, 'Administración Ambiental', 6),
(126, 'Ingeniería Forestal', 6),
(127, 'Ingeniería Sanitaria', 6),
(128, 'Ingenieria Topografica', 6),
(129, 'Esp. Ambiente y Desarrollo Local', 6),
(130, 'Esp. Diseño de Vias Urbanas, Tránsito y Trans', 6),
(131, 'Esp. Gerencia de Recursos Naturales', 6),
(132, 'Maestría Desarrollo Sustentable y Gestión Amb', 6),
(133, 'Maestria Manejo, Uso y Conservación del Bosqu', 6),
(134, 'Maestría Infraestructura Vial', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idtipo` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idtipo`, `tipo`) VALUES
(1, 'Sin Asignar'),
(2, 'Bicicleta'),
(3, 'Moto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoidentificacion`
--

CREATE TABLE `tipoidentificacion` (
  `idtipoIdentificacion` int(11) NOT NULL,
  `nombreTipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipoidentificacion`
--

INSERT INTO `tipoidentificacion` (`idtipoIdentificacion`, `nombreTipo`) VALUES
(1, 'Sin Asignar'),
(2, 'Tarjeta de Identidad'),
(3, 'Cédula de Ciudadanía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte`
--

CREATE TABLE `transporte` (
  `idtransporte` int(11) NOT NULL,
  `serial` varchar(45) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `fotoTransporte` varchar(45) DEFAULT NULL,
  `fotoCartaPropiedad` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `idColor` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idMarca` int(11) NOT NULL,
  `idParqueadero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `transporte`
--

INSERT INTO `transporte` (`idtransporte`, `serial`, `modelo`, `fotoTransporte`, `fotoCartaPropiedad`, `descripcion`, `estado`, `idColor`, `idTipo`, `idUsuario`, `idMarca`, `idParqueadero`) VALUES
(1, 'sin asignar', 'sin asignar', NULL, 'sinasignar', 'sin asignar', 1, 1, 1, 1, 1, 1),
(2, '12345671', '1995', 'imgFotosTransporte/202207300530481.png', 'imgFotosTransporte/202207300530482.png', 'Cicla montaña rin 22', 0, 2, 2, 2, 3, 2),
(3, 'ccs202', '1995', 'imgFotosTransporte/202208050447431.png', 'imgFotosTransporte/202208050447432.png', 'Clicla rin 24', 0, 2, 3, 2, 4, 6),
(4, 'css21s', '1995', 'imgFotosTransporte/202208170314401.png', 'imgFotosTransporte/202208170314402.png', 'moto', 0, 3, 3, 3, 5, 5),
(9, '123456123122', '2020', 'imgFotosTransporte/202209150308381.png', 'imgFotosTransporte/202209150308382.png', 'Cicla montaña rin 22', 0, 4, 2, 3, 2, 1),
(10, '123321', 'Todoterreno', 'imgFotosTransporte/202209160320341.png', 'imgFotosTransporte/202209160315421.png', 'Cicla montaña rin 22', 0, 3, 2, 6, 3, 2),
(12, 'nrx22d', '2015', 'imgFotosTransporte/202209160255561.png', 'imgFotosTransporte/202209160255562.png', 'Moto bandida', 0, 3, 3, 6, 5, 6),
(13, '987654321', 'Todoterreno', 'imgFotosTransporte/202209230241051.png', 'imgFotosTransporte/202209230241052.png', 'Cicla', 0, 3, 2, 4, 3, 1),
(14, '0912312312', 'Avanti', 'imgFotosTransporte/202210041213341.png', 'imgFotosTransporte/202210041213342.png', 'Cicla montaña rin 22', 0, 2, 2, 30, 2, 2),
(15, '123abc', 'Avanti', 'imgFotosTransporte/202212060343301.png', 'imgFotosTransporte/202212060343302.png', 'Rayones en el marco', 2, 3, 2, 41, 3, 1),
(16, '124587qas1', '2012', 'imgFotosTransporte/202212220235461.png', 'imgFotosTransporte/202212220235551.png', 'Raspones Sayas', 2, 3, 2, 47, 4, 11),
(17, '123po1', '1995', 'imgFotosTransporte/202212220238171.png', 'imgFotosTransporte/202212220238241.png', 'Robada por los Sayasssss', 2, 3, 3, 47, 5, 1),
(18, 'A878BV9202', 'Clásica', 'imgFotosTransporte/202301191212041.png', 'imgFotosTransporte/202301191212042.png', 'Bici de Carreras con Detalles Dorados', 2, 8, 2, 50, 31, 1),
(19, '12QW7E', '2011', 'imgFotosTransporte/202301191217101.png', 'imgFotosTransporte/202301191217102.png', 'Moto Deportiva Clásica', 2, 5, 3, 50, 12, 1),
(20, 'a96a999999', 'Fija', 'imgFotosTransporte/202301191134171.png', 'imgFotosTransporte/202301191134172.png', 'Fija', 2, 7, 2, 123, 28, 1),
(21, 'a01a01', '2022', 'imgFotosTransporte/202301191135111.png', 'imgFotosTransporte/202301191135112.png', 'Moto', 2, 2, 3, 123, 8, 1),
(22, '12QW7Ea123', 'Clásica', 'imgFotosTransporte/202301191142381.png', 'imgFotosTransporte/202301191142382.png', 'Robada sdasdasdsa', 2, 8, 2, 122, 29, 1),
(23, '12QW7Eui11', 'fixed', 'imgFotosTransporte/202301191145371.png', 'imgFotosTransporte/202301191145372.png', 'fiexed', 2, 9, 2, 121, 27, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transportesenparqueadero`
--

CREATE TABLE `transportesenparqueadero` (
  `idtransportesEnParqueadero` int(11) NOT NULL,
  `idTransporte` int(11) NOT NULL,
  `idParqueadero` int(11) NOT NULL,
  `idHistorialParqueadero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `transportesenparqueadero`
--

INSERT INTO `transportesenparqueadero` (`idtransportesEnParqueadero`, `idTransporte`, `idParqueadero`, `idHistorialParqueadero`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `codigoEstudiantil` varchar(45) DEFAULT NULL,
  `fechaNacimiento` date NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `numeroID` varchar(15) DEFAULT NULL,
  `idProyecto` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL,
  `codigoVerificacion` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellido`, `correo`, `password`, `codigoEstudiantil`, `fechaNacimiento`, `direccion`, `telefono`, `foto`, `estado`, `numeroID`, `idProyecto`, `idTipoIdentificacion`, `idGenero`, `codigoVerificacion`) VALUES
(1, 'Sin', 'Asignar', 'sa@sa.com', 'c12e01f2a13ff5587e1e9e4aedb8242d', 'sin asignar', '2012-11-14', 'sin asignar', '00', NULL, 4, '0', 1, 1, 1, NULL),
(2, 'Jairr', 'Gutierrez', 'jg@jg.com', '1272c19590c3d44ce33ba054edfb9c78', '20121778039', '2010-11-24', 'Diagonal 101 sur 2b 56', '3124544005', '', 0, '1231231233', 25, 3, 4, NULL),
(3, 'Diego', 'Fernandez', 'df@correo.udistrital.edu.co', 'eff7d5dba32b4da32d9a67a519434d3f', '20171778040', '1959-07-22', 'Calle 85 D Sur #78-11', '3155555555', NULL, 0, '1285989997', 2, 2, 3, NULL),
(4, 'Brayan', 'Sanchez', 'bs@bs.com', '7c9df801238abe28cae2675fd3166a1a', '20171778041', '2012-11-14', NULL, NULL, NULL, 0, NULL, 1, 1, 4, NULL),
(6, 'Manuelito Agustino', 'Fernandez Gonzalez', 'mafernandezg@correo.udistrital.edu.co', 'b74df323e3939b563635a2cba7a7afba', '20171778042', '2012-11-14', 'Diagonal 101 sur 2b 56', '3212649724', '', 0, '1023035990', 3, 3, 2, NULL),
(7, 'Lucas', 'Anaul', 'lc@lc.com', '196accbcf32b0a8e6bef92e1a37d0fc0', '20171778043', '2012-11-14', NULL, NULL, NULL, 1, NULL, 1, 1, 3, NULL),
(8, 'Tom', 'Holland', 'th@th.com', '1fdc0f893412ce55f0d2811821b84d3b', '20171778044', '2012-11-14', NULL, NULL, NULL, 0, NULL, 1, 1, 3, NULL),
(9, 'Daniela', 'Diaz', 'dd@udistrital.edu.co', '1aabac6d068eef6a7bad3fdf50a05cc8', '20171778045', '2012-11-14', 'Diagonal 101 sur 2b 56', '3124544005', NULL, 0, '1234567891', 2, 2, 3, NULL),
(10, 'Miguel', 'Pulgarin', 'mpulgarin@udistrital.edu.co', '202cb962ac59075b964b07152d234b70', '20171778046', '2012-11-14', NULL, NULL, NULL, 0, NULL, 1, 1, 2, NULL),
(27, 'Pedro', 'Sanchez', 'pedros@correo.udistrital.edu.co', '4297f44b13955235245b2497399d7a93', '20171778047', '2012-11-14', NULL, NULL, NULL, 0, NULL, 1, 1, 2, 762397),
(30, 'Javier', 'Parker', 'javierparker@correo.udistrital.edu.co', '202cb962ac59075b964b07152d234b70', '20171778048', '2012-11-14', 'Diagonal 101 sur 2b 56', '3124544005', NULL, 0, '1231231231', 2, 2, 4, 171578),
(41, 'Ali ', 'Rey Montoya', 'alirey@correo.udistrital.edu.co', '202cb962ac59075b964b07152d234b70', '20171778049', '2000-06-14', 'callle 34a', '3123969879', NULL, 0, '1306568997', 2, 3, 2, 259056),
(42, 'Cejaz', 'Negraz', 'cn@correo.udistrital.edu.co', '28198b369067e88dab9fefe85484dbf4', '20171778050', '1999-06-15', NULL, NULL, NULL, 3, NULL, 1, 1, 3, 478283),
(46, 'Lionel Andres', 'Messi', 'lmessi@correo.udistrital.edu.co', '64ca60972a6ec926d1c4b9d31080c687', '20171778051', '1997-11-01', 'Calle 80 ajsdklsa', '3167701212', NULL, 0, '1233491405', 4, 3, 4, 287241),
(47, 'Cristiano', 'Ronaldo', 'cristianito@correo.udistrital.edu.co', '3212f5f463edb370ff55d3c3a7a15c8f', '20171778052', '1994-02-22', 'Calle usme', '3123123132', 'imgFotosPerfil/202301160836131.png', 0, '1233491405', 4, 3, 4, 170553),
(48, 'Frank', 'Sinatra', 'franksinatra@correo.udistrital.edu.co', '64ca60972a6ec926d1c4b9d31080c687', '20171778053', '2000-04-10', NULL, NULL, NULL, 0, NULL, 1, 1, 3, 224903),
(49, 'Post', 'Malone', 'postmalone@correo.udistrital.edu.co', '64ca60972a6ec926d1c4b9d31080c687', '20171778054', '1993-10-01', 'as', '3197787994', NULL, 0, '1036976569', 3, 3, 3, 905957),
(50, 'José Pepe', 'Madero Nuñez', 'josemadero@correo.udistrital.edu.co', '3da770cc56ed4407b6aaf10ad4e72b4d', '20171778055', '2002-06-18', 'Calle Suba', '3197965656', NULL, 0, '1032635998', 56, 3, 3, 612802),
(51, 'David Mackalister', 'Silva Oñate', 'davidsilva@correo.udistrital.edu.co', '522748524ad010358705b6852b81be4c', '20171778056', '0000-00-00', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 718543),
(52, 'Benito ', 'Benito', 'benito@correo.udistrital.edu.co', '21ad0bd836b90d08f4cf640b4c298e7c', '20171778057', '0000-00-00', NULL, NULL, 'imgFotosPerfil/202301190923541.png', 2, NULL, 1, 1, 1, 307507),
(53, 'Rosalia', 'Ramirez', 'rosaliar@correo.udistrital.edu.co', '514f1b439f404f86f77090fa9edc96ce', '20171778058', '1999-11-02', 'España', '3029741669', NULL, 0, '1960357976', 58, 3, 2, 184863),
(55, 'Harrison', 'Andres', 'andres@aa.com', 'c12e01f2a13ff5587e1e9e4aedb8242d', '20121778060', '2012-11-14', 'sin asignar', '00', NULL, 2, '0', 1, 1, 1, 170551),
(56, 'Mayer', 'Candelo', 'mayer@aa.com', '1272c19590c3d44ce33ba054edfb9c78', '20121778061', '2010-11-24', 'Diagonal', '3124544005', NULL, 0, '0', 25, 3, 2, 170552),
(57, 'Wason', 'Fernandez', 'wason@aa.com', 'eff7d5dba32b4da32d9a67a519434d3f', '20121778062', '1959-07-22', 'Calle 85', '3155555555', NULL, 0, '0', 2, 2, 2, 170554),
(58, 'Nelson', 'Sanchez', 'nelson@aa.com', '7c9df801238abe28cae2675fd3166a1a', '20121778063', '2012-11-14', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 1705513),
(59, 'Lewis', 'Fernandez', 'lewis@aa.com', 'b74df323e3939b563635a2cba7a7afba', '20121778064', '2012-11-14', 'Diagona', '3212649724', NULL, 0, '0', 3, 3, 2, 170555),
(60, 'Roman', 'Torres', 'roman@aa.com', '196accbcf32b0a8e6bef92e1a37d0fc0', '20121778065', '2001-11-14', NULL, NULL, NULL, 0, NULL, 25, 1, 2, 170515),
(61, 'Daniela', 'Diaz', 'danielad@aa.com', '1aabac6d068eef6a7bad3fdf50a05cc8', '20121778067', '1979-11-14', 'Dia', '3124544005', NULL, 0, '0', 2, 2, 2, 170556),
(62, 'Pedro', 'Sanchez', 'pedros@aa.com', '4297f44b13955235245b2497399d7a93', '20121778069', '1978-11-14', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 762397),
(63, 'Javier', 'Parker', 'javierparker@aa.com', '202cb962ac59075b964b07152d234b70', '20121778070', '1992-11-14', NULL, '3124544005', NULL, 0, '0', 2, 2, 2, 171577),
(64, 'Ali ', 'Rey Montoya', 'alirey@aa.com', '202cb962ac59075b964b07152d234b70', '20121778071', '1990-06-14', 'callle', '3123969879', NULL, 0, '0', 2, 3, 2, 259056),
(65, 'Cejaz', 'Negraz', 'cn@aa.com', '28198b369067e88dab9fefe85484dbf4', '20121778071', '1999-06-15', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 478283),
(66, 'Lionel A', 'Messi', 'lmessi@aa.com', '64ca60972a6ec926d1c4b9d31080c687', '20121778072', '1986-11-01', 'Calle 80', '3167701212', NULL, 2, '0', 1, 3, 1, 287241),
(67, 'Cristiano', 'Ronaldo', 'cristianito@aa.com', '3212f5f463edb370ff55d3c3a7a15c8f', '20121778073', '1984-02-22', 'Usme', '3123123132', NULL, 2, '0', 1, 3, 1, 170553),
(68, 'Frank', 'Sinatra', 'franksinatra@aa.com', '64ca60972a6ec926d1c4b9d31080c687', '20121778074', '1995-04-10', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 224903),
(69, 'Post', 'Malone', 'postmalone@aa.com', '64ca60972a6ec926d1c4b9d31080c687', '20121778075', '1993-10-01', 'as', '3197787994', NULL, 2, '0', 1, 3, 1, 905957),
(70, 'José Pepe', 'Madero Nuñez', 'josemadero@aa.com', '3da770cc56ed4407b6aaf10ad4e72b4d', '20121778076', '2002-06-18', 'Suba', '3197965656', NULL, 2, '0', 1, 3, 1, 612802),
(71, 'David Mack', 'Silva Oñate', 'djgutierrezs@aa.com', '522748524ad010358705b6852b81be4c', '20121778077', '0000-00-00', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 718543),
(91, 'Pedro C', 'Franco', 'pedrof@aa.com', '1fdc0f893412ce55f0d2811821b84d3b', '20121778066', '1974-11-14', NULL, NULL, NULL, 0, NULL, 56, 1, 2, 170565),
(92, 'Miguel', 'Pulgarin', 'mpulgarin@aa.com', '202cb962ac59075b964b07152d234b70', '20121778068', '1979-11-14', NULL, NULL, NULL, 2, NULL, 1, 1, 1, 1705567),
(105, 'Camila', 'Andres', 'camilac@aa.com', 'c12e01f2a13ff5587e1e9e4aedb8242d', '20191778078', '2002-11-14', 'sin asignar', '00', NULL, 0, '0', 1, 1, 2, 211551),
(106, 'Mayerli', 'Candelo', 'mayerim@aa.com', '1272c19590c3d44ce33ba054edfb9c78', '20191778079', '2004-08-24', 'Diagonal', '3124544005', NULL, 0, '0', 125, 3, 2, 210552),
(107, 'Wendy', 'Fernandez', 'wendyw@aa.com', 'eff7d5dba32b4da32d9a67a519434d3f', '20191778080', '1979-07-22', 'Calle 85', '3155555555', NULL, 0, '0', 122, 2, 2, 210554),
(108, 'Natalia', 'Sanchez', 'Natalian@aa.com', '7c9df801238abe28cae2675fd3166a1a', '20191778081', '2002-06-14', NULL, NULL, NULL, 0, NULL, 122, 1, 3, 210551),
(109, 'Luisa', 'Fernandez', 'Luisal@aa.com', 'b74df323e3939b563635a2cba7a7afba', '20181778082', '2001-11-14', 'Diagona', '3212649724', NULL, 0, '0', 134, 3, 4, 210555),
(110, 'Romana', 'Torres', 'Romanar@aa.com', '196accbcf32b0a8e6bef92e1a37d0fc0', '20181778084', '2003-12-14', NULL, NULL, NULL, 0, NULL, 25, 1, 4, 210515),
(111, 'Patricia', 'Franco', 'Patriciap@aa.com', '1fdc0f893412ce55f0d2811821b84d3b', '20181778085', '1994-01-14', NULL, NULL, NULL, 0, NULL, 14, 1, 4, 210565),
(112, 'Dania', 'Diaz', 'Daniad@aa.com', '1aabac6d068eef6a7bad3fdf50a05cc8', '20181778086', '1989-10-14', 'Dia', '3124544005', NULL, 0, '0', 13, 2, 4, 210556),
(113, 'Macta', 'Pulgarin', 'Mactam@aa.com', '202cb962ac59075b964b07152d234b70', '20181778087', '1989-09-14', NULL, NULL, NULL, 0, NULL, 11, 1, 4, 2105567),
(114, 'Merida', 'Sanchez', 'Meridam@aa.com', '4297f44b13955235245b2497399d7a93', '20181778089', '1968-01-14', NULL, NULL, NULL, 0, NULL, 111, 1, 4, 212397),
(115, 'Johana', 'Parker', 'Johanaj@aa.com', '202cb962ac59075b964b07152d234b70', '20181778090', '1992-12-14', NULL, '3124544005', NULL, 0, '0', 92, 2, 2, 211577),
(116, 'Aliana', 'Rey Montoya', 'Alianaa@aa.com', '202cb962ac59075b964b07152d234b70', '20181778091', '1990-01-14', 'callle', '3123969879', NULL, 0, '0', 82, 3, 2, 219056),
(117, 'Caren', 'Negraz', 'Carenc@aa.com', '28198b369067e88dab9fefe85484dbf4', '20141778092', '1999-06-15', NULL, NULL, NULL, 0, NULL, 70, 1, 3, 218283),
(118, 'Leonusa', 'Messi', 'Leonusal@aa.com', '64ca60972a6ec926d1c4b9d31080c687', '20141778093', '1987-11-01', 'Calle 80', '3167701212', NULL, 0, '0', 49, 1, 3, 217241),
(119, 'Carmen', 'Ronaldo', 'Carmenc@aa.com', '3212f5f463edb370ff55d3c3a7a15c8f', '20141778094', '1988-02-22', 'Usme', '3123123132', NULL, 0, '0', 49, 1, 3, 210553),
(120, 'Francisca', 'Sinatra', 'Franciscaf@aa.com', '64ca60972a6ec926d1c4b9d31080c687', '20141778095', '1986-04-10', NULL, NULL, NULL, 0, NULL, 112, 1, 3, 214903),
(121, 'Paula', 'Malone', 'paulamalone@correo.udistrital.edu.co', '5109d85d95fece7816d9704e6e5b1279', '20141778096', '1987-10-01', 'as', '3197787994', NULL, 0, '1097846663', 51, 3, 2, 215957),
(122, 'Laura', 'Madero Nuñez', 'lauramadero@correo.udistrital.edu.co', '192292e35fbe73f6d2b8d96bd1b6697d', '20141778097', '1990-06-18', 'Suba', '3197965656', NULL, 0, '7915791163', 61, 3, 2, 212802),
(123, 'Katy', 'Silva Oñate', 'katysilva@correo.udistrital.edu.co', '05f39d8aef6a4f54dcc0ce5ab4385742', '20141778098', '1992-03-20', '12', '3029794464', NULL, 0, '1972325635', 81, 3, 2, 218543);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indices de la tabla `celador`
--
ALTER TABLE `celador`
  ADD PRIMARY KEY (`idcelador`),
  ADD KEY `fk_celador_Facultad1_idx` (`idFacultad`),
  ADD KEY `fk_celador_tipoIdentificacion1_idx` (`idTipoIdentificacion`),
  ADD KEY `FK_idGenero` (`idGenero`) USING BTREE;

--
-- Indices de la tabla `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`idcolor`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`idfacultad`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indices de la tabla `historialparqueadero`
--
ALTER TABLE `historialparqueadero`
  ADD PRIMARY KEY (`idhistorialParqueadero`),
  ADD KEY `fk_historialParqueadero_transporte1_idx` (`idTransporte`),
  ADD KEY `fk_historialParqueadero_parqueadero1_idx` (`idParqueadero`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`),
  ADD KEY `fk_marca_tipo1_idx` (`idTipo`);

--
-- Indices de la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  ADD PRIMARY KEY (`idparqueadero`),
  ADD KEY `fk_parqueadero_tipo1_idx` (`idTipo`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idproyecto`),
  ADD KEY `fk_proyecto_Facultad_idx` (`idFacultad`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  ADD PRIMARY KEY (`idtipoIdentificacion`);

--
-- Indices de la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD PRIMARY KEY (`idtransporte`),
  ADD KEY `fk_transporte_color1_idx` (`idColor`),
  ADD KEY `fk_transporte_tipo1_idx` (`idTipo`),
  ADD KEY `fk_transporte_usuario1_idx` (`idUsuario`),
  ADD KEY `fk_transporte_marca1_idx` (`idMarca`),
  ADD KEY `fk_transporte_parqueadero1_idx` (`idParqueadero`);

--
-- Indices de la tabla `transportesenparqueadero`
--
ALTER TABLE `transportesenparqueadero`
  ADD PRIMARY KEY (`idtransportesEnParqueadero`),
  ADD KEY `fk_transportesEnParqueadero_transporte1_idx` (`idTransporte`),
  ADD KEY `fk_transportesEnParqueadero_parqueadero1_idx` (`idParqueadero`),
  ADD KEY `Fk_idHistorialParqueadero` (`idHistorialParqueadero`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fk_usuario_proyecto1_idx` (`idProyecto`),
  ADD KEY `fk_usuario_tipoIdentificacion1_idx` (`idTipoIdentificacion`),
  ADD KEY `fk_idGenero` (`idGenero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `celador`
--
ALTER TABLE `celador`
  MODIFY `idcelador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `color`
--
ALTER TABLE `color`
  MODIFY `idcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `idfacultad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `idGenero` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `historialparqueadero`
--
ALTER TABLE `historialparqueadero`
  MODIFY `idhistorialParqueadero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  MODIFY `idparqueadero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idproyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idtipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  MODIFY `idtipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transporte`
--
ALTER TABLE `transporte`
  MODIFY `idtransporte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `transportesenparqueadero`
--
ALTER TABLE `transportesenparqueadero`
  MODIFY `idtransportesEnParqueadero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2116;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `celador`
--
ALTER TABLE `celador`
  ADD CONSTRAINT `celador_ibfk_1` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_celador_Facultad1` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idfacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_celador_tipoIdentificacion1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historialparqueadero`
--
ALTER TABLE `historialparqueadero`
  ADD CONSTRAINT `fk_historialParqueadero_parqueadero1` FOREIGN KEY (`idParqueadero`) REFERENCES `parqueadero` (`idparqueadero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_historialParqueadero_transporte1` FOREIGN KEY (`idTransporte`) REFERENCES `transporte` (`idtransporte`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `fk_marca_tipo1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `parqueadero`
--
ALTER TABLE `parqueadero`
  ADD CONSTRAINT `FK_idTipo` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idtipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_Facultad` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`idfacultad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD CONSTRAINT `fk_transporte_color1` FOREIGN KEY (`idColor`) REFERENCES `color` (`idcolor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_marca1` FOREIGN KEY (`idMarca`) REFERENCES `marca` (`idmarca`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_parqueadero1` FOREIGN KEY (`idParqueadero`) REFERENCES `parqueadero` (`idparqueadero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_tipo1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idtipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transporte_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `transportesenparqueadero`
--
ALTER TABLE `transportesenparqueadero`
  ADD CONSTRAINT `Fk_idHistorialParqueadero` FOREIGN KEY (`idHistorialParqueadero`) REFERENCES `historialparqueadero` (`idhistorialParqueadero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transportesenparqueadero_ibfk_1` FOREIGN KEY (`idTransporte`) REFERENCES `transporte` (`idtransporte`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transportesenparqueadero_ibfk_2` FOREIGN KEY (`idParqueadero`) REFERENCES `parqueadero` (`idparqueadero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idproyecto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipoidentificacion` (`idtipoIdentificacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_ibfk_3` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require 'persistencia/UsuarioDAO.php';
require_once 'persistencia/Conexion.php';

class Usuario extends Persona {
    private $codigoEstudiantil;
    private $estado;
    private $idProyecto;
    private $codigoVerificacion;
    private $usuarioDAO;
    private $conexion;

    

    public function getCodigoEstudiantil(){
        return $this->codigoEstudiantil;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getIdProyecto(){
        return $this->idProyecto;
    }

    public function getUsuarioDAO(){
        return $this->usuarioDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }
    public function getCodigoVerificacion(){
        return $this->codigoVerificacion;
    }
    
	
    function Usuario ($id="", $nombre="", $apellido="",$fechaNacimiento="", $correo="", $password="", $codigoEstudiantil="", $direccion="", $telefono="", 
                        $foto="", $estado="", $numeroID="", $idProyecto="", $idTipoIdentificacion="", $idGenero="", $codigoVerificacion=""){ 
        $this -> Persona($id , $nombre, $apellido,$fechaNacimiento , $correo, $password, $direccion, $telefono, $foto, $numeroID, $idTipoIdentificacion, $idGenero);
        $this -> codigoEstudiantil = $codigoEstudiantil;
        $this -> estado = $estado;
        $this -> idProyecto = $idProyecto;
        $this -> codigoVerificacion = $codigoVerificacion;
        $this -> conexion = new Conexion();
        $this -> usuarioDAO = new UsuarioDAO($id, $nombre, $apellido,$fechaNacimiento,  $correo,$password, $codigoEstudiantil, $direccion, $telefono, $foto, $estado, $numeroID, 
                                                $idProyecto, $idTipoIdentificacion, $idGenero, $codigoVerificacion);        
    }
    
    function registrar($codigoVerificacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> registrar($codigoVerificacion));
        $this -> conexion -> cerrar();
    }
    function cambiarCodigo($codigo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> borrarUsuario($codigo));
        $this -> conexion -> cerrar();
    }
    function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> usuarioDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer(); 
            $this -> id = $registro[0];
            $this -> estado = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    

    function autenticarclave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> autenticarclave());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer();
            $this -> nombre = $registro[0];
            $this -> apellido = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }


    function actualizarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> actualizarClave());
        $this -> conexion -> cerrar();
    }
    function actualizarCodigoVerificacion($actualizarCodigoVerificacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> actualizarCodigoVerificacion($actualizarCodigoVerificacion));
        $this -> conexion -> cerrar();
    }


    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function actualizarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO ->actualizarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> existeCorreo());
        $datos = 0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $datos += $registro[0];
        }
        $this -> conexion -> cerrar();
        if($datos >0){
            return true;
        }else{
            return false;
        }

    }

    function existeCodigo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> existeCodigo());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }

    function existeCedula($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> existeCedula($id));
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> fechaNacimiento = $resultado[2]; 
        $this -> correo = $resultado[3];
        $this -> codigoEstudiantil = $resultado[4];
        $this -> direccion = $resultado[5];
        $this -> estado = $resultado[6];
        $this -> telefono = $resultado[7];
        $this -> foto = $resultado[8];
        $this -> numeroID = $resultado[9];
        $this -> idProyecto = $resultado[10];
        $this -> idTipoIdentificacion = $resultado[11];
        $this -> idGenero = $resultado[12];
        $this -> codigoVerificacion = $resultado[13];       
        $this -> conexion -> cerrar();
    }
    function consultarTotalFilas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTotalFilas());
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }

    function eliminar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> usuarioDAO -> eliminar());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos( $filas, $pag){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTodos( $filas, $pag));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Usuario($registro[0], $registro[1], $registro[2],"", $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", $registro[7], "");
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;  
    }

    
    function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarEstado());
        $resultado = $this -> conexion -> extraer();
            $this -> estado = $resultado[0];
    }
    function consultarNombreYApellido($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarNombreYApellido($id));
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> codigoEstudiantil = $resultado[2];
        $this -> foto = $resultado[3];
        $this -> conexion -> cerrar();
    }

    function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> buscar($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new Usuario($registro[0], $registro[1], $registro[2],"", $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", $registro[7], "");
        }
        return $registros;
    }

    function buscarUsuarioParqueadero($filtro,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> buscarUsuarioParqueadero($filtro,$tipo));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new Usuario($registro[0], $registro[1], $registro[2],"", $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", $registro[7], "");
        }
        return $registros;
    }
    function buscarUsuarioUsandoParqueadero($filtro,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> buscarUsuarioUsandoParqueadero($filtro,$tipo));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new Usuario($registro[0], $registro[1], $registro[2],"", $registro[3], "", $registro[4], "", "", $registro[5], $registro[6], "", $registro[7], "");
        }
        return $registros;
    }

    

    function buscarPorCorreo($correo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> buscarPorCorreo($correo));
        $registro = $this -> conexion -> extraer(); 
        $this -> conexion -> cerrar();
        return  $registro[0];
    }



    public function consultarUsuariosPorGenero(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarUsuariosPorGenero());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }

    public function usuariosGenero(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> usuariosGenero());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }

    public function consultarTotalUsuariosActivos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarTotalUsuariosActivos());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }


    public function usuariosSinTransportes(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> usuariosSinTransportes());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }

    public function consultarUsuariosPorEdad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> usuarioDAO -> consultarUsuariosPorEdad());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
    
}
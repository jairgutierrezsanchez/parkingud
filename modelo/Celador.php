<?php
require 'persistencia/CeladorDAO.php';
require_once 'persistencia/Conexion.php';

class Celador extends Persona {
    
    private $estado;
    private $idFacultad;
    private $celadorDAO;
    private $conexion;	

    public function getEstado(){
        return $this->estado;
    }

    public function getIdFacultad(){
        return $this->idFacultad;
    }

    public function getCeladorDAO(){
        return $this->celadorDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Celador ($id="", $nombre="", $apellido="",$fechaNacimiento="", $correo="", $password="", $direccion="", $telefono="", 
                        $numeroID="", $foto= "", $estado="", $idTipoIdentificacion="", $idFacultad="", $idGenero=""){ 
        $this -> Persona($id , $nombre, $apellido,$fechaNacimiento, $correo, $password, $direccion, $telefono, $foto, $numeroID, $idTipoIdentificacion, $idGenero);
        $this -> estado = $estado;
        $this -> idFacultad = $idFacultad;

        $this -> conexion = new Conexion();
        $this -> celadorDAO = new CeladorDAO($id, $nombre, $apellido,$fechaNacimiento, $correo, $password, $direccion, $telefono, $numeroID, $foto, $estado, $idTipoIdentificacion, $idFacultad, $idGenero);        
    }
   
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> celadorDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer(); 
            $this -> id = $registro[0];
            $this->estado = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function actualizarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO ->actualizarEstado($estado));
        $this -> conexion -> cerrar();
    }

    function autenticarclave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> autenticarclave());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer();
            $this -> nombre = $registro[0];
            $this -> apellido = $registro[1];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }


    function actualizarclave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> actualizarClave());
        $this -> conexion -> cerrar();
    }

    function registrarClaveNueva(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> registrarClaveNueva());
        $this -> conexion -> cerrar();
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> existeCorreo());
        $datos = 0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $datos += $registro[0];
        }
        $this -> conexion -> cerrar();
        if($datos >0){
            return true;
        }else{
            return false;
        }

    }

    function existeCedula(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> existeCedula());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> fechaNacimiento = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> direccion = $resultado[4];
        $this -> telefono = $resultado[5];
        $this -> numeroID = $resultado[6];
        $this -> foto = $resultado[7];
        $this -> idTipoIdentificacion = $resultado[8];
        $this -> idGenero = $resultado[9];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos( $filas, $pag){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultarTodos( $filas, $pag));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Celador($registro[0], $registro[1], $registro[2], "", $registro[3], "", "", "", $registro[4], $registro[5], $registro[6], "", "", "");
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function consultarTotalFilas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultarTotalFilas());
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }

    function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> consultarEstado());
        $resultado = $this -> conexion -> extraer();
        $this -> estado = $resultado[0];
    }

    function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> celadorDAO -> buscar($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new Celador($registro[0], $registro[1], $registro[2], "", $registro[3], "", "","", $registro[4], $registro[5], $registro[6]);
        }
        $this -> conexion -> cerrar();
        return $registros;
    }
    
}
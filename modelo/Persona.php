<?php

class Persona {
    protected $id;
    protected $nombre;
    protected $apellido;
    protected $fechaNacimiento;
    protected $correo;
    protected $password;
    protected $direccion;
    protected $telefono;
    protected $foto;
    protected $numeroID;
    protected $idTipoIdentificacion;
    protected $idGenero;

    
    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdTipoIdentificacion(){
        return $this->idTipoIdentificacion;
    }

    public function getId(){
        return $this->id;
    }
    

    public function getNombre(){
        return $this->nombre;
    }

    public function getApellido(){
        return $this->apellido;
    }

    public function getCorreo(){
        return $this->correo;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getDireccion(){
        return $this->direccion;
    }

    public function getTelefono(){
        return $this->telefono;
    }

    public function getFoto(){
        return $this->foto;
    }

    public function getNumeroID(){
        return $this->numeroID;
    }

    public function getIdGenero(){
        return $this->idGenero;
    }
    public function getFechaNacimiento(){
        return $this->fechaNacimiento;
    }

    function Persona($id="" , $nombre="", $apellido="", $fechaNacimiento, $correo="", $password="", $direccion="", $telefono="", $foto="", $numeroID="", $idTipoIdentificacion="", $idGenero=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> password = $password;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> foto = $foto;
        $this -> numeroID = $numeroID;
        $this -> idTipoIdentificacion = $idTipoIdentificacion;
        $this -> idGenero = $idGenero;
        $this -> fechaNacimiento = $fechaNacimiento;
    }
}
?>
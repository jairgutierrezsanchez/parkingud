<?php
require 'persistencia/ParqueaderoDAO.php';
require_once 'persistencia/Conexion.php';

class Parqueadero {
    private $idparqueadero;
    private $numero;
    private $estado;
    private $puestosOcupados;
    private $puestosMaximos;
    private $idTipo;
    private $parqueaderoDAO;
    private $conexion;	
   
    public function getIdparqueadero(){
        return $this->idparqueadero;
    }

    public function getNumero(){
        return $this->numero;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getPuestosOcupados(){
        return $this->puestosOcupados;
    }

    public function getPuestosMaximos(){
        return $this->puestosMaximos;
    }

    public function getParqueaderoDAO(){
        return $this->parqueaderoDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }
   
    function Parqueadero($idparqueadero= "" , $numero= "", $estado= "", $puestosOcupados= "", $puestosMaximos= "",$idTipo=""){
        $this -> idparqueadero = $idparqueadero;
        $this -> numero = $numero;
        $this -> estado = $estado;
        $this -> puestosOcupados = $puestosOcupados;
        $this -> puestosMaximos = $puestosMaximos;
        $this -> idTipo = $idTipo;
        $this -> conexion = new Conexion();
        $this -> parqueaderoDAO = new ParqueaderoDAO($idparqueadero , $numero, $estado, $puestosOcupados, $puestosMaximos,$idTipo);

    }

    function existeNombre(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> existeNombre());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    function eliminar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> eliminar());
        $this -> conexion -> cerrar();
    }

    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizar());
        $this -> conexion -> cerrar();
    }

    function actualizarNombreParqueadero(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizarNombreParqueadero());
        $this -> conexion -> cerrar();
    }
    
    function existeNombreParqueadero(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->existeNombreParqueadero());
        $this -> conexion -> cerrar();
    }

    function actualizarPuestosOcupados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizarPuestosOcupados($this -> puestosOcupados));
        $this -> conexion -> cerrar();
    }
    function actualizarPuestosDesocupados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizarPuestosDesocupados($this -> puestosOcupados));
        $this -> conexion -> cerrar();
    }

    function actualizarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO ->actualizarEstado());
        $this -> conexion -> cerrar();
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> numero = $resultado[0];
        $this -> estado = $resultado[1];
        $this -> puestosOcupados = $resultado[2];
        $this -> puestosMaximos = $resultado[3];
        $this -> idTipo = $resultado[4];
        $this -> conexion -> cerrar();
    }
    function consultarPorId($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultarPorId($id));
        $resultado = $this -> conexion -> extraer();
        $this -> numero = $resultado[0];
        $this -> estado = $resultado[1];
        $this -> puestosOcupados = $resultado[2];
        $this -> puestosMaximos = $resultado[3];
        $this -> idTipo = $resultado[4];
        $this -> conexion -> cerrar();
    }

    function consultarTodos($atributo, $direccion, $filas, $pag, $tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultarTodos($atributo, $direccion, $filas, $pag, $tipo));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Parqueadero($registro[0], $registro[1], "", $registro[3], $registro[4], $registro[5]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }

    function consultarTotalFilas($tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultarTotalFilas($tipo));
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }
    
    function consultarTodosTipo($tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> consultarTodosTipo($tipo));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Parqueadero($registro[0], $registro[1], $registro[2], $registro[3], $registro[4]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function contarParqueaderos($tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> parqueaderoDAO -> contarParqueaderos($tipo));
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }
}
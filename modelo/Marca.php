<?php
require 'persistencia/MarcaDAO.php';
require_once 'persistencia/Conexion.php';

class Marca {
    private $id;
    private $nombre;
    private $idTipo;
    private $marcaDAO;
    private $conexion;
  
    public function getId(){
        return $this->id;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getIdTipo(){
        return $this->idTipo;
    }

    public function getMarcaDAO(){
        return $this->marcaDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Marca($id="" , $nombre="", $idTipo=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> idTipo = $idTipo;
        $this -> conexion = new Conexion();
        $this -> marcaDAO = new MarcaDAO($id, $nombre, $idTipo);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> marcaDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarMotosTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> marcaDAO -> consultarMotosTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Marca($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function consultarBicisTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> marcaDAO -> consultarBicisTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Marca($registro[0], $registro[1], $registro[2]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}
<?php 
require 'persistencia/FacultadDAO.php';
require_once 'persistencia/Conexion.php';

class Facultad{
    private $id;
    private $nombre;
    private $conexion;
    private $facultadDAO;
    
    function getId() {
       return $this->id;
    }
    function getNombre() {
        return $this->nombre;
    }
    

    function Facultad($id="", $nombre="") {
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> facultadDAO = new FacultadDAO($id, $nombre);
    }
    
    function consultarTodos() {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this-> facultadDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Facultad($registro[0], $registro[1]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    
    }
}

?>
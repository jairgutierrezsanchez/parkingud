<?php
require 'persistencia/GeneroDAO.php';
require_once 'persistencia/Conexion.php';

class Genero {
    private $idGenero;
    private $nombreGenero;
    private $generoDAO;
    private $conexion;
  
    public function getIdGenero(){
        return $this->idGenero;
    }
    public function setId($idGenero){
        $this -> idGenero = $idGenero;
        $this -> generoDAO = new GeneroDAO($idGenero);  
   }

    public function getNombreGenero(){
        return $this->nombreGenero;
    }

    public function getGeneroDAO(){
        return $this->generoDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Genero($idGenero="" , $nombreGenero=""){
        $this -> idGenero = $idGenero;
        $this -> nombreGenero = $nombreGenero;
        $this -> conexion = new Conexion();
        $this -> generoDAO = new GeneroDAO($idGenero, $nombreGenero);        
    
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> generoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombreGenero = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> generoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Genero($registro[0], $registro[1]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
}
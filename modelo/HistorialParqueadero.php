<?php
require 'persistencia/HistorialParqueaderoDAO.php';
require_once 'persistencia/Conexion.php';

class HistorialParqueadero {
    private $idhistorialParqueadero;
    private $fechaIngreso;
    private $horaIngreso;
    private $fechaSalida;
    private $horaSalida;
    private $idTransporte;
    private $idParqueadero;
    private $historialParqueaderoDAO;
    private $conexion;	
   

    public function getIdHistorialParqueadero()
    {
        return $this->idhistorialParqueadero;
    }
    public function getHoraIngreso()
    {
        return $this->horaIngreso;
    }
    public function getFechaIngreso()
    {
        return $this-> fechaIngreso;
    }
    public function getHoraSalida()
    {
        return $this-> horaSalida;
    }
    public function getFechaSalida()
    {
        return $this-> fechaSalida;
    }
    public function getTransporte()
    {
        return $this-> idTransporte;
    }
    public function getIdparqueadero()
    {
        return $this-> idParqueadero;
    }

    function HistorialParqueadero($idhistorialParqueadero= "",$fechaIngreso ="", $horaIngreso= "", $fechaSalida ="",$horaSalida = "",$idTransporte = "", $idParqueadero ="" ){
        $this -> idhistorialParqueadero = $idhistorialParqueadero;
        $this -> fechaIngreso = $fechaIngreso;
        $this -> horaIngreso = $horaIngreso;
        $this -> fechaSalida = $fechaSalida;
        $this -> horaSalida = $horaSalida;
        $this -> idTransporte = $idTransporte;
        $this -> idParqueadero = $idParqueadero;
        $this -> conexion = new Conexion();
        $this -> historialParqueaderoDAO = new HistorialParqueaderoDAO($idhistorialParqueadero,$fechaIngreso,$horaIngreso,$fechaSalida,$horaSalida,$idTransporte,$idParqueadero);

    }

    function registrarLlegada(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> registrarLlegada());
        $this -> conexion -> cerrar();
    }
    function registrarSalida(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> registrarSalida());
        $this -> conexion -> cerrar();
    }
    function buscarPorId(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> buscarPorId());
        $resultado = $this -> conexion -> extraer();
        $this -> idhistorialParqueadero = $resultado[2];
        $this -> fechaIngreso = $resultado[0];
        $this -> horaIngreso = $resultado[1];
        $this -> conexion -> cerrar();
    }

    function buscarLogs($codigo,$fecha1,$fecha2,$orden){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> buscarLogs($codigo,$fecha1,$fecha2,$orden));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new HistorialParqueadero($registro[0], $registro[1], $registro[2], $registro[3],$registro[4],$registro[5],$registro[6]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function buscarLogsUsers($id,$fecha1,$fecha2,$orden){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> buscarLogsUsers($id,$fecha1,$fecha2,$orden));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new HistorialParqueadero($registro[0], $registro[1], $registro[2], $registro[3],$registro[4],$registro[5],$registro[6]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }

    function consultarActual($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> consultarActual($id));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new HistorialParqueadero("", $registro[0], $registro[1], "", "", $registro[2], $registro[3]);
            $i++;   
        }        
        $this -> conexion -> cerrar();
        return $resultados;
        
    }

    function consultarActualExpulsar($atributo, $direccion, $filas, $pag,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> historialParqueaderoDAO -> consultarActualExpulsar($atributo, $direccion, $filas, $pag,$tipo));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new HistorialParqueadero("", $registro[0], $registro[1], "", "", $registro[2], $registro[3]);
            $i++;   
        }        
        $this -> conexion -> cerrar();
        return $resultados;
        /*consulta nueva*/
    }
}
?>
<?php
require 'persistencia/TransporteDAO.php';
require_once 'persistencia/Conexion.php';

class Transporte {
    private $idtransporte;
    private $serial;
    private $modelo;
    private $fotoTransporte;
    private $fotoCartaPropiedad;
    private $descripcion;
    private $estado;
    private $idcolor;
    private $idtipo;
    private $idUsuario;
    private $idmarca;
    private $idParqueadero;
    private $transporteDAO;
    private $conexion;	
   
    public function getIdtransporte(){
        return $this->idtransporte;
    }
    public function setIdtransporte($idtransporte){
        $this-> idtransporte = $idtransporte;
    }


    public function getSerial(){
        return $this->serial;
    }

    public function getModelo(){
        return $this->modelo;
    }

    public function getFotoTransporte(){
        return $this->fotoTransporte;
    }

    public function getFotoCartaPropiedad(){
        return $this->fotoCartaPropiedad;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function getIdcolor(){
        return $this->idcolor;
    }

    public function getIdtipo(){
        return $this->idtipo;
    }

    public function getIdusuario(){
        return $this->idUsuario;
    }

    public function getMarca(){
        return $this->idmarca;
    }

    public function getIdparqueadero(){
        return $this->idParqueadero;
    }

    public function getTransporteDAO(){
        return $this->transporteDAO;
    }

    public function getConexion(){
        return $this->conexion;
    }

    function Transporte($idtransporte= "" , $serial= "", $modelo= "", $fotoTransporte= "", $fotoCartaPropiedad= "", $descripcion= "",$estado= "", 
        $idcolor= "", $idtipo= "", $idUsuario= "",$idmarca= "", $idParqueadero= ""){
        $this -> idtransporte = $idtransporte;
        $this -> serial = $serial;
        $this -> modelo = $modelo;
        $this -> fotoTransporte = $fotoTransporte;
        $this -> fotoCartaPropiedad = $fotoCartaPropiedad;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> idcolor = $idcolor;
        $this -> idtipo = $idtipo;
        $this -> idUsuario = $idUsuario;
        $this -> idmarca = $idmarca;
        $this -> idParqueadero = $idParqueadero;
        $this -> conexion = new Conexion();
        $this -> transporteDAO = new TransporteDAO($idtransporte , $serial, $modelo, $fotoTransporte, $fotoCartaPropiedad, $descripcion, $estado, $idcolor, 
                                                    $idtipo, $idUsuario, $idmarca, $idParqueadero);

    }

    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> registrar());
        $this -> conexion -> cerrar();
    }
   
    
    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizar());
        $this -> conexion -> cerrar();
    }

    function actualizarFotoTransporte(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarFotoTransporte());
        $this -> conexion -> cerrar();
    }

    function actualizarFotoPropiedad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarFotoCartaPropiedad());
        $this -> conexion -> cerrar();
    }

    function existeSerial(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> existeSerial());
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    function existeSerialAdmin($serial){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> existeSerialAdmin($serial));
        if($this -> conexion -> numFilas() == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }

    function actualizarEstado($estado,$idtransporte,$idParqueadero){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarEstado($estado,$idtransporte,$idParqueadero));
        $this -> conexion -> cerrar();
    }

    function actualizarEstadoPendiente($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO ->actualizarEstadoPendiente($estado));
        $this -> conexion -> cerrar();
    }

    function consultarEstadoPendiente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarEstadoPendiente());
        $resultado = $this -> conexion -> extraer();
            $this -> estado = $resultado[0];
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> serial = $resultado[0];
        $this -> modelo = $resultado[1];
        $this -> fotoTransporte = $resultado[2];
        $this -> fotoCartaPropiedad = $resultado[3];
        $this -> descripcion = $resultado[4];
        $this -> estado = $resultado[5];
        $this -> idcolor = $resultado[6];
        $this -> idtipo = $resultado[7];
        $this -> idUsuario = $resultado[8];
        $this -> idmarca = $resultado[9];
        $this -> conexion -> cerrar();
    }
    function consultarPorId($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarPorId($id));
        $resultado = $this -> conexion -> extraer();
        $this -> serial = $resultado[0];
        $this -> modelo = $resultado[1];
        $this -> fotoTransporte = $resultado[2];
        $this -> fotoCartaPropiedad = $resultado[3];
        $this -> descripcion = $resultado[4];
        $this -> estado = $resultado[5];
        $this -> idcolor = $resultado[6];
        $this -> idtipo = $resultado[7];
        $this -> idUsuario = $resultado[8];
        $this -> idmarca = $resultado[9];
        $this -> conexion -> cerrar();
    }
    function consultarTotalFilas($tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTotalFilas($tipo));
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }
    function consultarTotalFilasPorAprobar($tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTotalFilasPorAprobar($tipo));
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
    }
    function consultarModal(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> serial = $resultado[0];
        $this -> modelo = $resultado[1];
        $this -> fotoTransporte = $resultado[2];
        $this -> fotoCartaPropiedad = $resultado[3];
        $this -> descripcion = $resultado[4];
        $this -> idcolor = $resultado[5];
        $this -> idtipo = $resultado[6];
        $this -> idmarca = $resultado[7];
        $this -> conexion -> cerrar();
    }

    function consultarTransportesPorId($id,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTransportesPorId($id,$tipo));
        $resultado = $this -> conexion -> extraer();
        $this -> idtransporte  = $resultado[0];
        $this -> serial = $resultado[1];
        $this -> modelo = $resultado[2];
        $this -> fotoTransporte = $resultado[3];
        $this -> fotoCartaPropiedad = $resultado[4];
        $this -> descripcion = $resultado[5];
        $this -> estado = $resultado[6];
        $this -> idcolor = $resultado[7];
        $this -> idtipo = $resultado[8];
        $this -> idUsuario = $resultado[9];
        $this -> idmarca = $resultado[10];
        $this -> idParqueadero = $resultado[11];
        $this -> conexion -> cerrar();
    }

    function consultarTransportesPendientes($id,$tipo,$estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTransportesPendientes($id,$tipo,$estado));
        $resultado = $this -> conexion -> extraer();
        $this -> idtransporte  = $resultado[0];
        $this -> serial = $resultado[1];
        $this -> modelo = $resultado[2];
        $this -> fotoTransporte = $resultado[3];
        $this -> fotoCartaPropiedad = $resultado[4];
        $this -> descripcion = $resultado[5];
        $this -> estado = $resultado[6];
        $this -> idcolor = $resultado[7];
        $this -> idtipo = $resultado[8];
        $this -> idUsuario = $resultado[9];
        $this -> idmarca = $resultado[10];
        $this -> idParqueadero = $resultado[11];
        $this -> conexion -> cerrar();
    }

    function consultarExistenciaTransporte($id,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarExistenciaTransporte($id,$tipo));
        $resultado = $this -> conexion -> extraer();
        if($resultado[0] == 0){
            $this -> conexion -> cerrar();
            return false;
        } else {
            $this -> conexion -> cerrar();
            return true;            
        }
    }
    
    function consultarTodos($atributo, $direccion, $filas, $pag,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTodos($atributo, $direccion, $filas, $pag,$tipo));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4], $registro[7], "", $registro[5], $registro[6]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }

    function consultarTodosPendientes($atributo, $direccion, $filas, $pag,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTodosPendientes($atributo, $direccion, $filas, $pag,$tipo));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], "", $registro[4], $registro[5], "", $registro[6], $registro[7]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultarFotosVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarFotosVehiculo());
        while(($registro = $this -> conexion -> extraer()) != null){
            $this -> fotoTransporte = $registro[0];
            $this -> fotoCartaPropiedad = $registro[1];
        }
        $this -> conexion -> cerrar();
    }
    function consultarFotosCartaDePropiedad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarFotosVehiculo());
        while(($registro = $this -> conexion -> extraer()) != null){
            $this -> fotoTransporte = $registro[0];
            $this -> fotoCartaPropiedad = $registro[1];
        }
        $this -> conexion -> cerrar();
    }
    function consultarSoloBicis($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarSoloBicis($id));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9], $registro[10],$registro[11]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function consultarSoloBicisParqueadero($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarSoloBicisParqueadero($id));
        $resultado = $this -> conexion -> extraer();
        $this -> idtransporte = $resultado[0];
        $this -> conexion -> cerrar();
    }
    function consultarSoloUsuario($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarSoloUsuario($id));
        $resultado = $this -> conexion -> extraer();
        $this -> idUsuario = $resultado[0];
        $this -> conexion -> cerrar();
    }

  

    function consultarSoloMotos($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarSoloMotos($id));
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Transporte($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9], $registro[10],$registro[11]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    function buscarTransportePendiente($filtro,$tipo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> buscarTransportePendiente($filtro,$tipo));
        if (($resultado = $this->conexion->extraer()) != null) {
            $this->idtransporte = $resultado[0];
            $this->serial = $resultado[1];
            $this->modelo = $resultado[2];
            $this->fotoTransporte = $resultado[3];
            $this->descripcion = $resultado[4];
            $this->estado = $resultado[5];
            $this->idtipo = $resultado[6];
            $this->idUsuario = $resultado[7];
        }
            $this->conexion->cerrar();
      
    }

    function cambiarEstado($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarSoloUsuario($id));
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
    }
    

    public function consultarTransportesPorMarca(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTransportesPorMarca());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }


    public function consultarPendientesTransportes(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarPendientesTransportes());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    } 
    
    public function consultarTransportesPorTipo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> transporteDAO -> consultarTransportesPorTipo());
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, $resultado);
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }


        
}
    

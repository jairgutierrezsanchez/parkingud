<?php
require_once  'Persistencia/Conexion.php';
require 'Persistencia/AdministradorDAO.php';
class Administrador extends Persona{
    private $administradorDAO;
    private $conexion;
    
    function Administrador($id="", $nombre="", $apellido="", $correo="", $clave="", $foto=""){
        $this -> Persona($id , $nombre, $apellido, $correo, $clave, $foto);
        $this -> conexion = new Conexion();
        $this -> administradorDAO = new AdministadorDAO($id, $apellido, $nombre, $correo, $clave, $foto);
    }

    function autenticar(){
        $this -> conexion -> abrir();
       $this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        if($this -> conexion ->numFilas()==1){
            $resultado = $this -> conexion -> extraer();
            $this -> id = $resultado[0]; 
            $this -> conexion ->cerrar();
            return true;
        } else {
            $this -> conexion ->cerrar();
            return false;
        }
    }

    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO ->actualizar());
        $this -> conexion -> cerrar();
    }
    
    function actualizarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO ->actualizarFoto());
        $this -> conexion -> cerrar();
    }

    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> existeCorreo());
        $datos = 0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $datos += $registro[0];
        }
        $this -> conexion -> cerrar();
        if($datos >0){
            return true;
        }else{
            return false;
        }

    }

    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $resultado = $this -> conexion -> extraer();        
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> conexion -> cerrar();
    }
}

?>
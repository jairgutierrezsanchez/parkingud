<?php
require_once 'modelo/Persona.php';
require_once 'modelo/Usuario.php';
require_once 'modelo/Transporte.php';
require_once 'modelo/Color.php';
require_once 'modelo/Tipo.php';
require_once 'modelo/Marca.php';

$idtransporte = $_GET['idtransporte'];
$transporte = new Transporte($idtransporte);
$transporte->consultar();
$u = new Usuario();
$u -> consultarNombreYApellido($transporte -> getIdusuario());
$c = new Color($transporte -> getIdcolor());
$c -> consultar();
$t = new Tipo($transporte -> getIdtipo());
$t -> consultar();
$m = new Marca($transporte -> getMarca());
$m -> consultar();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssModal/styleModal.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Consultar Transporte</title>
</head>

<body>
    <div class="backgroundModal">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deatalles Transporte</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="tabla">
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="th" width="20%">Propietario</th>
                            <td><?php echo $u->getNombre() . " " . $u->getApellido() ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Serial</th> 
                            <td><?php echo $transporte -> getSerial(); ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Modelo</th>
                            <td><?php echo $transporte -> getModelo(); ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Foto Transporte</th>
                            <td><?php echo $transporte -> getFotoTransporte()==""?"<img id='fotoTransModal' src=imgFotosPerfil/imagenBase.png>":"<img src=".$transporte -> getFotoTransporte(). " id='fotoConsultarTransporte'>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <th width="30%">Foto Carta de propiedad</th>
                            <td><?php echo $transporte -> getFotoTransporte()==""?"<img id='fotoTransModal' src=imgFotosPerfil/imagenBase.png>":"<img src=".$transporte -> getFotoCartaPropiedad(). " id='fotoConsultarTransporte'>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <th width="20%">Descripcion</th>
                            <td><?php echo $transporte -> getDescripcion(); ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Color</th>

                            <td><?php echo $c -> getColor(); ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Tipo</th>
                            <td><?php echo $t -> getTipo(); ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Marca</th>
                            <td><?php echo $m -> getNombre(); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
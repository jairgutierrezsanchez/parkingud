<?php
session_start();
require_once 'modelo/Persona.php';
require_once 'modelo/Administrador.php';

require_once 'modelo/TransporteEnParqueadero.php';
require_once 'modelo/Facultad.php';
require_once 'modelo/Proyecto.php';
require_once 'modelo/Tipo.php';
require_once 'modelo/Usuario.php';
require_once 'modelo/Celador.php';

require_once 'modelo/Color.php';
require_once 'modelo/Marca.php';
require_once 'modelo/Identificacion.php';
require_once 'modelo/Transporte.php';
require_once 'modelo/Parqueadero.php';
require_once 'modelo/HistorialParqueadero.php';
require_once 'modelo/Genero.php';


?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="cssLogin/styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{
            background: rgb(52,52,52);
            background: linear-gradient(90deg, rgba(52,52,52,1) 0%, rgba(208,207,207,0.7833508403361344) 100%);
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
<script
	src="https://www.gstatic.com/charts/loader.js"></script>
   	<script charset="utf-8">
        $(function () { 
        	$("[data-toggle='tooltip']").tooltip(); 
        });
    </script>
</head> 

<body>


<?php    
     if(isset($_GET["pid"])!=""){
        $pid = base64_decode($_GET["pid"]);
        if(isset($_GET["nos"]) || $_SESSION["id"]!=""){
            include $pid;
        }else{
            header("Location: index.php");
            
        }
    }else{
        include "presentacion/Login.php";
    }
    
?>


</body>
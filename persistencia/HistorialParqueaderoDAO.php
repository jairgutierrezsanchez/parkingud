<?php

class HistorialParqueaderoDAO {
    private $idhistorialParqueadero;
    private $fechaIngreso;
    private $horaIngreso;
    private $fechaSalida;
    private $horaSalida;
    private $idTransporte;
    private $idParqueadero;

    

    function HistorialParqueaderoDAO($idhistorialParqueadero,$fechaIngreso , $horaIngreso, $fechaSalida,$horaSalida ,$idTransporte, $idParqueadero){
        $this -> idhistorialParqueadero = $idhistorialParqueadero;
        $this -> fechaIngreso = $fechaIngreso;
        $this -> horaIngreso = $horaIngreso;
        $this -> fechaSalida = $fechaSalida;
        $this -> horaSalida = $horaSalida;
        $this -> idTransporte = $idTransporte;
        $this -> idParqueadero = $idParqueadero;
    }

    function registrarLlegada(){
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone('America/Bogota'));
        $date = $now -> format('y-m-d');
        $time= $now -> format('H:i:s');
        return "insert into historialparqueadero (fechaIngreso, horaIngreso, idTransporte, idParqueadero)
            values ('". $date . "','" . $time . "'," . $this -> idTransporte. "," .$this -> idParqueadero . ")";
    }
    function registrarSalida(){
        $now = new DateTime();
        $now->setTimezone(new DateTimeZone('America/Bogota'));
        $date = $now -> format('y-m-d');
        $time= $now -> format('H:i:s');
        return "update historialparqueadero
        SET horaSalida = '".$time."', fechaSalida = '".$date."'
        WHERE idTransporte =".$this -> idTransporte. " and idParqueadero =".$this -> idParqueadero ." and horaSalida is NULL and fechaSalida is NULL;";
    }
    
    function buscarPorId(){
        return  "select fechaIngreso, horaIngreso, idhistorialParqueadero from historialparqueadero
        where idTransporte =" .  $this -> idTransporte . " and idParqueadero =" .$this -> idParqueadero . " and fechaSalida is NULL and horaSalida is NULL";
    }
    function buscarLogs($codigo,$fecha1,$fecha2,$orden){
        return  "select hp.* FROM historialparqueadero as hp
         inner JOIN usuario as u 
        inner JOIN transporte as t 
        on hp.idTransporte= t.idtransporte 
        and t.idUsuario = u.idusuario" . 
        ($codigo!=""?" and u.codigoEstudiantil =".$codigo:"") .
        ($fecha1!="" && $fecha2!=null?" and hp.fechaIngreso BETWEEN '".$fecha1."' and '".$fecha2."' ":" and hp.fechaIngreso = '".$fecha1."'").
        " ORDER by hp.fechaIngreso ". $orden;
    }

    function buscarLogsUsers($id,$fecha1,$fecha2,$orden){
        return  "select hp.* FROM historialparqueadero as hp
         inner JOIN usuario as u 
        inner JOIN transporte as t 
        on hp.idTransporte= t.idtransporte 
        and t.idUsuario= u.idusuario".
        ($fecha1!="" && $fecha2!=""?" and hp.fechaIngreso BETWEEN '".$fecha1."' and '".$fecha2."' ":" and hp.fechaIngreso = '".$fecha1."'").
        " and u.idusuario =".$id."
         ORDER by hp.fechaIngreso ". $orden;
    }

    function consultarActual($id){
        return "select hp.fechaIngreso, hp.horaIngreso,hp.idTransporte ,hp.idParqueadero from historialparqueadero as hp 
                INNER join usuario as u 
                inner join transporte as t  
                on t.idUsuario = u.idusuario 
                and u.idusuario =".$id." 
                and t.idtransporte = hp.idTransporte
                and hp.horaSalida is NULL";
    }

    function consultarActualExpulsar($atributo, $direccion, $filas, $pag,$tipo){
        return "select u.idusuario, u.nombre, u.apellido, u.codigoEstudiantil as usuario,
		                hp.fechaIngreso, hp.horaIngreso,hp.idTransporte ,hp.idParqueadero as historialparqueadero 
                from historialparqueadero as hp 
                    inner join usuario as u 
                    inner join transporte as t  
                    on t.idUsuario = u.idusuario 
                    and t.idtransporte = hp.idTransporte
                    and hp.horaSalida is NULL
                    and idTipo =". $tipo."
                    " .(($atributo!="" && $direccion!="")?"order by ".$atributo ." ". $direccion:"").
                        " limit " . (($pag-1)*$filas) . ", " . $filas;  
    } /*consulta nueva*/


}
?>
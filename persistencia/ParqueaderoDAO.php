<?php

class ParqueaderoDAO{

    private $idparqueadero;
    private $numero;
    private $estado;
    private $puestosOcupados;
    private $idTipo;
    private $puestosMaximos;

    function ParqueaderoDAO($idparqueadero= "" , $numero= "", $estado= "", $puestosOcupados= "", $puestosMaximos= "", $idTipo= ""){        
        $this -> idparqueadero = $idparqueadero;
        $this -> numero = $numero;
        $this -> estado = $estado;
        $this -> puestosOcupados = $puestosOcupados;
        $this -> puestosMaximos = $puestosMaximos;
        $this -> idTipo = $idTipo;
    }
    
    function existeNombre(){
        return "select idparqueadero 
                from parqueadero
                where numero = '" . $this->numero . "'";
    }
    function eliminar(){
       return "delete from `parqueadero` WHERE idParqueadero =".$this ->idparqueadero;
    }

    function registrar(){
        return  "insert into parqueadero (idparqueadero, numero, estado, puestosOcupados, idTipo, puestosMaximos)
                values ('" . $this-> idparqueadero . "', '" . $this-> numero . "', 
                        '".$this -> estado . "', '".$this -> puestosOcupados . "',
                        '" .$this -> idTipo . "', '" .$this -> puestosMaximos . "')";
    }

    function actualizar(){
        return "update parqueadero set 
        puestosMaximos = '" . $this -> puestosMaximos . "'
        where idparqueadero=" . $this -> idparqueadero;
    }

    function actualizarNombreParqueadero(){
        return "update parqueadero set 
        numero = '" . $this -> numero . "'
        where idparqueadero=" . $this -> idparqueadero;
    }

    function existeNombreParqueadero(){
        return "select idparqueadero 
                from parqueadero
                where numero = '" . $this->numero . "'";
    }

    function actualizarPuestosOcupados($puestosOcupados){
        $int = intval($puestosOcupados);
        $int += 1;
        return  "update parqueadero set 
                puestosOcupados ='"  . $int . "'
                where idparqueadero=" . $this -> idparqueadero;
    }
    function actualizarPuestosDesocupados($puestosOcupados){
        $int = intval($puestosOcupados);
        $int -= 1;
        return  "update parqueadero set 
                puestosOcupados ='"  . $int . "'
                where idparqueadero=" . $this -> idparqueadero;
    }

    function actualizarEstado(){
        return "update parqueadero set
                estado = '" . $this -> estado . "'
                where idparqueadero=" . $this -> idparqueadero;
    }
    
    function consultar() {
        return "select numero,estado, puestosOcupados, puestosMaximos, idTipo
                from parqueadero
                where 	idparqueadero =" . $this -> idparqueadero;
    }
    function consultarPorId($id) {
        return "select numero,estado, puestosOcupados, puestosMaximos, idTipo
                from parqueadero
                where 	idparqueadero =" . $id;
    }

    function consultarTodosTipo($tipo){
        return "select idparqueadero, numero, estado, puestosOcupados, puestosMaximos, idTipo
                from parqueadero
                where idparqueadero <> 0 and idTipo = ". $tipo . " and estado = 1
                order by idparqueadero";
    }

    function consultarTodos($atributo, $direccion, $filas, $pag, $tipo){
        return "select idparqueadero, numero, estado, puestosOcupados, puestosMaximos, idTipo
                from parqueadero
                where idparqueadero <> 1 and idTipo =". $tipo."
                " .(($atributo!="" && $direccion!="")?"order by ".$atributo ." ". $direccion:"").
                " limit " . (($pag-1)*$filas) . ", " . $filas;   
        
    }

    

    public function consultarTotalFilas($tipo){
        return "select count(idparqueadero)
                from parqueadero
                where idTipo = ".$tipo;
    }
    public function contarParqueaderos($tipo){
        return "select count(idparqueadero)
                from parqueadero where idTipo=".$tipo;
    }

}

?>
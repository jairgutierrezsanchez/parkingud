<?php

class CeladorDAO{
    
    private $id;
    private $nombre;
    private $apellido;
    private $fechaNacimiento;
    private $correo;
    private $password;
    private $direccion;
    private $telefono;
    private $numeroID;
    private $foto;
    private $estado;
    private $idTipoIdentificacion;
    private $idFacultad;
    private $idGenero;

    function CeladorDAO($id = "", $nombre = "", $apellido = "",$fechaNacimiento="", $correo = "", $password = "", $direccion  ="", $telefono = "", $numeroID = "", $foto = "", $estado = "",
                        $idTipoIdentificacion = "", $idFacultad = "", $idGenero = ""){
                        
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->correo = $correo;
        $this->password = $password;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->numeroID = $numeroID;
        $this->foto = $foto;
        $this->estado = $estado;
        $this->idTipoIdentificacion = $idTipoIdentificacion;
        $this->idFacultad = $idFacultad;
        $this->idGenero = $idGenero;
    }

    function registrar(){
        return  "insert into celador (nombre, apellido, fechaNacimiento, correo, password, estado, idTipoIdentificacion, idFacultad, idGenero)
                values ('" . $this->nombre . "', '" . $this->apellido . "', 
                        '" . $this->fechaNacimiento . "', '" . $this->correo . "', 
                        '" . md5($this->password) . "', '" . $this->estado . "', 
                        '" . $this->idTipoIdentificacion . "', '" . $this->idFacultad . "',   
                        '" . $this->idGenero . "')";
    }

    function autenticar(){
        return "select idcelador, estado
                from celador 
                where correo = '" . $this -> correo . "' and password = '" . md5($this-> password) . "'";
    }

    function actualizar(){
        return "update celador set 
                nombre = '" . $this -> nombre . "',
                apellido= '" . $this -> apellido . "', 
                fechaNacimiento= '" . $this -> fechaNacimiento . "', 
                direccion= '" . $this -> direccion . "',
                telefono= '" . $this -> telefono . "',
                numeroID= '" . $this -> numeroID . "',
                idTipoIdentificacion= '" . $this -> idTipoIdentificacion . "',
                idGenero= '" . $this -> idGenero . "'
                where idcelador=" . $this -> id;	
    }
    
    function actualizarFoto(){
        return "update celador set
                foto = '" . $this -> foto . "'
                where idcelador=" . $this -> id;
    }

    function actualizarEstado($estado){
        return "update celador set
                estado = '" . $estado . "'
                where idcelador=" . $this -> id;
    }


    function autenticarclave(){
        return "select *
                from celador
                where idcelador = " . $this -> id . " and password = '" . md5($this->password) . "'";
    }


    function actualizarClave() {
        return "update celador
                set password = '" . md5($this -> password) . "'
                where idcelador=" . $this->id . "";
    }

    function registrarClaveNueva(){
        return  "update celador 
                set password= '" . md5($this -> password) . "'
                where idcelador=" . $this->id . "";
    }
    
    function consultar() { 
        return "select nombre, apellido, fechaNacimiento, correo, direccion, telefono, numeroID, foto,  idTipoIdentificacion, idGenero
                from celador
                where 	idcelador =" . $this -> id;
    }

    function existeCorreo(){
        return "select count(c.idcelador) as 'correos' 
        from celador as c where c.correo = '".$this -> correo ."' 
        UNION ALL 
        select count(u.idusuario) 
        from usuario as u where u.correo = '".$this -> correo ."' 
        union all 
        select count(a.idadmin) 
        from admin as a where a.correo = '".$this -> correo ."'" ;
    }

    function existeCedula(){
        return "select idcelador 
                from celador
                where numeroID = '" . $this->numeroID . "'";
    }

    function consultarTodos( $filas, $pag){
        return "select idcelador, nombre, apellido, correo, numeroID, foto, estado
                from celador limit " . (($pag-1)*$filas) . ", " . $filas;    
        
    }

    public function consultarTotalFilas(){
        return "select count(idcelador)
                from celador";
    }

    function consultarEstado(){
        return "select estado
                from celador
                where idcelador = " . $this -> id;
    }

    function buscar($filtro){
        return "select idcelador, nombre, apellido, correo, numeroID, foto, estado
                from celador
                where 
                nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }


	
}

?>
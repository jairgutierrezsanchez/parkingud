<?php

class ProyectoDAO{
    
    private $id;
    private $nombre;
    private $idFacultad;

    function ProyectoDAO($id = "", $nombre = "", $idFacultad = ""){
        
        $this->id = $id;
        $this->nombre = $nombre;
        $this->idFacultad = $idFacultad;
    }
    
    function consultar($id) {
        return "select idproyecto, nombre, idFacultad
                from proyecto
                where 	idproyecto =" . $id;
    }

    function consultarTodosPorFacultad(){
        return "select idproyecto, nombre, idFacultad
                from proyecto
                where idFacultad =" . $this -> idFacultad . "
                order by idproyecto
                ";
    }
     public function consultarUsuariosPorProyecto() {
        return "select sub.nombre as proyectoCurricular, count(u.idusuario) as cantidad
        from usuario as u  right join
        (SELECT  idproyecto, nombre from proyecto where idFacultad =".$this-> idFacultad.") as sub 
        on sub.idProyecto = u.idProyecto
        group by sub.nombre
       ";
    }
}

?>
<?php

class TransporteDAO{

    private $idtransporte;
    private $serial;
    private $modelo;
    private $fotoTransporte;
    private $fotoCartaPropiedad;
    private $descripcion;
    private $estado;
    private $idcolor;
    private $idtipo;
    private $idUsuario;
    private $idmarca;
    private $idParqueadero;

    function TransporteDAO($idtransporte= "", $serial= "", $modelo= "", $fotoTransporte= "", $fotoCartaPropiedad= "", $descripcion= "", 
                            $estado= "", $idcolor= "", $idtipo= "", $idUsuario= "", $idmarca= "", $idParqueadero= ""){        
        $this -> idtransporte = $idtransporte;
        $this -> serial = $serial;
        $this -> modelo = $modelo;
        $this -> fotoTransporte = $fotoTransporte;
        $this -> fotoCartaPropiedad = $fotoCartaPropiedad;
        $this -> descripcion = $descripcion;
        $this -> estado = $estado;
        $this -> idcolor = $idcolor;
        $this -> idtipo = $idtipo;
        $this -> idUsuario = $idUsuario;
        $this -> idmarca = $idmarca;
        $this -> idParqueadero = $idParqueadero;
        
    }
    

    function registrar(){
        return   "insert into transporte (serial, modelo, descripcion, estado, idColor, idTipo, idUsuario, idMarca, idParqueadero, fotoTransporte, fotoCartaPropiedad)
                values ('" . $this-> serial . "', '" . $this-> modelo . "', '".$this -> descripcion . "',
                        '" .$this -> estado . "', '" .$this -> idcolor . "', '" .$this -> idtipo ."',
                        '" .$this -> idUsuario ."', '" .$this -> idmarca . "', '" .$this -> idParqueadero ."',
                        '". $this -> fotoTransporte . "', '". $this -> fotoCartaPropiedad . "')";
    }

    function actualizar(){
        return "update transporte set 
                serial = '" . $this -> serial . "',
                estado = '" . $this -> estado . "',
                modelo = '" . $this -> modelo . "',
                descripcion = '" . $this -> descripcion . "',
                idColor = '" . $this -> idcolor . "',
                idMarca ='" . $this -> idmarca . "'
                where idtransporte=" . $this -> idtransporte;
    }
    
    function actualizarFotoTransporte(){
        return "update transporte set
                fotoTransporte = '" . $this -> fotoTransporte . "',
                estado = '" . $this -> estado . "'
                where idtransporte=" . $this -> idtransporte;
    }

    function actualizarFotoCartaPropiedad(){
        return "update transporte set
                fotoCartaPropiedad = '" . $this -> fotoCartaPropiedad . "',
                estado = '" . $this -> estado . "'
                where idtransporte=" . $this -> idtransporte;
    }
    public function consultarTotalFilas($tipo){
        return "select count(idtransporte)
                from transporte
                where idTipo =" .$tipo;
    }
    public function consultarTotalFilasPorAprobar($tipo){
        return "select count(idtransporte)
                from transporte
                where idTipo =" .$tipo." and estado =2";
    }

    function existeSerial(){
        return "select idtransporte 
                from transporte
                where serial = '" . $this->serial . "'";
    }
    function existeSerialAdmin($serial){
        return "select idtransporte 
                from transporte
                where serial = '" . $serial . "'";
    }

    function actualizarEstado($estado,$idtransporte,$idParqueadero){
        return "update transporte set
                estado = '" . $estado . "',
                idParqueadero ='" . $idParqueadero . "'
                where idtransporte=" . $idtransporte;
    }

    function actualizarEstadoPendiente($estado){
        return "update transporte set
                estado = '" . $estado . "'
                where idtransporte=" . $this -> idtransporte;
    }

    function consultarEstadoPendiente(){
        return "select estado
                from transporte
                where idtransporte = " . $this -> idtransporte;
    }
    
    function consultar() {
        return "select serial, modelo, fotoTransporte, fotoCartaPropiedad, 
                        descripcion, estado, idcolor, idtipo, idUsuario, idmarca
                from transporte
                where 	idtransporte=" . $this -> idtransporte;
    }
    function consultarPorId($id) {
        return "select serial, modelo, fotoTransporte, fotoCartaPropiedad, 
                        descripcion, estado, idcolor, idtipo, idUsuario, idmarca
                from transporte
                where 	idtransporte=" . $id;
    }

    function consultarModal() {
        return "select serial, modelo, fotoTransporte, fotoCartaPropiedad, 
                    descripcion, idcolor, idtipo, idmarca
                from transporte
                where 	idtransporte =" . $this -> idtransporte;
    }

    function consultarTodos($atributo, $direccion, $filas, $pag,$tipo){
        return "select idtransporte, serial, modelo, fotoTransporte, descripcion, idTipo, idUsuario, estado
        from transporte
        where idtransporte <> 1 and idTipo =". $tipo."
        " .(($atributo!="" && $direccion!="")?"order by ".$atributo ." ". $direccion:"").
        " limit " . (($pag-1)*$filas) . ", " . $filas;           
    }

    function consultarTodosPendientes($atributo, $direccion, $filas, $pag,$tipo){
        return "select idtransporte, serial, modelo, fotoTransporte, descripcion, estado, idTipo, idUsuario
        from transporte
        where idtransporte <> 1 and idTipo =". $tipo." and estado=2
        " .(($atributo!="" && $direccion!="")?"order by ".$atributo ." ". $direccion:"").
        " limit " . (($pag-1)*$filas) . ", " . $filas;           
    }
       
    function consultarFotosVehiculo(){
       return "select  fotoTransporte, fotoCartaPropiedad
                from transporte
                where idTransporte=" . $this -> idtransporte;
    }
    function consultarTransportesPorId($id,$tipo){
        return "select *
                from transporte
                where idUsuario =". $id ."
                and idTipo = " . $tipo . "
                order by idtransporte";
    }
/*and u.codigoEstudiantil ="  .$filtro ." */
    function consultarTransportesPendientes($id,$tipo,$estado){
        return "select *
                from transporte

                
        
                where idUsuario =". $id ."
                and idTipo = " . $tipo . "
                and estado = " . $estado . "
                order by idtransporte";
    }

    function consultarExistenciaTransporte($id,$tipo){
        return "select COUNT(idtransporte) 
                FROM transporte 
                WHERE idUsuario =". $id ." 
                and idTipo =" .$tipo;

    }
    

    function consultarSoloBicis($id){
        return "select *
                from transporte
                where idUsuario =". $id ." and  idTipo=2
                order by idtransporte";
    }
    function consultarSoloBicisParqueadero($id){
        return "select idTransporte
                from transporte
                where idUsuario =". $id ." and  idTipo=2";
    }

    function consultarSoloBicisAdmin($id){
        return "select *
                from transporte
                where idUsuario =". $id ." and  idTipo=2
                order by idtransporte";
    }

    function consultarSoloMotos($id){
        return "select *
                from transporte
                where idUsuario =". $id ." and  idTipo=3
                order by idtransporte";
    }
    function consultarSoloUsuario($id){
        return "select idUsuario
                from transporte
                where idtransporte =". $id;
    }
      function buscarTransportePendiente($filtro,$tipo){
        return "select DISTINCT t.idtransporte, t.serial, t.modelo, t.fotoTransporte, t.descripcion, t.estado, t.idTipo, t.idUsuario from transporte as t
        inner JOIN usuario as u
        on t.idUsuario = u.idusuario and u.codigoEstudiantil =". $filtro . " and t.estado =2 and t.idTipo=". $tipo;
    }





    public function consultarTransportesPorTipo(){
        return "(select 'Bicicleta', count(idtipo)
                    from transporte
                    where idtipo = 2)
                    union
                (select 'Moto', count(idtipo)
                    from transporte
                    where idtipo = 3)";
    }

    
    public function consultarTransportesPorMarca(){
        return "select m.nombre as Marca, count( sub.idtransporte) as cantidad
        from marca as m RIGHT JOIN
        (SELECT idtransporte,idMarca from transporte where idTipo =".$this->idtipo.") as sub
        on m.idmarca = sub.idMarca 
        GROUP by m.nombre;";
    }

    public function consultarPendientesTransportes(){
        return "(select 'Motos Pendientes', count(idTipo)
                        from transporte
                        where idTipo = 3
                        and estado= 2)
                        union
                (select 'Bicis Pendientes', count(idTipo)
                        from transporte
                        where idTipo = 2
                        and estado= 2)
                        union
                (select 'Motos Activas', count(idTipo)
                        from transporte
                        where idTipo = 3
                        and estado= 0)
                        union
                (select 'Bicis Activas', count(idTipo)
                        from transporte
                        where idTipo = 2
                        and estado= 0)";
    }
    

    

}

?>
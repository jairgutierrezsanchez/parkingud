<?php

class UsuarioDAO {

    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $fechaNacimiento;
    private $password;
    private $codigoEstudiantil;
    private $direccion;
    private $telefono;
    private $foto;
    private $estado;
    private $numeroID;
    private $idProyecto;
    private $idTipoIdentificacion;
    private $idGenero;
    private $codigoVerificacion;

    function UsuarioDAO($id= "", $nombre= "", $apellido= "",$fechaNacimiento="", $correo= "", $password= "", $codigoEstudiantil= "", $direccion= "", $telefono= "", 
                        $foto= "", $estado= "", $numeroID= "", $idProyecto= "", $idTipoIdentificacion= "", $idGenero="", $codigoVerificacion=""){

        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->correo = $correo;
        $this->password = $password;
        $this->codigoEstudiantil = $codigoEstudiantil;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
        $this->foto = $foto;
        $this->estado = $estado;
        $this->numeroID = $numeroID;
        $this->idProyecto = $idProyecto;
        $this->idTipoIdentificacion = $idTipoIdentificacion;
        $this->idGenero = $idGenero;
        $this -> codigoVerificacion=$codigoVerificacion;
    }

    function registrar($codigoVerificacion){
        return  "insert into usuario (nombre, apellido, correo, password, codigoEstudiantil,  estado, idProyecto,idGenero,idTipoIdentificacion, codigoVerificacion)
                values ('" . $this->nombre . "', '" . $this->apellido . "', 
                        '" . $this->correo . "', 
                        '" . md5($this->password) . "', '" . $this->codigoEstudiantil . "', 
                        '" . $this->estado . "', 
                        '" . $this->idProyecto . "','" . $this->idTipoIdentificacion . "', 
                        '" . $this->idGenero . "',  '". $codigoVerificacion ."')";
    }
   

    function autenticar(){
        return "select idusuario, estado
                from usuario 
                where correo = '" . $this -> correo . "' and password = '" . md5($this-> password) . "'";
    }


    function autenticarclave(){
        return "select *
                from usuario
                where idusuario = " . $this -> id . " and password = '" . md5($this->password) . "'";
    }


    function actualizarClave() {
        return "update usuario set 
                password = '" . md5($this -> password) . "',
                estado= 0
                where idusuario=" . $this->id . "";
    }
    function actualizarCodigoVerificacion($codigo) {
        return "update usuario
                set codigoVerificacion = '" . $codigo . "'
                where idusuario=" . $this->id . "";
    }
   
    function actualizar(){
        return "update usuario set 
                nombre = '" . $this -> nombre . "',
                apellido='" . $this -> apellido . "',
                fechaNacimiento='" . $this -> fechaNacimiento . "',
                direccion='" . $this -> direccion . "',
                telefono='" . $this -> telefono . "',
                numeroID='" . $this -> numeroID . "',
                idProyecto='" . $this -> idProyecto . "',
                idTipoIdentificacion='" . $this -> idTipoIdentificacion . "',
                idGenero='" . $this -> idGenero . "' ,
                estado = 0
                where idusuario=" . $this -> id;
    }
    
    function actualizarFoto(){
        return "update usuario set
                foto = '" . $this -> foto . "'
                where idusuario=" . $this -> id;
    }

    function actualizarEstado($estado){
        return "update usuario set
                estado = '" . $estado . "'
                where idusuario=" . $this -> id;
    }
    
    function consultar() {
        return "select nombre, apellido, fechaNacimiento, correo, codigoEstudiantil, direccion, estado, telefono, foto, numeroID, idProyecto, idTipoIdentificacion, idGenero, codigoVerificacion
        from usuario
        where idusuario =" . $this -> id ;
                }
    function consultarNombreYApellido($id) {
        return "select nombre, apellido, codigoEstudiantil, foto
                from usuario
                where idusuario =" . $id;
    }

    function eliminar() {
        return "delete usuario 
                from usuario
                where idusuario =" . $this -> id;
    }
    function cambiarEstado($estado) {
        return "update usuario
        set estado = ".$estado."
        where idusuario=" . $this ->id . "";
    }
        
    function existeCorreo(){
        return "select count(c.idcelador) as 'correos' 
        from celador as c where c.correo = '".$this -> correo ."' 
        UNION ALL 
        select count(u.idusuario) 
        from usuario as u where u.correo = '".$this -> correo ."' 
        union all 
        select count(a.idadmin) 
        from admin as a where a.correo = '".$this -> correo ."'" ;
    }
    function existeCodigo(){
        return "select idusuario 
                from usuario
                where codigoEstudiantil = '" . $this->codigoEstudiantil . "'";
    }

    function existeCedula($id){
        return "select idusuario 
                from usuario
                where numeroID = '" . $this->numeroID . "' and numeroID <> '".$id ."'";
    }

    function consultarTodos( $filas, $pag){	
        return "select idusuario, nombre, apellido, correo, codigoEstudiantil, foto, estado, idProyecto
                from usuario where estado <> 4 limit " . (($pag-1)*$filas) . ", " . $filas;           
                
    }
    public function consultarTotalFilas(){
        return "select count(idusuario)
                from usuario";
    }
    function consultarEstado(){
        return "select estado
                from usuario
                where idusuario = " . $this -> id;
    }

    function buscar($filtro){
        return "select idusuario, nombre, apellido, correo, codigoEstudiantil, foto, estado, idProyecto
                from usuario
                where codigoEstudiantil like '" . $filtro . "%' ";
    }

    function buscarUsuarioParqueadero($filtro,$tipo){
        return "select distinct u.idusuario, u.nombre, u.apellido, u.correo, u.codigoEstudiantil, u.foto, u.estado, u.idProyecto
        FROM usuario as u
        inner JOIN transporte as t 
        on t.idUsuario=u.idusuario  
        and u.codigoEstudiantil ="  .$filtro ."
        and t.idTipo=" . $tipo ; 
    }
    function buscarUsuarioUsandoParqueadero($filtro,$tipo){
        return "select distinct u.idusuario, u.nombre, u.apellido, u.correo, u.codigoEstudiantil, u.foto, u.estado, u.idProyecto
        FROM `usuario` as u 
        inner JOIN transporte as t 
        inner join transportesenparqueadero as tep 
        on u.codigoEstudiantil =".$filtro."
        and u.idusuario = t.idUsuario 
        and t.idtransporte = tep.idTransporte
        and t.idTipo =".$tipo."
        and t.estado = 1";
    }


    /*select DISTINCT u.idusuario, u.nombre, u.apellido, tep.idTransporte, tep.idParqueadero FROM `transportesenparqueadero` as tep inner JOIN transporte as t inner join usuario as u inner join parqueadero as p on p.idParqueadero = tep.idparqueadero and t.idtransporte = tep.idTransporte;

    SELECT * FROM `transportesenparqueadero` where idParqueadero = '2';*/

    function buscarPorCorreo($correo){
        return "select idusuario
        FROM usuario 
        Where correo = '". $correo ."'";
    }



/*CONSULTA PARA Estadisticas */
    public function consultarUsuariosPorGenero(){
        return "(select 'Femenino', count(idGenero)
                    from usuario
                    where idGenero = 2)
                    union
                (select 'Masculino', count(idGenero)
                    from usuario
                    where idGenero = 3)
                    union
                (select 'Otro', count(idGenero)
                    from usuario
                    where idGenero = 4)";
    }


    public function consultarTotalUsuariosActivos(){
        return"(select 'Total Usuarios Activos', count(idUsuario)
                    from usuario
                    where estado = 0)
                    union
                (select 'Total Usuarios Pendientes', count(idUsuario)
                    from usuario
                    where estado = 2);";
    }


    public function consultarUsuariosPorEdad(){
            $currentTime = date("Y-m-d");
            $año15 = date("Y-m-d",strtotime($currentTime."- 15 year"));
        
            $año20 = date("Y-m-d",strtotime($currentTime."- 20 year"));
            
            $año25 = date("Y-m-d",strtotime($currentTime."- 25 year"));
            
            $año30 = date("Y-m-d",strtotime($currentTime."- 30 year"));
            
            $año35 = date("Y-m-d",strtotime($currentTime."- 35 year"));
            
            $año40 = date("Y-m-d",strtotime($currentTime."- 40 year"));
            
            $año45 = date("Y-m-d",strtotime($currentTime."- 45 year"));
            
            $año50 = date("Y-m-d",strtotime($currentTime."- 50 year"));
            
            $año55 = date("Y-m-d",strtotime($currentTime."- 55 year"));
            
            $año60 = date("Y-m-d",strtotime($currentTime."- 60 year"));
            
            $año65 = date("Y-m-d",strtotime($currentTime."- 65 year"));
   
           return "(select '15-20', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año15."' and fechaNacimiento > '".$año20."')
           UNION
           (select '20-25', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año20."' and fechaNacimiento > '".$año25."')
           UNION
           (select '25-30', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año25."' and fechaNacimiento > '".$año30."')
           UNION
           (select '30-35', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año30."' and fechaNacimiento > '".$año35."')
           UNION
           (select '35-40', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año35."' and fechaNacimiento > '".$año40."')
           UNION
           (select '40-45', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año40."' and fechaNacimiento > '".$año45."')
           UNION
           (select '45-50', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año45."' and fechaNacimiento > '".$año50."')
           UNION
           (select '50-55', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año50."' and fechaNacimiento > '".$año55."')
           UNION
           (select '55-60', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año55."' and fechaNacimiento > '".$año60."')
           UNION
           (select '60-65', count(idusuario)
           from usuario
           where fechaNacimiento <='".$año60."' and fechaNacimiento > '".$año65."')";
    }

    public function usuariosSinTransportes(){
        return "(select 'Sin Transportes Creados', count(u.idusuario)
                    from usuario as u
                    left JOIN transporte as t
                    on t.idUsuario = u.idusuario
                    where t.idUsuario is null
                    and u.estado = 0)";
    }

    public function usuariosGenero(){
        return "(select 'N/A con Transporte Bici', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 2
                        and t.estado = 0
                        and u.idGenero = 4
                        and u.estado = 0)
                        union
                (select 'N/A con Transporte Moto', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 3
                        and t.estado = 0
                        and u.idGenero = 4
                        and u.estado = 0)
                        union
                (select 'Hombres con Transporte Bici', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 2
                        and t.estado = 0
                        and u.idGenero = 3
                        and u.estado = 0)
                        union
                (select 'Hombres con Transporte Moto', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 3
                        and t.estado = 0
                        and u.idGenero = 3
                        and u.estado = 0)
                        union
                (select 'Mujeres con Transporte Bici', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 2
                        and t.estado = 0
                        and u.idGenero = 2
                        and u.estado = 0)
                        union
                (select 'Mujeres con Transporte Moto', count(u.idusuario)
                        from usuario as u
                        inner JOIN transporte as t
                        on t.idUsuario = u.idusuario
                        where t.idTipo = 3
                        and t.estado = 0
                        and u.idGenero = 2
                        and u.estado = 0)";
    }


}

?>
<?php
ob_start();
require_once 'modelo/Persona.php';
require_once 'modelo/Celador.php';
require_once 'modelo/Identificacion.php';
require_once 'modelo/Genero.php';

$idcelador = $_GET['idCelador'];
$celador = new Celador($idcelador);
$celador->consultar();
$i = new Identificacion($celador -> getIdTipoIdentificacion());
$i -> consultar();
$g = new Genero($celador -> getIdGenero());
$g -> consultar();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssModal/styleModal.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Celador</title>
</head>
<body>
	<div class="backgroundModal">
					<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Desatalles Celador</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<div class="tabla">
							<table class="table">
								<tbody>
									<tr>
										<th width="20%">Nombre</th>
										<td><?php echo $celador->getNombre() . " " . $celador->getApellido() ?></td>
									</tr>
									<tr>
										<th width="30%">Foto</th>
										<td><?php echo $celador -> getFoto()==""?"<img id='fotoUsuarioModal' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/".$celador -> getFoto(). " id='fotoConsultarUsuariao'>"; ?></td>
									</tr>	
									<tr>
										<th width="20%">correo</th>
										<td><?php echo $celador -> getCorreo(); ?></td>
									</tr>
									<tr>
										<th width="30%">Dirección Vivienda</th>
										<td><?php echo $celador->getDireccion() ?></td>
									</tr>
									<tr>
										<th width="30%">Teléfono de Contacto</th>
										<td><?php echo $celador->getTelefono() ?></td>
									</tr>
									<tr>
										<th width="30%">Tipo de Identificación</th>
										<td><?php echo $i->getNombreTipo() ?></td>
									</tr>
									<tr>
										<th width="30%">Número ID</th>
										<td><?php echo $celador->getNumeroID() ?></td>
									</tr>
									<tr>
										<th width="30%">Edad</th>
										<td>
											<div>
												<?php 
												$ahora = new DateTime();
												$nacimiento = new DateTime($celador -> getFechaNacimiento());
												$edad = $ahora->diff($nacimiento);
												echo $edad-> y . " años" ?>
											</div>
										</td>
									</tr>
									<tr>
										<th width="30%">Género</th>
										<td><?php echo $g->getNombreGenero() ?></td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
	</div>
	<?php unset($i);
		  unset($celador); ?>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

<?php
$error = 0;
$correo = $_POST["correo"];
$password = $_POST["password"];
$usuario = new Usuario("", "", "","", $correo, $password);
$celador = new Celador("", "", "","", $correo, $password);
$administrador = new Administrador("", "", "", $correo, $password);
if($administrador -> autenticar()){
    $_SESSION['id'] = $administrador -> getId();
    $_SESSION["rol"] = "administrador";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else if($usuario -> autenticar()){
    if($usuario -> getEstado() == 0){
        $_SESSION["id"] = $usuario -> getId();
        $_SESSION["rol"] = "usuario";
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionUsuario.php"));
    }else if($usuario -> getEstado() == 1){
        header("Location: index.php?error=2");
    }else if($usuario -> getEstado() == 3){
		$_SESSION["id"] = $usuario -> getId();
        $_SESSION["rol"] = "usuario";
        header("Location: index.php?pid=". base64_encode("presentacion/usuario/actualizarPassword.php"));
    }else if($usuario -> getEstado() == 2){
		$_SESSION["id"] = $usuario -> getId();
        $_SESSION["rol"] = "usuario";
		header("Location: index.php?pid=". base64_encode("presentacion/usuario/actualizarUsuario.php"));
	}else if($usuario -> getEstado() == 4){
        header("Location: index.php?error=3");
	}
}else if($celador -> autenticar()){
	if($celador -> getEstado()==1){
		$_SESSION["id"] = $celador -> getId();
		$_SESSION["rol"] = "celador";
		header("Location: index.php?pid=" . base64_encode("presentacion/sesionCelador.php"));
	}else{
		header("Location: index.php?error=2");
	}
}else{
    header("Location: index.php?error=1");
}

?>


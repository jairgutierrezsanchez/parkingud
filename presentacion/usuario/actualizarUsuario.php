<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();

if (isset($_POST["actualizar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $fechaNacimiento = $_POST["fechaNacimiento"];
    $direccion = $_POST["direccion"];
    $telefono = $_POST["telefono"];
    $numeroID = $_POST["numeroID"];
    $idProyecto = $_POST["idProyecto"];
    $idTipoIdentificacion = $_POST["idTipoIdentificacion"];
    $idGenero = $_POST["idGenero"];
    $numeroIDanterior = $usuario -> getNumeroID();
    $usuario = new Usuario($_SESSION["id"], $nombre, $apellido, $fechaNacimiento, "", "","", $direccion, $telefono, "", "", $numeroID, $idProyecto, $idTipoIdentificacion, $idGenero);

        if(!$usuario -> existeCedula($numeroIDanterior)){
            $usuario -> actualizar();
            $error = 1;
        }else{
            $error = 2;
        }
    }

include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';

?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
	<title>Actualizar Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Actualizar Usuario</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionActualizarUserAdmin">
        <section class="sectionActualizarUserAdmin-arriba">
            <div class="sectionActualizarUserAdmin-arriba-actualizar">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/usuario/actualizarUsuario.php")?> method="post">
                <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionForm">
                        <div class="row"> 
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu nombre" value="<?php echo $usuario->getNombre(); ?>" require> 
                            </div>
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Apellido</label>
                                <input type="text" name="apellido" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu Apellido" value="<?php echo $usuario->getApellido(); ?>" require>
                            </div> 
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Fecha de Nacimiento</label>
                                <div class="input-group date sectionActualizarUserAdmin-arriba-sectionFechas-divFecha" id="datepicker">
                                    <input id="fecha1" type="text" name="fechaNacimiento" class="form-control" autocomplete="off" value="<?php echo $usuario -> getFechaNacimiento() ?>" require>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-white d-block">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Direccion</label>
                                <input type="text" name="direccion" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu direccion" value="<?php echo $usuario->getDireccion(); ?>" require>
                            </div>
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Telefono</label>
                                <input type="number" name="telefono" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe tu teléfono" onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;" min="3000000000" max="3999999999" value="<?php echo $usuario->getTelefono(); ?>" require>
                            </div>
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="inputState">Género</label>
                                    <select id="inputState" class="form-control" name="idGenero">
                                        <?php
                                            $genero = new Genero();
                                            $generos = $genero -> consultarTodos();
                                            foreach ($generos as $g) {
                                                echo "<option value='" . $g->getIdGenero() . "'". ($g -> getIdGenero() == $usuario -> getIdGenero()?'selected':'') ." >" . $g->getNombreGenero() . "</option>";
                                            }
                                        ?>
                                    </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="inputState">Facultad</label>
                                    <select id="idFacultad" class="form-control" name="idFacultad">
                                        <?php
                                            $facultad = new Facultad();
                                            $proyectoUsuario = new Proyecto($usuario->getIdproyecto());
                                            $facultad = $facultad -> consultarTodos();
                                            $proyectoUsuario -> consultar($usuario->getIdproyecto());
                                            foreach ($facultad as $f) {
                                                echo "<option value='" . $f->getId() . "'". ($f -> getId() == $proyectoUsuario -> getIdFacultad() ?'selected':'') ." >" . $f->getNombre() . "</option>";
                                            }
                                        ?>
                                    </select>
                            </div>
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="inputState">Proyecto curricular</label>
                                    <div id="idProyecto"> 
                                    <select id="idProyecto" class="form-control" name="idProyecto">
                                            <?php
                                            $proyecto = new Proyecto("","",$proyectoUsuario -> getIdFacultad());
                                            $proyectos = $proyecto -> consultarTodosPorFacultad();
                                            foreach ($proyectos as $p) {
                                                echo "<option value='" . $p->getId() . "'". ($p -> getId() == $usuario -> getIdproyecto()?'selected':'') ." >" . $p->getNombre() . "</option>";
                                            }
                                            ?>
                                    </select>
                                    </div>
                                    
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Tipo de identificación</label>
                                    <select id="inputState" class="form-control" name="idTipoIdentificacion">
                                        <?php
                                            $identificacion = new Identificacion();
                                            $identificacions = $identificacion -> consultarTodos();
                                            foreach ($identificacions as $i) {
                                                echo "<option value='" . $i->getId() . "'". ($i -> getId() == $usuario -> getIdTipoIdentificacion()?'selected':'') ." >" . $i->getNombreTipo() . "</option>";
                                            }
                                        ?>
                                    </select>
                            </div>                 
                            <div class="form-group col-md-4 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Número de identificación</label>
                                <input type="number" name="numeroID" class="form-control" id="exampleFormControlInput1" placeholder="Escribe tu número de identificación" min="1000000" max="9999999999" onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;" value="<?php echo $usuario->getNumeroID(); ?>" require>
                            </div>
                            
                        </div>
                    </section>
                    <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert">
                        <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear">
                            <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">

                                    <?php if (isset($_POST["actualizar"])) { ?>
                                        <div class="alert alert-<?php echo ($error==1) ? "success" : "danger" ?> alert-dismissible fade show sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta" 
                                            role="alert">
                                            <?php echo ($error==1) ? "Usuario actualizado exitosamente." : "El número de identificación " . $_POST['numeroID'] . " ya existe"; ?>
                                        </div>
                                    <?php  } ?>
                                </div>
                            </section>
                            <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario" name="actualizar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                    </section>
                </form>
            </div>
        </section>
        <?php if($usuario -> getEstado() == 0){ ?>
        <section class="sectionActualizarUserAdmin-abajo">
            <div class="sectionActualizarUserAdmin-abajo-cardInfo">
                <div class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo">
                    <div class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")  ?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
        <?php } else { ?>
            <section class="sectionActualizarUserAdmin-abajo" hidden>
            <div class="sectionActualizarUserAdmin-abajo-cardInfo">
                <div class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo">
                    <div class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionActualizarUserAdmin-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")  ?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
    </div>
  
   
    <script type="text/javascript">
            var today = new Date();
            $("#datepicker").datepicker({ 
                format: 'yyyy-mm-dd',
                startDate: new Date(1950,12,31),
                endDate: new Date(today.getFullYear()-10, today.getMonth(), today.getDate())
            });
    </script>   
    <script type="text/javascript">
        $("#idFacultad").change(function() {
            var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/usuario/consultarProyectosAjax.php"); ?>&facultad="+$("#idFacultad").val()+"&usuario="+<?php echo $_SESSION["id"]?>;
                $("#idProyecto").load(ruta);	
        });
    </script>
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>       
</body>
</html>
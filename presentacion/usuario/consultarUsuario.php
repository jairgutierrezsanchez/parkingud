<?php  
if($_SESSION["rol"]=="celador"){ 
    $celador = new Celador($_SESSION["id"]); 
    $celador -> consultar(); 
    include "presentacion/celador/menuCelador.php"; 
	include 'presentacion/footer.php'; 
}else if($_SESSION["rol"]=="administrador"){ 
    $administrador = new Administrador($_SESSION["id"]); 
    $administrador -> consultar(); 
	include "presentacion/menuAdministrador.php"; 
	include 'presentacion/footer.php'; 
} 
$atributo = ""; 
$direccion = ""; 
$filas = 100; 
$pag = 1; 
if(isset($_GET["atributo"])){  
    $atributo = $_GET["atributo"]; 
} 
if(isset($_GET["direccion"])){ 
    $direccion = $_GET["direccion"]; 
} 
if(isset($_GET["pag"]) && $_GET["pag"]>0){ 
    $pag = $_GET["pag"]; 
} 
if(isset($_GET["filas"])){ 
    $filas = $_GET["filas"]; 
} 
$usuario = new Usuario(); 
$usuarios = $usuario->consultarTodos( $filas, $pag); 
$totalFilas = $usuario->consultarTotalFilas(); 
?> 
 
<!DOCTYPE html> 
<html lang="en"> 
<head> 
	<meta charset="UTF-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css"> 
 
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"  
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"> 
	<link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES--> 
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES--> 
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet"> 
  
	<title>Consultar Usuario</title> 
</head> 
<body> 
<h1 class="titulosAdmin">Listado de Usuarios</h1> 
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
	<div class="containerListadoUsuariosAdmin"> 
		<div class="containerListadoUsuariosAdmin-consulta"> 
			<div class="containerListadoUsuariosAdmin-consulta-info"> 
				<div class="containerListadoUsuariosAdmin-consulta-info-arca"> 
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-arriba"> 
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-arriba-cantidad"> 
							<select class="form-select" id="filas"> 
								<option value="100" <?php if($filas==100) echo "selected"?>>100</option> 
								<option value="200" <?php if($filas==200) echo "selected"?>>200</option> 
							</select> 
						</div> 
					</section> 
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-mitad mt-2"> 
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla"> 
							<table class="table"> 
								<thead class="table-light">  
									<tr class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead"> 
										<th scope="col" id="thPequenios">#</th> 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-foto">Foto</th> 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-nombreT">Nombre Completo</th>		 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-email">Correo</th> 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-codeStudente">Código Usuario</th> 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-estado">Estado</th> 
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-servicios">Servicios</th> 
									</tr> 
								</thead> 
								<tbody> 
									<?php 
										$p = new Proyecto(); 
										$pos = (isset($_GET["pag"]) && $_GET["pag"]!=1?((($pag-1)*$filas)+1):1); 
										foreach ($usuarios as $u) { 
											echo "<tr>"; 
											echo "<td>" . $pos ++ . "</td>"; 
											echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-foto'>" . ( ($u -> getFoto()=="")?"<img class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-fotoConsultar' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-fotoConsultar'>") . "</td>"; 
											echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-nombreTH'>" . $u->getNombre() . ' <br> ' . $u->getApellido() . "</td>"; 
											echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-email'>" . $u->getCorreo() . "</td>"; 
											echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-codeStudente'>" . $u->getCodigoEstudiantil() . "</td>"; 
											echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-estado'><span id='estado" .$u->getId() ."'  class='fas " . ($u->getEstado()==1?"fa-times-circle":(($u->getEstado()==0)?"fa-check-circle":"fa-exclamation-triangle")) . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($u->getEstado()==1?"Inhabilitado":($u->getEstado()==0?"Habilitado":"Pendiente")) . "' ></span>" . "</td>"; 
											echo "<td id='services'>" . "<a class='aIconos' href='modalUsuario.php?idUsuario=" . $u->getId() . "' data-bs-toggle='modal' data-bs-target='#modalUsuario' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a> 
																<a class='fas fa-pencil-ruler aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarUsuario-admin.php") . "&idusuario=" . $u->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>" 
															.($u -> getEstado()<=1?" <a id='cambiarEstado" . $u->getId() . "' class='aIconos fas " .($u->getEstado()==1? "fa-user-check": "fa-user-times"). "' data-bs-original-title='".($u->getEstado()==1?"Habilitar":"Desabilitar"). "' href='#' data-toggle='tooltip' data-placement='left'> </a>":"") . 
												"</td>";  
											echo "</tr>"; 
										} 
									echo"<caption id='captionRegistros'> ".count($usuarios)." registros encontrados</caption>"?> 
								</tbody> 
							</table> 
						</div>	 
					</section> 
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-abajo"> 
						<nav aria-label="Page navigation example"> 
							<ul class="pagination justify-content-center"> 
								<?php  
									$numPags = intval($totalFilas/$filas); 
									if($totalFilas%$filas != 0){ 
										$numPags++; 
									}							 
									echo ($pag!=1)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/usuario/consultarUsuario.php") . "&pag=" . ($pag-1) . "&filas=" . $filas . "'> <span aria-hidden='true' style='color:black;'>Anterior</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&laquo;</li></a>"; 
									for($i=1; $i<=$numPags; $i++){ 
										echo "<li class='page-item " . (($pag==$i)?"active":"") . "'>" . (($pag!=$i)?"<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/usuario/consultarUsuario.php") . "&pag=" . $i . "&filas=" . $filas ."'>" . $i . "</a>":"<a class='page-link'>" . $i . "</a>") . "</li>"; 
									} 
									echo ($pag!=$numPags)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/usuario/consultarUsuario.php") . "&pag=" . ($pag+1) . "&filas=" . $filas . "'> <span class='sgte' aria-hidden='true'>Siguiente</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&raquo;</a></li>"; 
								?>							 
							</ul> 
						</nav> 
					</section> 

				</div> 
			</div> 
		</div> 
		<div class="containerListadoUsuariosAdmin-consulta-infoVolver mt-2"> 
			<div class="containerListadoUsuariosAdmin-consulta-infoVolver-botones mt-1"> 
				<a class="containerListadoUsuariosAdmin-consulta-infoVolver-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuarios.php")  ?>">Volver</a> 
			</div> 
		</div> 
	</div> 
		 
     
 
<!--SCRIPTS ADICIONALES--> 
<div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
	<div class="modal-dialog modal-lg" > 
		<div class="modal-content" id="modalContent"> 
		</div> 
	</div> 
</div> 
<script> 

	$('body').on('show.bs.modal', '.modal', function (e) { 
		var link = $(e.relatedTarget); 
		$(this).find(".modal-content").load(link.attr("href")); 
	}); 
</script> 
 
 
<script type="text/javascript"> 
	<?php  
		foreach ($usuarios as $u) { 
			if($u -> getEstado() ==0|| $u -> getEstado() ==1){ 
				echo "$('#cambiarEstado" . $u -> getId() . "').click(function() {\n"; 
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/usuario/cambiarEstadoUsuarioAjax.php") . "&id=" . $u -> getId() . "';\n"; 
				echo "\t$('#cambiarEstado" . $u -> getId() . "').load(url);\n"; 
				echo "\tif($('#cambiarEstado" . $u -> getId() . "').attr('class') == 'aIconos fas fa-user-times'){\n"; 
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'aIconos fas fa-user-check');\n"; 
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('data-bs-original-title', 'Habilitar');\n"; 
				echo "\t}else{\n"; 
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'aIconos fas fa-user-times');\n"; 
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('data-bs-original-title', 'Desabilitar');\n"; 
				echo "\t}\n"; 
				echo "\tif($('#estado" . $u -> getId() . "').attr('class') == 'fas fa-times-circle'){\n"; 
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-check-circle');\n"; 
				echo "\t\t$('#estado" . $u -> getId() . "').attr('data-bs-original-title', 'Habilitado');\n"; 
				echo "\t}else{\n"; 
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-times-circle');\n"; 
				echo "\t\t$('#estado" . $u -> getId() . "').attr('data-bs-original-title', 'Inhabilitado');\n"; 
				echo "\t}\n"; 
				echo "});\n"; 
				 
				         
			} 
		} 
	?> 
</script> 
<script> 
$("#filas").change(function() { 
	var filas = $("#filas").val();  
	var url = "index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuario.php") ?>&filas=" + filas; 
	<?php if($atributo!="" && $direccion!="") { ?> 
	url += "&atributo=<?php echo $atributo ?>&direccion=<?php echo $direccion ?>";	 
	<?php } ?> 
	location.replace(url);  	 
}); 
</script> 
 
 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
		 
</body> 
</html>
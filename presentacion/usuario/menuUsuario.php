<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssBarra/menuAdminStyles.css">
    <!-- Boxiocns CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--FUENTES-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--FUENTES-->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

    <title>Document</title>
</head>

<body>
    <div class="sidebar close">
        <div class="logo-details">  
            <i class='bx bx-menu'></i>
            <span class="logo_name">UDistrital</span>
        </div>
        <ul class="nav-links"><?php if($usuario -> getEstado() == 0){ ?>
                <li>
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/sesionUsuario.php")?>">
                        <i class='bx bx-grid-alt'></i>
                        <span class="link_name">Inicio</span>
                    </a>
                </li>
                <li>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/pagSeleccionEspacios.php")?>">
                            <i class='bx bx-car'></i>
                            <span class="link_name">Transporte</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/transporte/elegirTransporte.php")?>">Crear Transporte</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/elegirConsultarTransporte.php")?>">Consultar Transporte</a></li>
                    </ul>
                </li>
                <li>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/elegirOpcionUsers.php")?>">
                            <i class='bx bx-history'></i>
                            <span class="link_name">Historial</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialParqueaderoUsers.php")?>">Historial parqueadero</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialActualUser.php")?>">Historial Actual</a></li>
                    </ul>
                </li>
                <li>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">
                            <i class="fas fa-users-cog"></i>
                            <span class="link_name">Perfil</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">Ver Perfil</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarPassword.php")?>">Actualizar Contraseña</a></li>
                    </ul>
                </li>
                <li>
                    <div class="profile-details">
                        <div class="profile-content">
                            <img  src="./<?php echo ($usuario->getFoto()!=null?$usuario -> getFoto():"imgFotosPerfil/imagenBase.png")?>" alt="profileImg">
                        </div>
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">
                                <div class="name-job">
                                    <div class="profile_name">Usuario</div>
                                        <div class="job"><?php echo $usuario -> getNombre() ?></div>
                                </div>
                            </a>
                        <a href="index.php?pid=<?php echo base64_encode("destruirSesion.php") ?>">
                            <i class='bx bx-log-out'></i></a>
                    </div>
                </li>
            <?php } else { ?>
                <li hidden>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/pagSeleccionEspacios.php")?>">
                            <i class='bx bx-car'></i>
                            <span class="link_name">Transporte</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/transporte/elegirTransporte.php")?>">Crear Transporte</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/elegirConsultarTransporte.php")?>">Consultar Transporte</a></li>
                    </ul>
                </li>
                <li hidden>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/elegirOpcionUsers.php")?>">
                            <i class='bx bx-history'></i>
                            <span class="link_name">Historial</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialParqueaderoUsers.php")?>">Historial parqueadero</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialActualUser.php")?>">Historial Actual</a></li>
                    </ul>
                </li>
                <li hidden>
                    <div class="iocn-link">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">
                            <i class="fas fa-users-cog"></i>
                            <span class="link_name">Perfil</span>
                        </a>
                        <i class='bx bxs-chevron-down arrow'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">Ver Perfil</a></li>
                        <li><a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarPassword.php")?>">Actualizar Contraseña</a></li>
                    </ul>
                </li>
                <li>
                    <div class="profile-details">
                        <div class="profile-content">
                            <img  src="./<?php echo ($usuario->getFoto()!=null?$usuario -> getFoto():"imgFotosPerfil/imagenBase.png")?>" alt="profileImg">
                        </div>
                            <a hidden href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php")?>">
                                <div class="name-job">
                                    <div class="profile_name">Usuario</div>
                                        <div class="job"><?php echo $usuario -> getNombre() ?></div>
                                </div>
                            </a>
                        <a href="index.php?pid=<?php echo base64_encode("destruirSesion.php") ?>">
                            <i class='bx bx-log-out'></i></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>



    <script type="text/javascript">
        let arrow = document.querySelectorAll(".arrow");
        for (var i = 0; i < arrow.length; i++) {
        arrow[i].addEventListener("click", (e)=>{
        let arrowParent = e.target.parentElement.parentElement;//selecting main parent of arrow
        arrowParent.classList.toggle("showMenu");
        });
        }

        let sidebar = document.querySelector(".sidebar");
        let sidebarBtn = document.querySelector(".bx-menu");
        console.log(sidebarBtn);
        sidebarBtn.addEventListener("click", ()=>{
        sidebar.classList.toggle("close");
        });
    </script>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>


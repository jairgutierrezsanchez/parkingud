<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$proyecto = new Proyecto();
$identificacion = new Identificacion();

include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';


$error = 0;
if(isset($_POST['cambiar'])){
    $usuario = new Usuario($_SESSION['id'], "","","","", $_POST['clavea'], "", "", "", "", "", "", "", "", "");
    if($_POST['claven']==$_POST['clavenc']){
        $claven = $_POST['claven'];
        $mayusculas  =  preg_match ( '/[A-Z]+/' ,  $claven ); 
        $minusculas  =  preg_match ( '/[a-z]+/' ,  $claven ); 
        $number     =  preg_match ( '/[0-9]+/' ,  $claven ); 
        $specialChars  =  preg_match ( "/[\p{P}\p{S}]+/" ,  $claven );  
        if($mayusculas  &&  $minusculas  && $number  &&  $specialChars  && strlen ( $claven ) >  8 ){
                if ( $claven != $_POST['clavea'] || $_POST['clavenc'] != $_POST['clavea']){
                    $usuario = new Usuario($_SESSION['id'], "","","","", $_POST['claven'], "", "", "", "", "", "", "", "", "");
                        $usuario-> actualizarClave();
                    
                }else{
                    $error = 2;
                }
                
            }else{
                $error = 3;
            }
        }else{
            $error=1;
        }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
<body> 
<h1 class="titulosUsers">Cambiar Clave </h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="actualizarPassUser">
    <section class="actualizarPassUser-arriba">
        <div class="actualizarPassUser-arriba-crearPass" style="width: 460px !important; height: 450px !important">
            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar"> 
                <form  action= <?php echo "index.php?pid=" . base64_encode("presentacion/usuario/actualizarPassword.php") ?> method="post" enctype="multipart/form-data">
                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear" style="height: 230px !important;">
                        <div class="row mt-1">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label mt-1">
                                <label for="inputState">Clave antigua</label>
                                <div class="input-group">    
                                    <input name="clavea" id="password1" type="password" class="form-control" placeholder="Digite clave Actual" required="required">
                                    <button id="show_password1" class="btn btn-dark" type="button" onclick="mostrarPassword1()"> 
                                        <span id="ojo1" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label">
                                <label for="inputState">Clave nueva</label>
                                <div class="input-group">
                                    <input name="claven" id="password2" type="password" class="form-control" placeholder="Digite clave Nueva" required="required">
                                    <button id="show_password2" class="btn btn-dark" type="button" onclick="mostrarPassword2()"> 
                                        <span id="ojo2" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label">
                                <label for="inputState">Confirmar clave</label>
                                <div class="input-group"> 
                                    <input name="clavenc" id="password3" type="password" class="form-control" placeholder="Confirmar clave" required="required">
                                    <button id="show_password3" class="btn btn-dark" type="button" onclick="mostrarPassword3()"> 
                                        <span id="ojo3" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                    </section>

                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection">
                        <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection" style="display: flex; flex-direction: column;">
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection-alert">
                                    <?php 
                                        if (isset($_POST['cambiar'])) { 
                                    ?>
                                        <div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show"
                                        role="alert" style="height: 100px;" >
                                        <?php if ($error==0) {
                                            echo "Cambio exitoso";
                                        }else if($error == 1){
                                            echo "Las claves no coinciden";
                                        }else if($error == 2){
                                            echo "Clave actual es igual a la anterior";
                                        }else{
                                            echo "La contraseña debe tener minimo 8 caracteres, una mayuscula y minuscula,\n
                                            un caracter especial y un número";
                                        } ?>
                                    </div>
                                    <?php }?>
                                </div>
                            </section>
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton">
                                    <button class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton-botonCrearTransporte mt-5" type="submit" name="cambiar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </section>
        </div>
        <?php $usuario = new Usuario($_SESSION['id']);
                $usuario -> consultar(); 
                if($usuario -> getEstado() != 3 && $usuario -> getEstado() != ''){ ?>
            <div class="actualizarPassUser-arriba-botonBack mt-1">
                <div class="actualizarPassUser-arriba-botonBack-bCI">
                    <div class="actualizarPassUser-arriba-botonBack-bCI-botones">
                        <a class="actualizarPassUser-arriba-botonBack-bCI-botones-linkCard"   
                            href="index.php?pid=<?php echo base64_encode("presentacion/usuario/perfilUsuario.php") ?>">
                            Volver</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
</div>

</body>

<script type="text/javascript">
function mostrarPassword1(){
		var cambio = document.getElementById("password1");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#ojo1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#ojo1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
    function mostrarPassword2(){
		var cambio = document.getElementById("password2");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#ojo2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#ojo2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
    function mostrarPassword3(){
		var cambio = document.getElementById("password3");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#ojo3').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#ojo3').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
</script>

</html>
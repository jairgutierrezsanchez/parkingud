<?php
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$proyecto = new Proyecto();
$identificacion = new Identificacion();
$genero = new Genero();

include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
	<title>Perfil Usuario</title>
</head>
<body>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerPC">
    <div class="containerPC-cardPC">
        <section class="containerPC-cardPC-tituloPC">
            <h1 class="containerPC-cardPC-tituloPC-tituloProfile">Perfil Usuario</h1>
        </section>
        <section class="containerPC-cardPC-infoPC">
            <section class="containerPC-cardPC-infoPC-sectionIzq">
                <section class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo">
                    <h5 class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo-nameCelador">
                        <?php echo $usuario -> getNombre() . " " . $usuario -> getApellido() ?>
                    </h5>
                    <div class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo-infoCelador">
                        <div>
                            <?php echo "Correo: ". $usuario -> getCorreo() ?> 
                        </div> 
                        <div>
                            <?php echo "Código Estudiantil: ". $usuario -> getCodigoEstudiantil() ?> 
                        </div>
                        <div>
                            <?php echo "Dirección de su hogar: ". $usuario -> getDireccion() ?> 
                        </div>
                        <div>
                            <?php echo "Número de contacto: ". $usuario -> getTelefono() ?> 
                        </div>
                        <div>
                            <?php $identificacion -> setId($usuario -> getIdTipoIdentificacion()) ?>
                            <?php $identificacion -> consultar() ?> 
                            <?php $identificacion -> getNombreTipo() ?>
                        </div>
                        <div> <?php $identificacion -> setId($usuario -> getIdTipoIdentificacion()) ?>
                            <?php $identificacion -> consultar() ?> 
                            <?php echo $identificacion -> getNombreTipo() . ": ". $usuario -> getNumeroID() ?>
                        </div>
                        <div>
                            <?php $genero -> setId($usuario -> getIdGenero()) ?>
                            <?php $genero -> consultar() ?> 
                            <?php echo "Género: ". $genero -> getNombreGenero() ?>
                        </div>
                        <div>
                            <?php 
                            $ahora = new DateTime();
                            $nacimiento = new DateTime($usuario -> getFechaNacimiento());
                            $edad = $ahora->diff($nacimiento);
                            echo "Edad ". $edad-> y; ?>
                        </div>
                    </div>
                </section>
                <section class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo mt-4">
                    <a class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo-linkCard" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarUsuario.php")?>">Actualizar datos</a>
                        
                    <a class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo-linkCard mt-2" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarPassword.php")?>">Actualizar Contraseña</a>
                </section>
            </section>
            <section class="containerPC-cardPC-infoPC-sectionDer">
                <section class="containerPC-cardPC-infoPC-sectionDer-fotoVer">
                    <a href="#!" data-bs-toggle="modal" data-bs-target="#modalVerFoto">
                        <img class="containerPC-cardPC-infoPC-sectionDer-fotoVer-fotoVerFoto" 
                        src="./<?php echo ($usuario->getFoto()!=null?$usuario -> getFoto():"imgFotosPerfil/imagenBase.png")?>"></img>
                    </a> 
                </section>
                <section class="containerPC-cardPC-infoPC-sectionDer-botonFoto">
                    <a class="containerPC-cardPC-infoPC-sectionDer-botonFoto-botonAFC" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/usuario/actualizarFotoUsuario.php")?>">
                        Actualizar Foto de Perfil
                    </a>
                </section>
            </section>
        </section>
    </div>
</div>

<div tabindex="-1" aria-labelledby="modalVerFoto" aria-hidden="true" class="modal fade" id="modalVerFoto">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-contentt" id="modal-contentt" >
            <img class="containerFotoModal" id="fotoModal" src="/parking/parkingud/<?php echo $usuario->getFoto()?>" alt="" >
        </div> 
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}
$filas = 5;
$pag = 1;
if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}


$usuario = new Usuario();
$usuarios = $usuario->consultarTodos( $filas, $pag);
$totalFilas = $usuario->consultarTotalFilas();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
 
	<title>Consultar Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Consultar Usuario</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerUsuariosAdmin">
        <div class="containerUsuariosAdmin-cuerpo">
			<div class="containerUsuariosAdmin-cuerpo-cardParqueadero">
                <div class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar">
                    <div class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar">
                        <div class="form-group containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras">
                            <label class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-labelBuscar mb-2" id="userCode">Buscar usuarios: </label> 
                            <input name="filtro" id="filtro" type="number" class="form-control containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-inputt" placeholder="Buscar usuario por Código"
                             onKeyDown="if(this.value.length==11 && event.keyCode!=8) return false;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="containerUsuariosAdmin-cuerpo-cardInfo mt-3 mb-3">
				<div class="containerUsuariosAdmin-cuerpo-cardInfo-tabla" id="resultados" ></div>
            </div>
            <div class="containerUsuariosAdmin-cuerpo-botonn">
				<div class="containerUsuariosAdmin-cuerpo-botonn-botoness">
					<a class="containerUsuariosAdmin-cuerpo-botonn-botoness-linkCardd" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/usuario/pagSeleccionUsuario.php"):base64_encode("presentacion/usuario/pagSeleccionUsuario.php"))  ?>">Volver</a>
					<a class="containerUsuariosAdmin-cuerpo-botonn-botoness-linkCardd" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuario.php")  ?>">Listado de Usuarios</a>
				</div>
		    </div>

           
        </div>
    </div>
    <div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
	<div class="modal-dialog modal-lg" > 
		<div class="modal-content" id="modalContent"> 
		</div> 
	</div> 
    
<script>
	$('body').on('shown.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
    

<!--SCRIPTS ADICIONALES-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#filtro").keyup(function(){
			var tam=$("#filtro").val().length;
            if(tam>=11){
                var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuariosAjax.php"); ?>&filtro="+$("#filtro").val();
                $("#resultados").load(ruta);
            }
        });
    });
</script>




	
 

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		
</body>
</html>
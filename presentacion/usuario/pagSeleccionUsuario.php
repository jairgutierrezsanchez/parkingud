<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
	<title>Perfil Usuario</title>
</head>
<body>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerSeleccion">
    <div class="containerSeleccion-elegir">
        <section class="containerSeleccion-elegir-izqCard">
            <div class="containerSeleccion-elegir-izqCard-imagen">
                <i class="fas fa-search"></i>
            </div>
        </section>
        <section class="containerSeleccion-elegir-derechaCard">
            <section class="containerSeleccion-elegir-derechaCard-infoArriba">
                <h5 class="containerSeleccion-elegir-derechaCard-infoArriba-titulo">Consultar Usuarios</h5>   
            </section>
            <section class="containerSeleccion-elegir-derechaCard-infoAbajo">
                <div class="containerSeleccion-elegir-derechaCard-infoAbajo-botonCard">
                    <a class="containerSeleccion-elegir-derechaCard-infoAbajo-botonCard-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuarios.php")?>">Consultar Usuarios</a>
                </div> 
            </section>
        </section>
    </div>
</div>




	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
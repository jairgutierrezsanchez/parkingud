<?php
    $filtro = $_GET['filtro'];
    $usuario = new Usuario();
    $usuarios = $usuario -> buscar($filtro);
    if(count($usuarios)>0){
?>
<script charset="utf-8">
        $(function () { 
        	$("[data-toggle='tooltip']").tooltip(); 
        });
    </script>

	<div class="tablaAjax">
		<table class="table hey mt-3">
			<thead class="table-dark"> 
				<tr>
					<th scope="col">#</th>
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-foto">Foto</th>
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-nombre">Nombre Completo</th>		
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-email">Correo</th>
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-codeStudente">Código Usuario</th>
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-estado">Estado</th>
					<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-services">Servicios</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$pos = (isset($_GET["pag"]) && $_GET["pag"]!=1?((($pag-1)*$filas)+1):1);
					
					foreach ($usuarios as $u) {
						echo "<tr>";
						echo "<td>" . $pos ++ . "</td>";
						echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-foto'>" . ( ($u -> getFoto()=="")?"<img class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-fotoConsultar' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-fotoConsultar'>") . "</td>";
						echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-nombreTH'>" . $u->getNombre() . ' <br> ' . $u->getApellido() . "</td>";
						echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-email'>" . $u->getCorreo() . "</td>";
						echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-codeStudente'>" . $u->getCodigoEstudiantil() . "</td>";
						echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-estado'><span id='estado" .$u->getId() ."'  class='fas " . ($u->getEstado()==1?"fa-times-circle":(($u->getEstado()==0)?"fa-check-circle":"fa-exclamation-triangle")) . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($u->getEstado()==1?"Inhabilitado":($u->getEstado()==0?"Habilitado":"Pendiente")) . "' ></span>" . "</td>";
						echo "<td id='services'>" . "<a class='aIconos' href='modalUsuario.php?idUsuario=" . $u->getId() . "' data-bs-toggle='modal' data-bs-target='#modalPaciente' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
											<a class='fas fa-pencil-ruler aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarUsuario-admin.php") . "&idusuario=" . $u->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>"
										.($u -> getEstado()<=1?" <a id='cambiarEstado" . $u->getId() . "' class='aIconos fas " .($u->getEstado()==1? "fa-user-check": "fa-user-times"). "' data-bs-original-title='".($u->getEstado()==1?"Habilitar":"Desabilitar"). "' href='#' data-toggle='tooltip' data-placement='left'> </a>":"") .
							"</td>"; 
						echo "</tr>";
					}
					echo "<caption style='margin-left:15px'>" . count($usuarios) . " registros encontrados</caption>"?>
			</tbody>
		</table>
	</div>
    <?php } else { ?>
        <div class="alert alert-danger alert-dismissible fade show containerUsuariosAdmin-cuerpo-cardInfo-tabla-alertaNoResultados" role="alert">
            No se encontraron resultados
        </div>
    <?php } ?>



<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>


<script type="text/javascript">
	<?php 
		foreach ($usuarios as $u) {
			if($u -> getEstado() ==0|| $u -> getEstado() ==1){
				echo "$('#cambiarEstado" . $u -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/usuario/cambiarEstadoUsuarioAjax.php") . "&id=" . $u -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $u -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $u -> getId() . "').attr('class') == 'aIconos fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'aIconos fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('data-bs-original-title', 'Habilitar');\n"; 
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'aIconos fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('data-bs-original-title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $u -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('data-bs-original-title', 'Habilitado');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('data-bs-original-title', 'Inhabilitado');\n";
				echo "\t}\n";
				echo "});\n";
				

				
				        
			}
		}
	?>
</script>
<script>
$("#filas").change(function() {
	var filas = $("#filas").val(); 
	var url = "index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuarios.php") ?>&filas=" + filas;
	<?php if($atributo!="" && $direccion!="") { ?>
	url += "&atributo=<?php echo $atributo ?>&direccion=<?php echo $direccion ?>";	
	<?php } ?>
	location.replace(url);  	
});
</script>
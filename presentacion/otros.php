<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Elija qué Reporte desea Consultar</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerEOPAdmin">
    <div class="containerEOPAdmin-infoEOP">
        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historial/historialParqueadero.php")?>">
            <div class="card containerEOPAdmin-infoEOP-elegirP mr-5">
                <i class='bx bx-history'></i>
                <div class="card-footer bg-transparent ">Historial Parqueadero</div>
            </div>
        </a> 
        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/consultar/consultarTransportesParqueadero.php")?>">
            <div class="card containerEOPAdmin-infoEOP-elegirP mr-5">
                <i class='bx bxs-edit-location' ></i>
                <div class="card-footer bg-transparent ">Información<br>de Parqueaderos</div>
            </div>
        </a> 
        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Reportes/elegirOpcionReportes.php")?>">
            <div class="card containerEOPAdmin-infoEOP-elegirP mr-5">
            <i class='bx bxs-pie-chart-alt-2'></i>
                <div class="card-footer bg-transparent ">Reportes</div>
            </div>
        </a>

        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/pendientes/paginaSeleccionarTrasnporte.php")?>">
            <div class="card containerEOPAdmin-infoEOP-elegirP mr-5">
                <i class='bx bx-error-circle'></i>
                <div class="card-footer bg-transparent ">Transportes<br>Pendientes</div>
            </div>
        </a>
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
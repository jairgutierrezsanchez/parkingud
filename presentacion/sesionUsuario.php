<?php
$usuario = new Usuario($_SESSION['id']);
$usuario->consultar();
include "presentacion/usuario/menuUsuario.php";
include 'presentacion/footer.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--FUENTES-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--FUENTES-->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <title>Document</title>
</head>

<body>
<h1 class="titulosUsers">Bienvenido, Usuario: <?php echo $usuario -> getNombre() . " " . $usuario -> getApellido() ?> </h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="inicioUsers">
        <div class="inicioUsers-sectionContainer">
            <div class="inicioUsers-sectionContainer-cardsInicio">
                <section class="inicioUsers-sectionContainer-cardsInicio-imgInicio">
					<div class="inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito">
						<img id="imagenInicio" class="card-img-top inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito-imageTransport" 
                                src="./img/fotoUnoCarousel.jpg" alt="...">
					</div>
                </section>
                <section class="inicioUsers-sectionContainer-cardsInicio-linkInicio">
                    <div class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio mt-1">
                        <a id="linkInicio" class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio-linkCardd" 
                            href="https://tecnologica.udistrital.edu.co/course/index.php" target="_blank">Aulas Virtuales</a>
                    </div>
                </section>
            </div>
            <div class="inicioUsers-sectionContainer-cardsInicio">
                <section class="inicioUsers-sectionContainer-cardsInicio-imgInicio">
					<div class="inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito">
						<img id="imagenInicio" class="card-img-top inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito-imageTransport" 
                                src="./img/fotoDosCarousel.jpg" alt="...">
					</div>
                </section>
                <section class="inicioUsers-sectionContainer-cardsInicio-linkInicio">
                    <div class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio mt-1">
                        <a id="linkInicio" class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio-linkCardd" 
                            href="https://estudiantes.portaloas.udistrital.edu.co/appserv/" target="_blank">Cóndor UD</a>
                    </div>
                </section>
            </div>
            <div class="inicioUsers-sectionContainer-cardsInicio">
                <section class="inicioUsers-sectionContainer-cardsInicio-imgInicio">
					<div class="inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito">
						<img id="imagenInicio" class="card-img-top inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito-imageTransport" 
                                src="./img/fotoTresCarousel.jpg" alt="...">
					</div>
                </section>
                <section class="inicioUsers-sectionContainer-cardsInicio-linkInicio">
                    <div class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio mt-1">
                        <a id="linkInicio" class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio-linkCardd" 
                            href="https://moodleilud.udistrital.edu.co/" target="_blank">ILUD</a>
                    </div>
                </section>
            </div>
            <div class="inicioUsers-sectionContainer-cardsInicio">
                <section class="inicioUsers-sectionContainer-cardsInicio-imgInicio">
					<div class="inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito">
						<img id="imagenInicio" class="card-img-top inicioUsers-sectionContainer-cardsInicio-imgInicio-fotito-imageTransport" 
                                src="./img/fotoCuatroCarousel.jpg" alt="...">
					</div>
                </section>
                <section class="inicioUsers-sectionContainer-cardsInicio-linkInicio">
                    <div class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio mt-1">
                        <a id="linkInicio" class="inicioUsers-sectionContainer-cardsInicio-linkInicio-botonInicio-linkCardd" 
                            href="https://www.udistrital.edu.co/inicio" target="_blank">Ir Página UD</a>
                    </div>
                </section>
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
        integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
</body>

</html>
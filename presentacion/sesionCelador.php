<?php
$celador = new Celador($_SESSION['id']);
$celador->consultar();
include 'presentacion/celador/menuCelador.php';
include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Sesión Celador</title>
</head>
<body>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<h1 class="titulosAdmin">Bienvenido, celador: <?php echo $celador -> getNombre() . " " . $celador -> getApellido() ?> </h1>
<div class="containerSC">
    <div class="containerSC-cardSC">
        <section class="containerSC-cardSC-parrafoSC">
        El uso de este portal es exclusivo. Por favor, usar con mucho cuidado y atención para no generar inconvenientes.
        </section>
        <section class="containerSC-cardSC-fechaSC">
            <h3>
                <?php
                    date_default_timezone_set('America/Bogota');
                    $fechaActual = date('d/m/y H:i');
                    echo "Fecha Actual: " . $fechaActual;
                ?>
            </h3>
        </section>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
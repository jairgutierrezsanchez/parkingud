<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
$identificacion = new Identificacion();
include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">
    <title>Document</title>
</head>
<body>
<h1 class="titulosAdmin">Perfil Administrador</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="perfilAdmin">
        <div class="perfilAdmin-imagen">
                <img src="./<?php echo ($administrador->getFoto()!=null?$administrador -> getFoto():"imgFotosCelador/imagenBase.png")?>" height="200px"></img>
        </div>
        <div class="perfilAdmin-cardPerfiles">
            <div class="perfilAdmin-cardPerfiles-containerPerfilUsuario">
                <h5 class="card-title perfilAdmin-cardPerfiles-containerPerfilUsuario-nombreTitulo"><?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></h5>
                <div class="card-text perfilAdmin-cardPerfiles-containerPerfilUsuario-infoUsuario">
                    <div>
                        <?php echo "Correo: ". $administrador -> getCorreo() ?> 
                    </div>  
                </div>

                <div class="perfilAdmin-cardPerfiles-containerPerfilUsuario-botonCard mt-4">
                    <a class="perfilAdmin-cardPerfiles-containerPerfilUsuario-botonCard-linkCard" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/actualizarPerfilAdmin.php")?>">Actualizar datos</a>
                </div> 
                <div class="perfilAdmin-cardPerfiles-containerPerfilUsuario-botonCard mt-3">
                    <a class="perfilAdmin-cardPerfiles-containerPerfilUsuario-botonCard-linkCard" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/actualizarFotoAdmin.php")?>">Actualizar Foto de Perfil</a> 
                </div> 
        </div>
    </div>



 

</body>
</html>
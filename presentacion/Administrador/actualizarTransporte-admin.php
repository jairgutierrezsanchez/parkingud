<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}

$transporte = new Transporte($_GET["idtransporte"]);
$transporte -> consultar();
if (isset($_POST["actualizar"]) && $transporte -> existeSerialAdmin($transporte -> getSerial())) {
    $serial = $_POST["serial"];
    $modelo = $_POST["modelo"];  
    $descripcion = $_POST["descripcion"];
    $idColor = $_POST["idcolor"];
    $idMarca = $_POST["idmarca"];
    $transporte = new Transporte($_GET["idtransporte"], $serial, $modelo, "", "", $descripcion, "", $idColor, ($_GET["idtipo"]=='Moto'?3:2), $transporte -> getIdusuario(), $idMarca, "");
    $transporte->actualizar();
    $error =1;
}else{
    $error = 2;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Actualizar Usuario</title>
</head>

<body>
<h1 class="titulosAdmin"><?php echo "Actualizar ". ($_GET["idtipo"]);?></h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionATA">
        <section class="sectionATA-arriba">
            <div class="sectionATA-arriba-actualizar">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Administrador/actualizarTransporte-admin.php")."&idtransporte=".$_GET["idtransporte"]."&idtipo=".$_GET["idtipo"] ?> method="post">
               
                    <section class="sectionATA-arriba-actualizar-sectionForm">
                        <div class="row">
                            <div class="form-group col-md-4 sectionATA-arriba-actualizar-sectionForm-label">
                                <label for="inputState"><?php echo (($_GET["idtipo"]=="Moto")?"Placa":"Serial")?></label>
                                <input type="text" name="serial" class="form-control" id="exampleFormControlInput1" value=<?php echo  $transporte -> getSerial()?>  <?php echo (($_GET["idtipo"]=="Moto")?"minlength='6' maxlength='6' size='6'":"minlength='6' maxlength='10' size='10'")?> required = "required">
                            </div>
                            <div class="form-group col-md-4 sectionATA-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Modelo</label>
                                <?php if($_GET["idtipo"]=="Bicicleta"){
                                        echo "<input type='text' name='modelo' class='form-control' id='exampleFormControlInput1' placeholder='Escribe el modelo' required = 'required' value ='" . $transporte -> getModelo() ."'>";
                                    }else{
                                            $year = date("Y");
                                            echo "<select id='inputState' class='form-control' name='modelo'>";
                                            for($i=1995 ; $i<=$year+1; $i++){
                                                echo "<option".($i == $transporte-> getModelo()?'selected':'')." value= " . $i . ">" . $i . "</option>";
                                            }
                                            echo "</select>";
                                }?>
                            </div> 
                            <div class="form-group col-md-4 sectionATA-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Descripción</label>
                                <input type="text" name="descripcion" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe detalles de tu transporte" value="<?php echo $transporte->getDescripcion(); ?>">
                            </div>
                        </div>
                        <div class="row mt-2">
                            
                            <div class="form-group col-md-4 sectionATA-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Color</label>
                                <select id="inputState" class="form-control" name="idcolor">
                                    <?php
                                        $color = new Color();
                                        $colores = $color -> consultarTodos();
                                        foreach ($colores as $c) {
                                            echo "<option value='" . $c->getIdC() . "'".($c -> getIdC() == $transporte -> getIdcolor()?'selected':''). ">" . $c->getColor() . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 sectionATA-arriba-actualizar-sectionForm-label">
                                <label for="inputState">Marca</label>
                                <select id="inputState" class="form-control" name="idmarca">
                                    <?php
                                        $marca = new Marca();
                                        $marcas = (($_GET["idtipo"]=="Bicicleta")?$marca -> consultarMotosTodos():$marca -> consultarBicisTodos());
                                        foreach ($marcas as $m) {
                                            echo "<option value='" . $m->getId() . "'". ($m -> getId() == $transporte -> getMarca()?'selected':'') ." >" . $m->getNombre() . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </section>
                    <section class="sectionATA-arriba-actualizar-sectionButtonAlert">
                        <div class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear">
                            <section
                                class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div
                                    class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">

                                    <?php if (isset($_POST["actualizar"]) && $error = 1){ ?>
                                    <div class="alert alert-success alert-dismissible fade show sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta"
                                        role="alert">Transporte actualizado exitosamente.</div>
                                    <?php }else if(isset($_POST["actualizar"]) && $error=2){ ?>
                                        <div class="alert alert-danger alert-dismissible fade show sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta"
                                        role="alert">El serial ya existe.</div>
                                    <?php  } ?>
                                </div>
                            </section>
                            <section
                                class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div
                                    class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button
                                        class="sectionATA-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario"
                                        name="actualizar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </div>
        </section>
        <section class="sectionATA-abajo">
            <div class="sectionATA-abajo-cardInfo">
                <div class="sectionATA-abajo-cardInfo-cajitaInfo">
                    <div class="sectionATA-abajo-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionATA-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo ($_GET["idtipo"]=="Bicicleta"? base64_encode("presentacion/Transporte/consultarTodosBicicletas.php"): base64_encode("presentacion/Transporte/consultarTodosMotos.php") )?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
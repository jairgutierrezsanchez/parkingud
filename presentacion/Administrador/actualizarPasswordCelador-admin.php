<?php 
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();


$celador = new Celador($_GET["idcelador"]);
$celador -> consultar();
if(isset($_POST["registrarClaveNueva"])){
    $password = $_POST["password"];

    $celador = new Celador($_GET["idcelador"], "", "", "", "", $password, "", "", "", "", "", "", "", "");
    $celador -> registrarClaveNueva();
        
}


include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
<body> 
<h1 class="titulosUsers">Cambiar Clave</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="actualizarPassUser">
    <section class="actualizarPassUser-arriba">
        <div class="actualizarPassUser-arriba-crearPass">
            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar"> 
                <form  action= <?php echo "index.php?pid=" . base64_encode("presentacion/Administrador/actualizarPasswordCelador-admin.php")."&idcelador=".$_GET["idcelador"] ?> method="post" enctype="multipart/form-data">
                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear">
                        <div class="row mt-2">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label mt-1">
                                <label for="inputState">Clave nueva</label>
                                <div class="input-group">    
                                    <input name="password" id="txtPassword" type="password" class="form-control" placeholder="Digite clave nueva" required="required">
                                    <button id="show_password" class="btn btn-dark" type="button" onclick="mostrarPassword()"> 
                                        <span class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection">
                        <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection">
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection-alert">
                                    <?php if (isset($_POST['registrarClaveNueva'])) { ?>
                                        <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection-alert-alertaPass alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show" 
                                            role="alert">Cambio exitoso</div>
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton">
                                    <button class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton-botonCrearTransporte" type="submit" name="registrarClaveNueva">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </section>
        </div>
        <div class="actualizarPassUser-arriba-botonBack mt-1">
            <div class="actualizarPassUser-arriba-botonBack-bCI">
                <div class="actualizarPassUser-arriba-botonBack-bCI-botones">
                    <a class="actualizarPassUser-arriba-botonBack-bCI-botones-linkCard"   
                        href="index.php?pid=<?php echo base64_encode("presentacion/celador/consultarCeladores.php") ?>">
                        Volver</a>
                </div>
            </div>
        </div>
    </section>
</div>



<script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("txtPassword");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
    $('#ShowPasswordd').click(function () {
		$('#Passwordd').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
});
</script>
</body>

</html>
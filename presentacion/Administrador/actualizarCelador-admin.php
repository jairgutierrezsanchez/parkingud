<?php 
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

$celador = new Celador($_GET["idcelador"]);
$celador -> consultar();
if (isset($_POST["actualizar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $fechaNacimiento = $_POST["fechaNacimiento"];
    $direccion = $_POST["direccion"];
    $telefono = $_POST["telefono"];
    $numeroID = $_POST["numeroID"];
    $idTipoIdentificacion = $_POST["idTipoIdentificacion"];
    $idGenero = $_POST["idGenero"];

    $celador = new Celador($_GET["idcelador"], $nombre, $apellido, $fechaNacimiento,"", "", $direccion, $telefono, $numeroID, "", "", $idTipoIdentificacion, "", $idGenero);
    if(!$celador -> existeCedula()){
        $celador -> actualizar();
        $error = 1;
    }else{
        $error = 2;
    }
}
include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Actualizar Celador</title>
</head>
<body>
<h1 class="titulosAdmin">Actualizar Celador</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionActualizarUserAdmin">
        <section class="sectionActualizarUserAdmin-arriba">
            <div class="sectionActualizarUserAdmin-arriba-actualizar">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Administrador/actualizarCelador-admin.php")."&idcelador=".$_GET["idcelador"] ?> method="post">
                    <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionForm">
                        <div class="row">
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="exampleFormControlInput1"
                                        placeholder="Escribe el nombre" value="<?php echo $celador->getNombre(); ?>" required="">
                            </div>
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Apellido</label>
                                <input type="text" name="apellido" class="form-control" id="exampleFormControlInput1"
                                        placeholder="Escribe el Apellido" value="<?php echo $celador->getApellido(); ?>" required>
                            </div>
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Direccion</label>
                                <input type="text" name="direccion" class="form-control" id="exampleFormControlInput1"
                                        placeholder="Escribe la dirección" value="<?php echo $celador->getDireccion(); ?>" required>
                            </div>
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Telefono</label>
                                <input type="number" name="telefono" class="form-control" id="exampleFormControlInput1"
                                        placeholder="Escribe el teléfono" onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;" min="3000000000" max="3999999999" value="<?php echo $celador->getTelefono(); ?>" required>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="inputState">Género</label>
                                    <select  class="form-control" name="idGenero">
                                        <?php
                                            $genero = new Genero();
                                            $generos = $genero -> consultarTodos();
                                            foreach ($generos as $g) {
                                                echo "<option value='" . $g->getIdGenero() . "'". ($g -> getIdGenero() == $celador -> getIdGenero()?'selected':'') ." >" . $g->getNombreGenero() . "</option>";
                                            }
                                        ?>
                                    </select>
                            </div>
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-fecha">
                                <label for="exampleFormControlInput1">Fecha de Nacimiento</label>
                                <div class="input-group date sectionHistorial-arriba-sectionFechas-divFecha" id="datepicker">
                                    <input type="text" name="fechaNacimiento" class="form-control" autocomplete="off" value="<?php echo $celador -> getFechaNacimiento() ?>" require>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-white d-block">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Tipo de identificación</label>
                                <select  class="form-control" name="idTipoIdentificacion">
                                    <?php
                                            $identificacion = new Identificacion();
                                            $identificaciones = $identificacion -> consultarTodos();
                                            foreach ($identificaciones as $i) {
                                                echo "<option value='" . $i->getId() . "'". ($i -> getId() == $celador -> getIdTipoIdentificacion()?'selected':'') ." >" . $i->getNombreTipo() . "</option>";
                                            }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6 sectionActualizarUserAdmin-arriba-actualizar-sectionForm-id">
                                <label for="exampleFormControlInput1">Número de identificación</label>
                                <input type="number" name="numeroID" class="form-control" id="exampleFormControlInput1" 
                                        placeholder="Escribe tu número de identificación" onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;" min="10000000" max="9999999999" value="<?php echo $celador->getNumeroID(); ?>" required>
                            </div>
                        </div>
                    </section>
                    <section class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert">
                        <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear">
                            <section
                                class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">
                                    <?php if (isset($_POST["actualizar"])) { ?>
                                        <div class="alert alert-<?php echo ($error==1) ? "success" : "danger" ?> alert-dismissible fade show sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta" 
                                            role="alert">
                                            <?php echo ($error==1) ? "Celador actualizado exitosamente." : "El número de identificación " . $_POST['numeroID'] . " ya existe"; ?>
                                        </div>
                                    <?php  } ?>
                                </div>
                            </section>
                            <section
                                class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button class="sectionActualizarUserAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario" name="actualizar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </div>
        </section>
        <section class="sectionActualizarUserAdmin-abajoo">
            <div class="sectionActualizarUserAdmin-abajoo-cardInfo">
                <div class="sectionActualizarUserAdmin-abajoo-cardInfo-cajitaInfo">
                    <div class="sectionActualizarUserAdmin-abajoo-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionActualizarUserAdmin-abajoo-cardInfo-cajitaInfo-botones-linkCard"
                            href="<?php echo  "index.php?pid=".base64_encode("presentacion/celador/consultarCeladores.php") ?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
            var today = new Date();
            $("#datepicker").datepicker({ 
                format: 'yyyy-mm-dd',
                startDate: new Date(1940,12,31),
                endDate: new Date(today.getFullYear()-10, today.getMonth(), today.getDate())
            });
    </script>   

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>   

</body>

</html>
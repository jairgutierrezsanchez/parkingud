<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
$error = -1;
$email = "";

if(isset($_POST["recuperarClave"])){
    $email = $_POST["email"];
    $usuario = new Usuario("", "", "","", $email);
    if($usuario -> existeCorreo() ){
      if(preg_match("/^\w+@(correo.)?(udistrital.edu.co)$/",$email)){
        $key = "";
        $pattern = "1234567890abcdefghijklmñnopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
        for($i = 0; $i < 16; $i++){
            $key .= substr($pattern, mt_rand(0,16), 1);
        }
        $mail = new PHPMailer(true);
        $usuario = $usuario -> buscarPorCorreo($email);
        $u = new Usuario($usuario,"","","","",$key);
        $u -> consultar();
          try {
              //Server settings
              $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
              $mail->isSMTP();                                            //Send using SMTP
              $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
              $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
              $mail->Username   = 'parqueadero.udistrital@gmail.com';                     //SMTP username
              $mail->Password   = 'gcvtsckqrsvddlsr';                               //SMTP password
              $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
              $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
              //Recipients
              $mail->setFrom('parqueadero.udistrital@gmail.com', 'Codigo Parqueadero Universidad Distrital Francisco José de Caldas');
              $mail->addAddress($email, $u->getNombre(). " " .$u-> getApellido());     //Add a recipient            //Name is optiona
              //Attachment
              //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
              //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

              //Content
              
              $mail->isHTML(true);                                  //Set email format to HTML
              $mail->Subject = 'Administracion Parqueadero UDistrital';
              $mail->CharSet= 'UTF-8';
              $mail->Body    = '<h1>Parqueadero UDistrital</h1><br><p>A continuacion encontrará la contraseña provisional que se le ha dado, al momento de usarla es recomendable cambiarla </p><br><b>'.$key.'</b>';
              $mail->AltBody = 'Codigo Verificacion UDistrital';
              $mail->send();
          } catch (Exception $e) {
          }
          $u -> actualizarclave();
          $u -> cambiarEstado(3);
        $error = 0;
      }else{
        $error = 2;
      }
       
    }else{
        $error = 1;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssRegistro/styleRegistro.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
  <div class="fondoRegistro">  
    <div class="cardFormulario">
                <div class="card">
                  <div class="card-title">
                    <h4>Recuperar <br> Contraseña</h4>
                  </div>
                    <div class="card-body">
                    <?php 
                      if($error == 0){
                      ?>
                      <div class="alert alert-success" role="alert">
                         Contraseña provisional enviada.
                      </div>
                      <?php } else if($error == 1) { ?>
                      <div class="alert alert-danger" role="alert">
                        El correo<br> <?php echo $email; ?><br> no existe
                      </div>
                      <?php } else if($error == 2) { ?>
                      <div class="alert alert-danger" role="alert">
                        El correo<br> <?php echo $email; ?><br> no es valido
                      </div>
                    <?php } ?>
                        <form class="formlario" action=<?php echo "index.php?pid=" .base64_encode("presentacion/recuperarPassword.php")."&nos=true" ?> method="post">
                            <div class="mb-3">
                              <p> Ingrese su correo insititucional </p>
                              <input type="email" name="email"  class="form-control" id="email"  placeholder="correo institucional"  ?>                            </div>
                            <button type="submit" name="recuperarClave" class="boton mb-2">Enviar Código</button>
                            <div class="hipervinculo">
                              <a href="index.php">Volver al inicio.</a>	
                            </div>
                          </form>
                    </div>
                  </div>
            </div>
                
            
</div>

    
</body>
</html>
<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Reportes</title>
</head>
<body>
    <h1 class="titulosAdmin">Reporte Marcas</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionReportesAdmin">
        <section class="sectionReportesAdmin-arriba">
            <div class="sectionReportesAdmin-arriba-cardArriba">
                <section class="sectionReportesAdmin-arriba-cardArriba-select">
                    <label class="sectionReportesAdmin-arriba-cardArriba-select-label" for="inputState">Seleccione Facultad:</label>
                    <select id="idTipo" class="sectionReportesAdmin-arriba-cardArriba-select-selectw" name="idTipo">
                        <?php
                            $tipo = new Tipo();
                            $tipos = $tipo -> consultarTodos();
                            foreach ($tipos as $t) {
                                echo "<option value='" . $t->getId() . "'>". $t->getTipo() . "</option>";
                            }
                        ?>
                    </select>
                </section>
                <section class="sectionReportesAdmin-arriba-cardArriba-button">
                    <div class="sectionReportesAdmin-arriba-cardArriba-button-botones">
                        <a id="generarTabla" class="sectionReportesAdmin-arriba-cardArriba-button-botones-linkCard" >Consultar</a>
                    </div>
                </section>
            </div>
        </section>
        <section class="sectionReportesAdmin-abajo">
            <div class="sectionReportesAdmin-abajo-cardAbajo" id="reporte">
            </div>
        </section>
        <div class="sectionReportesAdmin-cardVolver mt-1">
			<div class="sectionReportesAdmin-cardVolver-cardInfo">
				<div class="sectionReportesAdmin-cardVolver-cardInfo-cajitaInfo-botones mt-3">
					<a class="sectionReportesAdmin-cardVolver-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Reportes/elegirOpcionReportes.php")?>">Volver</a>
				</div>
			</div>
        </div>
    </div>
    <script type="text/javascript">
        $("#generarTabla").click(function() {
            var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/Reportes/marcas/reporteMarcasAjax.php"); ?>&tipo="+$("#idTipo").val();
                $("#reporte").load(ruta);	
        });
    </script>

</body>

</html>
<?php
/* consulta en transportes */
$transporte = new Transporte("","","","","","","","",$_GET["tipo"]);
$marcasTransporte = $transporte -> consultarTransportesPorMarca();
?>

<div class="sectionReportesAdmin-abajo-cardAbajo-reporte">
    <div class="sectionReportesAdmin-abajo-cardAbajo-reporte-sectionEstadisticas">
        <div id="marcasTransporte"></div>
    </div>
</div>

 <!--SCRIPTS ADICIONALES-->
 <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
        


    var dataMarca = google.visualization.arrayToDataTable([
                ["Marca ", "Cantidad "],
                <?php 
                foreach ($marcasTransporte as $u){
                    echo "['" . $u[0] . "', " . $u[1] . "],\n";        
                }        
                ?>
            ]);
            
            var viewMarca = new google.visualization.DataView(dataMarca);
            
            var optionsMarca = {
                title: "Transportes por marca",
                width: 1000,
                height: 300,
                bar: {groupWidth: "50%"},
                legend: { position: "none" },
            };
            var chartMarca = new google.visualization.ColumnChart(document.getElementById("marcasTransporte"));
            chartMarca.draw(viewMarca, optionsMarca);

}
</script>

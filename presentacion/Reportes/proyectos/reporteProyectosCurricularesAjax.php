<?php
/* consulta en transportes */
$proyecto = new Proyecto("","",$_GET["facultad"]);
$usuariosPorProyecto = $proyecto -> consultarUsuariosPorProyecto();
?>

<div class="sectionReportesAdmin-abajo-cardAbajo-reporte">
    <div class="sectionReportesAdmin-abajo-cardAbajo-reporte-sectionEstadisticas">
        <div id="usuariosPorProyecto"></div>
    </div>
</div>

 <!--SCRIPTS ADICIONALES-->
 <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
        


    var dataMarca = google.visualization.arrayToDataTable([
                ["Marca", "Cantidad "],
                <?php 
                foreach ($usuariosPorProyecto as $u){
                    echo "['" . $u[0] . "', " . $u[1] . "],\n";        
                }        
                ?>
            ]);
            
            var viewMarca = new google.visualization.DataView(dataMarca);
            
            var optionsMarca = {
                title: "Usuarios por Proyecto",
                width: 1000,
                height: 300,
                bar: {groupWidth: "50%"},
                legend: { position: "none" },
            };
            var chartMarca = new google.visualization.ColumnChart(document.getElementById("usuariosPorProyecto"));
            chartMarca.draw(viewMarca, optionsMarca);

}
</script>

<?php
if ($_SESSION["rol"] == "celador") {
    $celador = new Celador($_SESSION["id"]);
    $celador->consultar();
    include "presentacion/celador/menuCelador.php";
    include 'presentacion/footer.php';
} else if ($_SESSION["rol"] == "administrador") {
    $administrador = new Administrador($_SESSION["id"]);
    $administrador->consultar();
    include "presentacion/menuAdministrador.php";
    include 'presentacion/footer.php';
}

/* consulta en transportes */
$usuario = new Usuario();
$totalUsuariosActivos = $usuario -> consultarTotalUsuariosActivos();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">
    <title>Reportes</title>
</head>
<body>
    <h1 class="titulosAdmin">Reporte Usuarios Activados y Usuarios Pendientes</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionReportesAdmin">
        <div class="sectionReportesAdmin-cardReportes">
            <div class="sectionReportesAdmin-cardReportes-sectionEstadisticas">
	            <div id="totalUsuariosActivos" style="width: 1000px; height: 300px;"></div>
            </div>
        </div>
        <div class="sectionReportesAdmin-cardVolver mt-1">
			<div class="sectionReportesAdmin-cardVolver-cardInfo">
				<div class="sectionReportesAdmin-cardVolver-cardInfo-cajitaInfo-botones mt-3">
					<a class="sectionReportesAdmin-cardVolver-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Reportes/elegirOpcionReportes.php")?>">Volver</a>
				</div>
			</div>
        </div>
    </div>


 <!--SCRIPTS ADICIONALES-->
 <script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
        


    var dataMarca = google.visualization.arrayToDataTable([
                ["Marca", "Cantidad "],
                <?php 
                foreach ($totalUsuariosActivos as $u){
                    echo "['" . $u[0] . "', " . $u[1] . "],\n";        
                }        
                ?>
            ]);
            
            var viewMarca = new google.visualization.DataView(dataMarca);
            
            var optionsMarca = {
                title: "Cantidad Usuarios Activos",
                bar: {groupWidth: "95%"},
                legend: { position: "none" },
            };
            var chartMarca = new google.visualization.ColumnChart(document.getElementById("totalUsuariosActivos"));
            chartMarca.draw(viewMarca, optionsMarca);

}
</script>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="cssLogin/styles.css">
    <!-- Boxiocns CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--FUENTES-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

    <title>Document</title>
</head>

<body>
    <div class="sectionLogin">
        <section class="sectionLogin-izqq">
            <img class="sectionLogin-izqq-imagenFondo" src="./img/fondoLogin.jpg" alt="">
        </section>
        <section class="sectionLogin-derr">
            <div class="sectionLogin-derr-login">
                <div class="sectionLogin-derr-login-clase">
                    <section class="sectionLogin-derr-login-clase-arriba">
                        <div class="sectionLogin-derr-login-clase-arriba-titulo">
                            <h4>Parqueadero Universidad Distrital <br> Francisco José de Caldas</h4>
                        </div>
                        <div class="sectionLogin-derr-login-clase-arriba-imagen">
                            <img class="sectionLogin-derr-login-clase-arriba-imagen-imagenLogo" src="./img/logoUniversidad.png" alt="Fondo de bicicletas rodando">
                        </div>
                    </section>
                    <section class="sectionLogin-derr-login-clase-mitad">
                        <div class="sectionLogin-derr-login-clase-mitad-cardFormulario">
                            <div class="card mt-10">
                                <div class="card-body">
                                    <?php
                                    if(isset($_GET["error"])){
                                        if($_GET["error"] == 1){
                                            echo "<div class='alert alert-warning' role='alert'>";
                                            echo "Credenciales invalidas";
                                            echo "</div>";
                                        }else if($_GET["error"] == 2){
                                            echo "<div class='alert alert-danger' role='alert'>";
                                            echo "Perfil Inhabilitado";
                                            echo "</div>";
                                        }
                                        else if($_GET["error"] == 3){
                                            echo "<div class='alert alert-danger' role='alert'>";
                                            echo "Perfil sin validar";
                                            echo "</div>";
                                        }
                                        else if($_GET["error"] == 5){
                                            echo "<div class='alert alert-success' role='alert'>";
                                            echo "Validacion de codigo Exitosa";
                                            echo "</div>";
                                        }
                                    }
                                    ?>
                                    <form class="sectionLogin-derr-login-clase-mitad-cardFormulario-formlario"
                                        action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php")?>&nos=true"
                                        method="post">
                                        <div class="mb-3">
                                            <input name="correo" type="email" class="form-control" id="Email"
                                                aria-describedby="emailHelp" placeholder="correo institucional">
                                        </div>
                                        <div class="mb-3">
                                            <div class="input-group"> 
                                                <input name="password" type="password" class="form-control" id="Password" placeholder="contraseña">
                                                <button id="show_password" class="btn btn-dark" type="button" onclick="mostrarPassword()"> 
                                                    <span class="fa fa-eye-slash icon"></span> 
                                                </button>
                                            </div>
                                        </div>
                                        <button type="submit" name="autenticar" class="sectionLogin-derr-login-clase-mitad-cardFormulario-formlario-boton mb-2">Ingresar</button>
                                        <div class="sectionLogin-derr-login-clase-mitad-cardFormulario-formlario-recordarPassword">
                                            <a id="noStyle" href=<?php echo "index.php?pid=" . base64_encode("presentacion/recuperarPassword.php") . "&nos=true" ?>>olvidó su contraseña?</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="sectionLogin-derr-login-clase-abajo">
                        <div class="sectionLogin-derr-login-clase-abajo-registrar">
                            <div class="sectionLogin-derr-login-clase-abajo-registrar-letra">
                                <h6>si aún no está registrado</h6>
                            </div>
                            <div class="sectionLogin-derr-login-clase-abajo-registrar-hipervinculo">
                                <a id="noStyleRegistrer" href=<?php echo "index.php?pid=" . base64_encode("presentacion/Registro.php") . "&nos=true" ?>>Regístrese Acá.</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("Password");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
});
</script>
</body>

</html>
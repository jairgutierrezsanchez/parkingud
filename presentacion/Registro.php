<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$error = -1;
if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $password = $_POST["password"];
    $codigoEstudiantil = $_POST["codigoEstudiantil"];
    $confirmarCodigo  = $_POST["confirmarCodigo"];
    $confirmPassword = $_POST["comfirmpassword"];
    $usuario = new Usuario("", $nombre, $apellido,"", $correo, $password, $codigoEstudiantil, "","", "",3,"",1,1,1,1);

    if($usuario -> existeCorreo() &&  $usuario -> existeCodigo()){
        $usuarioRegistrado = new Usuario($usuario -> buscarPorCorreo($correo));
        $usuarioRegistrado-> consultar();

        if($usuarioRegistrado -> getEstado()==3){
          $usuarioRegistrado -> eliminar();
        }
    }
            if($codigoEstudiantil==$confirmarCodigo){
                if(!$usuario -> existeCodigo() ){
                    if(!$usuario -> existeCorreo()){    
                        if(preg_match("/^\w+@(correo.)?(udistrital.edu.co)$/",$correo)){
                            $mayusculas  =  preg_match ( '/[A-Z]+/' ,  $password ); 
                            $minusculas  =  preg_match ( '/[a-z]+/' ,  $password ); 
                            $number     =  preg_match ( '/[0-9]+/' ,  $password ); 
                            $specialChars  =  preg_match ( "/[\p{P}\p{S}]+/" ,  $password );  
                            if ( $mayusculas  &&  $minusculas  && $number  &&  $specialChars  && strlen ( $password ) >  8 ) { 
                                if($password == $confirmPassword){
                                    $mail = new PHPMailer(true);
                                    $codigo = rand(100000,999999);
                                    try {
                                        //Server settings
                                        $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
                                        $mail->isSMTP();                                            //Send using SMTP
                                        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                                        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                                        $mail->Username   = 'parqueadero.udistrital@gmail.com';                     //SMTP username
                                        $mail->Password   = 'gcvtsckqrsvddlsr';                               //SMTP password
                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                                        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                                        //Recipients
                                        $mail->setFrom('parqueadero.udistrital@gmail.com', 'Codigo Parqueadero Universidad Distrital Francisco José de Caldas');
                                        $mail->addAddress($correo, $nombre. " " .$apellido);     //Add a recipient            //Name is optiona
                                        //Attachments
                                        //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
                                        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

                                        //Content
                                        
                                        $mail->isHTML(true);                                  //Set email format to HTML
                                        $mail->Subject = 'Administracion Parqueadero UDistrital';
                                        $mail->CharSet= 'UTF-8';
                                        $mail->Body    = '<h1>Parqueadero UDistrital</h1><br><p>A continuacion encontrara el codigo que tiene que ingresar en la aplicación del parqueadero: </p><b>'.$codigo.'</b>';
                                        $mail->AltBody = 'Codigo Verificacion UDistrital';
                                        $mail->send();
                                    } catch (Exception $e) {
                                    }
                                    $usuario -> registrar($codigo);
                                    header("Location: index.php?pid=" . base64_encode("presentacion/validarCodigo.php"). "&nos=true&id=".$usuario ->buscarPorCorreo($correo));
                                }else{
                                    $error = 3;
                                }
                            }else{
                                $error = 6;
                                }
                        }else{
                        $error = 2;
                        }
                    }else{
                        $error = 4;
                    }
                }else{
                    $error =1;
                }
            }else{
                $error =5;
            }
        
            
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssRegistro/styleRegistro.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="registro">
        <div class="registro-cardRegistro">
            <h4>Registro Usuarios Parqueadero Universidad Distrital Francisco José de Caldas</h4>
            <section class="registro-cardRegistro-sectionAR mt-3">
                <div class="registro-cardRegistro-sectionAR-infoRegistro">
                    <form class="formlario"
                        action=<?php echo "index.php?pid=" .base64_encode("presentacion/Registro.php")."&nos=true" ?>
                        method="post">
                        <section class="registro-cardRegistro-sectionAR-infoRegistro-formRegistro">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="inputState">Nombre</label>
                                    <input type="text" name="nombre" class="form-control inputsCrearUsuario"
                                        id="exampleFormControlInput1" placeholder="Escriba el nombre" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="inputState">Apellido</label>
                                    <input type="text" name="apellido" class="form-control inputsCrearUsuario"
                                        id="exampleFormControlInput1" placeholder="Escriba el apellido"required>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="exampleFormControlInput1">Código
                                        estudiantil</label>
                                    <input type="number" name="codigoEstudiantil" class="form-control"
                                        id="exampleFormControlInput1" placeholder="Escribe tu Código estudiantil"
                                        min="10000000000" max="99999999999"
                                        onKeyDown="if(this.value.length==11 && event.keyCode!=8) return false;" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="exampleFormControlInput1">Confirmar codigo
                                        estudiantil</label>
                                    <input type="number" name="confirmarCodigo" class="form-control"
                                        id="exampleFormControlInput1" placeholder="Confirma tu Código estudiantil"
                                        onKeyDown="if(this.value.length==11 && event.keyCode!=8) return false;"
                                        min="10000000000" max="99999999999" required>
                                </div>

                            </div>

                            <div class="row mt-3">

                                <div class="form-group col-md-12">
                                    <label id="crearUsuariolabel" for="inputState">Correo</label>
                                    <input type="email" name="correo" class="form-control inputsCrearUsuario"
                                        id="exampleFormControlInput1" placeholder="Escriba el correo" required>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="inputState">Contraseña</label>
                                    <div class="input-group">
                                        <input type="password" id="txtPassword" name="password" class="form-control" id="exampleInputPassword1" placeholder="Escriba una contraseña" required>
                                        <button id="show_password" class="btn btn-dark" type="button" onclick="mostrarPassword()"> 
                                            <span id="mostrarPassword1" class="fa fa-eye-slash icon"></span> 
                                        </button>
                                </div> 
                                </div>
                                <div class="form-group col-md-6">
                                    <label id="crearUsuariolabel" for="inputState">Confirmar Contraseña</label>
                                    <div class="input-group">    
                                        <input type="password" id="txtPasswordd" name="comfirmpassword" class="form-control" id="exampleInputPassword1" placeholder="Vuelva a escribir su contraseña" required>
                                        <button id="show_passwords" class="btn btn-dark" type="button" onclick="mostrarPasswordd()"> 
                                            <span id="mostrarPassword2" class="fa fa-eye-slash icon"></span> 
                                        </button> 
                                </div>
                            </div>
                        </section>
                        <section class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro">
                            <div
                                class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear mt-2">
                                <section
                                    class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionAlerta">
                                    <div
                                        class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionAlerta-alert">
                                        <?php if (isset($_POST['registrar'])) { ?>
                                        <div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionAlerta-alert-alerta"
                                            role="alert">

                                            <?php echo ($error==4) ?  $_POST['correo'] . " ya existe":"" ?>
                                            <?php echo ($error==2) ? "El correo debe ser del dominio de la universidad":""; ?>
                                            <?php echo ($error==3) ? "La contraseña no es la misma":""; ?>
                                            <?php echo ($error==1) ? $_POST['codigoEstudiantil'] . " ya existe":""; ?>
                                            <?php echo ($error==5) ? "Los codigos deben ser iguales":""; ?>
                                            <?php echo ($error==6) ? "La contraseña debe tener minimo 8 caracteres, una mayuscula y minuscula,\n
                                                                      un caracter especial y un número":""; ?>

                                            </button>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </section>
                                <section
                                    class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionBoton">
                                    <div
                                        class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionBoton-botonsito">
                                        <button
                                            class="registro-cardRegistro-sectionAR-infoRegistro-alertRegistro-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario"
                                            name="registrar">Registrar</button>
                                    </div>
                                </section>
                            </div>
                        </section>
                    </form>
                </div>
            </section>
            <section class="registro-cardRegistro-sectionABR">
                <div class="registro-cardRegistro-sectionABR-cardInfo">
                    <div class="registro-cardRegistro-sectionABR-cardInfo-cajitaInfo-botones mt-3">
                        <a class="registro-cardRegistro-sectionABR-cardInfo-cajitaInfo-botones-linkCard"
                            href="index.php">Volver al Inicio</a>
                    </div>
                </div>
            </section>
        </div>
    </div>


</body>
<script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("txtPassword");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#mostrarPassword1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#mostrarPassword1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	function mostrarPasswordd(){
		var cambio = document.getElementById("txtPasswordd");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#mostrarPassword2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#mostrarPassword2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
</script>

</html>
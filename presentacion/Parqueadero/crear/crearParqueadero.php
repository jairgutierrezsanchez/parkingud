<?php
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}

$error = -1;
$numero = "";
$puestosMaximos = "";
	

if(isset($_POST["crear"])){
    $numero = $_POST["numero"];
    $puestosMaximos = $_POST["puestosMaximos"];
    $parqueadero = new Parqueadero("",	$numero, 1, "", $puestosMaximos, $_GET["idTipo"]);
    if(!$parqueadero -> existeNombre()){
        if($parqueadero -> contarParqueaderos($_GET["idTipo"])<8){
            $parqueadero -> registrar();
            $error = 0;
        }else{
            $error= 2;
        }
    }else{
        $error = 1;
    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
 
<body>
    <h1 class="titulosAdmin">Crear Parqueadero</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionDobleCC">
		<section class="sectionDobleCC-arriba">
			<div class="sectionDobleCC-arriba-crearParking">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Parqueadero/crear/crearParqueadero.php")."&idTipo=".$_GET["idTipo"]?> method="post">
                    <section class="sectionDobleCC-arriba-crearParking-sectionFormulario mt-4">
                        <div class="row">
                            <div class="form-group col-md-6 sectionDobleCC-arriba-crearParking-sectionFormulario-label">
                                <label for="inputState">Nombre del Parqueadero</label>
                                <input type="text" name="numero" class="form-control" id="exampleFormControlInput1" placeholder="Escriba el nombre del Parqueadero" required>
                            </div>
                            <div class="form-group col-md-6 sectionDobleCC-arriba-crearParking-sectionFormulario-label">
                                <label for="inputState">Capacidad del Parqueadero</label>
                                <input type="number" name="puestosMaximos" class="form-control" id="exampleFormControlInput1" placeholder="Escriba la capacidad del parqueadero" min="1" required>
                            </div> 
                        </div>
                    </section>
                    <section class="sectionDobleCC-arriba-crearParking-sectionButtonAlert">
                        <div class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear mt-2">
                            <section class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">
                                    <?php if (isset($_POST['crear'])) { ?>
                                        <div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show sectionDobleCU-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta"
                                            role="alert">
                                            <?php echo ($error==0) ? "Registro exitoso" : ($error==1?"El parqueadero: ".$_POST["numero"] . " ya existe":"Alcanzo el límite, no se pueden crear más parqueaderos"); ?>
                                            </button>
                                        </div>
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button class="sectionDobleCC-arriba-crearParking-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario" name="crear">Crear</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
		    </div>
		</section>
		<section class="sectionDobleCC-abajo mt-1">
			<div class="sectionDobleCC-abajo-cardInfo">
				<div class="sectionDobleCC-abajo-cardInfo-cajitaInfo-botones mt-3">
					<a class="sectionDobleCC-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php"):base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php"))."&idParqueadero=".$parqueadero -> getIdparqueadero()."&idTipo=" . $_GET["idTipo"]?>">Volver</a>
				</div>
			</div>  
		</section>
    </div> 

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
</body>
</html>
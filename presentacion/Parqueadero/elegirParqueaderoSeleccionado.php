
<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}

$parqueadero = new Parqueadero();
$parqueaderos = $parqueadero -> consultarTodosTipo($_GET["idTipo"]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Elija al parqueadero que desea acceder (<?php echo ($_GET["idTipo"]==2?"Bicicletas":"Motocicletas") ?>)</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="parkingCardMotoBiciAdmin">
    <div class="parkingCardMotoBiciAdmin-EPS">
        <section class="parkingCardMotoBiciAdmin-EPS-arribaSectionEPS">
            <div class="parkingCardMotoBiciAdmin-EPS-arribaSectionEPS-crearParqueadero">
                <div class="parkingCardMotoBiciAdmin-EPS-arribaSectionEPS-crearParqueadero-botonCrear">
                    <a id="noSub" class="parkingCardMotoBiciAdmin-EPS-arribaSectionEPS-crearParqueadero-botonCrear-linkCrear"
                        href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/crear/crearParqueadero.php") . "&idTipo=" . $_GET["idTipo"]?>">
                        Crear Parqueadero Nuevo
                    </a>
                </div>
            </div>
        </section>
        <section class="parkingCardMotoBiciAdmin-EPS-abajoSectionEPS">
            <div class="parkingCardMotoBiciAdmin-EPS-abajoSectionEPS-infoCMB">
                <?php foreach($parqueaderos as $p){ ?>
                    <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/agregarUsuarioEnParqueadero.php")."&idParqueadero=".$p -> getIdparqueadero()."&idTipo=" . $_GET["idTipo"]?>">
                        <div class="card parkingCardMotoBiciAdmin-EPS-abajoSectionEPS-infoCMB-elegir mr-5">
                            <section class="parkingCardMotoBiciAdmin-EPS-abajoSectionEPS-infoCMB-elegir-arribaEPS">
                                <?php echo $p ->getNumero() ?> <br>
                            </section>
                            <section class="parkingCardMotoBiciAdmin-EPS-abajoSectionEPS-infoCMB-elegir-abajoEPS">
                                <div class="parkingCardMotoBiciAdmin-EPS-abajoSectionEPS-infoCMB-elegir-abajoEPS-puestosEPS">
                                    <?php echo "Capacidad: " . $p ->getPuestosMaximos() ?> <br>
                                    <?php echo "Puestos Ocupados: " . $p ->getPuestosOcupados() ?>
                                </div>
                            </section>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </section>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php
    $usuario = new Usuario();

    $filtro =$_GET["filtro"];
    // buscar usuario en el parqueadero si esta su vehiculo en el parqueadero
    $usuarios = $usuario -> buscarUsuarioUsandoParqueadero($filtro,$_GET["idTipo"]);
    // si el usuario no esta en el parqueadero que lo anuncie en un alert
    if(count($usuarios)>0){
        $transporte = new Transporte();
        $transporte -> consultarTransportesPorId($usuarios[0] -> getId(),$_GET["idTipo"]);
        $tep = new TransporteEnParqueadero("",$transporte -> getIdtransporte());
        $tep -> consultar();
        $historialParqueadero = new HistorialParqueadero("","","","","",$transporte -> getIdtransporte(),$tep -> getIdParqueadero());
        $historialParqueadero -> buscarPorId();
        
    }
    $idUsuario="";
    if(count($usuarios)>0 && $transporte!=null && $transporte -> getEstado()==1) {
?>
                <table class="table parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax mt-2">
                    <thead class="table-dark"> 
                        <tr>
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoA">Foto</th>
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombreA">Nombre</th>			
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudente">Código Estudiantil</th>
                            <!--<th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-proyecto">Proyecto Curricular</th>-->
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-hora">Fecha Ingreso</th>
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-hora">Hora Ingreso</th>
                            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-servicios">Detalles</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $p = new Proyecto();
                        $t = new Transporte();
                            foreach ($usuarios as $u) {
                                $idUsuario = $u -> getId();
                                echo "<tr>";
                                echo "<td>" . ( ($u -> getFoto()=="")?"<img class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax'>") . "</td>";
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombreTsSD'>" . $u->getNombre() . '<br>' . $u->getApellido() . "</td>";
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudenteA'>" . $u->getCodigoEstudiantil() . "</td>";
                                /*$p -> consultar($u -> getIdProyecto());
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-proyectoA'>" . $p -> getNombre() . "</td>"; */
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-horaA'>" . $historialParqueadero -> getFechaIngreso() . "</td>";
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-horaA'>" . $historialParqueadero -> getHoraIngreso() . "</td>";
                                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-serviciosA'>" . "<a href='modalConsultarTransporte.php?idtransporte=" . $transporte->getIdtransporte() . "' data-bs-toggle='modal' data-bs-target='#modalConsultarTransporte' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>".
                                          
                                    "</td>"; 
                                echo "</tr>";
                            }
                        echo "<caption>" . count($usuarios) . " registros encontrados</caption>"?>
                    </tbody>
                    
                </table>
                <form method='post' action='index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/expulsar/expulsarUsuarioEnParqueadero.php") . "&idUsuario=" . $idUsuario ."&idTipo=" . $_GET["idTipo"]."&idParqueadero=".$tep -> getIdParqueadero()?>'>
                    <button type="submit" class="parkingBuscarParqueadero-cuerpo-cardInfo-cardBotoon-botoon" name="guardarLlegada">Expulsar Transporte</button>
                </form> 

                <?php } else { ?>
        <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert"  >
            <p text-align: center;>No se encontraron resultados</p>
            
        </div>
    <?php } ?>
    

    

<!--SCRIPTS ADICIONALES-->

</script>
<div class="modal fade" id="modalConsultarTransporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>


<script type="text/javascript">
	<?php 
		foreach ($usuarios as $u) {
			if($u -> getEstado() !=2){
				echo "$('#cambiarEstado" . $u -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/usuario/cambiarEstadoUsuarioAjax.php") . "&id=" . $u -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $u -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $u -> getId() . "').attr('class') == 'fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $u -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t}\n";
				echo "});\n";

				
				        
			}
		}
	?>
</script>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

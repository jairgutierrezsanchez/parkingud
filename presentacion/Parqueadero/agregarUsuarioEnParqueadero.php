<?php 
ob_start();
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
    include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
    include 'presentacion/footer.php';
}

$parqueadero = new Parqueadero($_GET["idParqueadero"]);
$parqueadero -> consultar();
$puestosOcupados =  $parqueadero -> getPuestosOcupados();
//ACTUALIZAR

if (isset($_POST["actualizar"]) && $_POST["puestosMaximos"] != "") {
    $puestosMaximos = $_POST["puestosMaximos"];
    $parqueadero = new Parqueadero($_GET["idParqueadero"], "", "", "", $puestosMaximos, "");
    $parqueadero->actualizar();
}

//ACTUALZIAR
$puestosDisponibles = intval ($parqueadero -> getPuestosMaximos()) - intval ($puestosOcupados) ;


$atributo = "";
$direccion = "";
$filas = 5;
$pag = 1;
if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}

$usuario = new Usuario();
$usuarios = $usuario->consultarTodos($filas, $pag);
$totalFilas = $usuario->consultarTotalFilas();
if(isset($_POST["guardarLlegada"])){
    $transporte = new Transporte();
    $transporte -> consultarTransportesPorId($_GET["idUsuario"],$_GET["idTipo"]);
    $transporte -> actualizarEstado(1,$transporte -> getIdtransporte(),$_GET["idParqueadero"]);
    $parqueadero = new Parqueadero($_GET["idParqueadero"]);
    $parqueadero -> consultar();
    $parqueadero -> actualizarPuestosOcupados();
    $historialParqueadero = new HistorialParqueadero("","","","","",$transporte -> getIdtransporte(),$_GET["idParqueadero"]);
    $historialParqueadero -> registrarLlegada();
    $historialParqueadero -> buscarPorId();
    $tep = new TransporteEnParqueadero();
    $tep -> registrar($transporte -> getIdtransporte(),$_GET["idParqueadero"],$historialParqueadero-> getIdHistorialParqueadero());  
    header("location: index.php?pid=" . base64_encode("presentacion/Parqueadero/agregarUsuarioEnParqueadero.php")."&idParqueadero=".$_GET["idParqueadero"]."&idTipo=" . $_GET["idTipo"]);
    return;
} ?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Agregar usuario a Parqueadero de <?php echo ($_GET["idTipo"]==2?"Bicicletas":"Motocicletas") ?></h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="parkingBuscarParqueadero">
        <div class="parkingBuscarParqueadero-cuerpo">
            <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero">
                <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar">
                    <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar"> 
                        <div class="form-group parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras">
                            <label class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-labelBuscar-controlOpciones">Buscar usuario:</label> 
                            <input name="filtro" id="filtro" type="number" class="form-control parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-inputt"  onKeyDown="if(this.value.length==11 && event.keyCode!=8) return false;" placeholder="Ingrese código estudiantil">
                            <input type="number" class="form-control form-control parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-controlOpcioness" placeholder=" <?php echo "Ocupados: ". $puestosOcupados ?>" disabled>
                            <input type="number" class="form-control form-control parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-controlOpcioness" placeholder=" <?php echo "Disponibles: ". $puestosDisponibles?>" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="parkingBuscarParqueadero-cuerpo-cardInfo mt-3">
               <div id="resultados"></div>
            </div>
            <div class="parkingBuscarParqueadero-cuerpo-cardOpciones">
                <div class="parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr">
                    <div class="parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar">
                        <div class="form-group parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm">
                            <form class="formlario" action=<?php echo "index.php?pid=" . base64_encode("presentacion/Parqueadero/agregarUsuarioEnParqueadero.php")."&idParqueadero=".$_GET["idParqueadero"] . "&idTipo=" . $_GET["idTipo"]?> method="post">
                                <div class="mb-3 parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm-controlFormulario">
                                    <label class="parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm-controlFormulario-controlOpciones">Capacidad:</label> 
                                    <input type="number" name="puestosMaximos" class="form-control parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm-controlFormulario-controlOpcionesLabel" min = "0" max = "999" 
                                            placeholder=" <?php echo "Puestos máximos: ". $parqueadero -> getPuestosMaximos() ?>" onKeyDown="if(this.value.length==3 && event.keyCode!=8) return false;">
                                    <button type="submit" name="actualizar" class="parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm-controlFormulario-botonActualizar">Actulizar puestos máximos</button>
                                </div>     
                            </form>
                            <?php if (isset($_POST["actualizar"]) && $_POST["puestosMaximos"] != "") { ?>
                                <div class="alert alert-success parkingBuscarParqueadero-cuerpo-cardOpciones-opcionesFormr-actualizar-opcionesForm-alertas" role="alert">Puestos actualizados.</div>						
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="parkingBuscarParqueadero-cuerpo-botonn">
				<div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo">
                    <div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness mt-1">
                        <a class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness-linkCardd" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php"):base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php"))."&idParqueadero=".$parqueadero -> getIdparqueadero()."&idTipo=" . $_GET["idTipo"]?>">Volver</a>
                    </div>
                    <div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness mt-1">
                        <a class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness-linkCardd" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Parqueadero/editarNombreParqueadero.php"):base64_encode("presentacion/Parqueadero/editarNombreParqueadero.php"))."&idParqueadero=".$parqueadero -> getIdparqueadero()."&idTipo=" . $_GET["idTipo"]?>">Editar Nombre</a>
                    </div>
                </div>
		    </div>

           
        </div>
    </div>


<!--SCRIPTS ADICIONALES-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#filtro").keyup(function(){
            var tam= $("#filtro").val();
            if(tam.length==11){
                var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/Parqueadero/agregarUsuarioEnParqueaderoAjax.php")."&idTipo=".$_GET["idTipo"]."&idParqueadero=".$_GET["idParqueadero"]; ?>&filtro="+$("#filtro").val();
                $("#resultados").load(ruta);
            }
        });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

<?php
require 'pdf/class.pdf.php';
require 'pdf/class.ezpdf.php';


$pdf = new Cezpdf('LETTER');
$pdf -> selectFont('pdf/fonts/Helvetica.afm');
$pdf -> ezSetCmMargins(2, 2, 2, 2);
$pdf -> setColor(0,0,0);
$tletra1=18;
$tletra=12;
$texto ="Historial del parqueadero del ".$_GET["fecha1"]. (isset($_GET["fecha2"])?" hasta " . $_GET["fecha2"]:""). "\n";
$pdf -> ezText($texto, $tletra1, array('justification' => 'center'));

$cols = array('num'=>'No', 'fechaIngreso'=>'Fecha Ingreso','horaIngreso'=>'Hora Ingreso', 'fechaSalida'=>'Fecha Salida',
                'horaSalida'=>'Hora Salida', 'transporte'=>'Transporte','usuario'=>'Usuario','parqueadero'=>'Parqueadero');
$fecha1= $_GET['fecha1'];
$fecha2= "";
if(isset($_GET["fecha2"])){
    $fecha2 = $_GET["fecha2"];
}
$codigo = $_GET['codigo'];
$orden = "ASC";
$historial = new HistorialParqueadero();
$historiales = $historial ->  buscarLogs($codigo,$fecha1,$fecha2,$orden);
$transporte = new Transporte();
$parqueadero = new Parqueadero();
$usuario = new Usuario();
$tipoTrasporte = new Tipo();
$data = array();
$i = 1;
foreach($historiales as $h){
    $transporte = new Transporte($h -> getTransporte());
    $transporte -> consultar();
    $usuario = new Usuario($transporte -> getIdusuario());
    $usuario -> consultar();
    $parqueadero = new Parqueadero($h -> getIdparqueadero());
    $parqueadero -> consultar();

    
    $data[$i-1] = array('num'=>$i, 'fechaIngreso'=>$h -> getFechaIngreso(), 'horaIngreso'=>$h -> getHoraIngreso(), 'fechaSalida'=>($h -> getFechaSalida()==""?"No aplica":$h -> getFechaSalida()),
    'horaSalida'=>($h -> getHoraSalida()==""?"No aplica":$h -> getHoraSalida()), 'transporte'=>$transporte -> getSerial(),
    'usuario'=>$usuario -> getNombre(). "\n".$usuario ->getApellido(),'parqueadero'=>$parqueadero -> getNumero());
    $i++;
    unset($transporte,$usuario,$parqueadero);
}

   ///y las cabeceras de la tabla las pondremos ocultas
unset ($opciones_tabla);

//// mostrar las lineas
$opciones_tabla['showlines']=1;

//// mostrar las cabeceras
$opciones_tabla['showHeadings']=1;

//// lineas sombreadas
$opciones_tabla['shaded']= 1;

//// tamaño letra del texto
$opciones_tabla['fontSize']= 10;

//// color del texto
$opciones_tabla['textCol'] = array(0,0,0);

//// tamaño de las cabeceras (texto)
$opciones_tabla['titleFontSize'] = 12;

//// margen interno de las celdas
$opciones_tabla['rowGap'] = 3;
$opciones_tabla['colGap'] = 3;
//esta es la primera tabla
   $pdf->ezTable($data, $cols, "", $opciones_tabla);
$pdf -> ezStream();

function puntos_cm ($medida, $resolucion=72)
{
   //// 2.54 cm / pulgada
   return ($medida/(2.54))*$resolucion;
}

//ob_end_flush();
?>
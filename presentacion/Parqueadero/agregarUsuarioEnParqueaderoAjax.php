<?php
    $idParqueadero = $_GET["idParqueadero"];
    $usuario = new Usuario();
    $parqueadero = new Parqueadero($_GET["idParqueadero"]);
    $parqueadero -> consultar();
    $filtro =$_GET["filtro"];
    // buscar usuario en el parqueadero si no esta su vehiculo en el parqueadero
    $usuarios = $usuario -> buscarUsuarioParqueadero($filtro,$_GET["idTipo"]);
    // si el usuario ya esta en el parqueadero que lo anuncie en un alert
    
    $transporte = new Transporte();
    if(count($usuarios)>0){
        $transporte -> consultarTransportesPorId($usuarios[0] -> getId(),$_GET["idTipo"]);
        $idUsuario="";
        if ($transporte->getEstado() != 2 )  {
            if($transporte-> getEstado() != 1){
                if ($parqueadero->getPuestosMaximos() - $parqueadero->getPuestosOcupados() > 0) {
                    if ($transporte != null && $transporte->getEstado() == 0) {
?>
    <table class="table parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax mt-3">
        <thead class="table-dark"> 
            <tr>
                <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoA">Foto</th>
                <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombreA">Nombre</th>			
                <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudente">Código Estudiantil</th>
                <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-proyecto">Proyecto Curricular</th>
                <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-servicios">Detalles</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $p = new Proyecto();
            foreach ($usuarios as $u) {
                $idUsuario = $u->getId();
                echo "<tr>";
                echo "<td>" . ( ($u -> getFoto()=="")?"<img class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax'>") . "</td>";
                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombreTsSD'>" . $u->getNombre() .  '<br>' . $u->getApellido() . "</td>";
                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudenteA'>" . $u->getCodigoEstudiantil() . "</td>";
                $p->consultar($u->getIdproyecto());
                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-proyectoA'>" . $p->getNombre() . "</td>";
                echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-serviciosA'>" . "<a href='modalConsultarTransporte.php?idtransporte=" . $transporte->getIdtransporte() . "' data-bs-toggle='modal' data-bs-target='#modalConsultarTransporte' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>" .

                    "</td>";
                echo "</tr>";
            }
            echo "<caption>" . count($usuarios) . " registros encontrados</caption>" ?>
        </tbody>
    </table>
    <form method='post' action='index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/agregarUsuarioEnParqueadero.php") . "&idUsuario=" . $idUsuario . "&idParqueadero=" . $idParqueadero . "&idTipo=" . $_GET["idTipo"] ?>'>
        <button type="submit" class="parkingBuscarParqueadero-cuerpo-cardInfo-cardBotoon-botoon" name="guardarLlegada">Guardar Llegada</button>
    </form> 
    <?php } else { ?>
                    <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert">
                                No se encontraron registros
                    </div>
            <?php }
                    } else { ?>
                    <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert">
                        Parqueadero lleno
                <?php }
                }else{ ?>
                    <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert">
                        El transporte del usuario ya esta en el parqueadero
                <?php }
                    }else{ ?>
                    <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert">
                        El transporte del usuario esta pendiente por activar
                    <?php }
                        }else{ ?>
                    <div class="alert alert-danger alert-dismissible fade show parkingBuscarParqueadero-cuerpo-cardInfo-alertaNoResultados" role="alert">
                        No se encontraron resultados
                    <?php } ?>
    

    

<!--SCRIPTS ADICIONALES-->

</script>
<div class="modal fade" id="modalConsultarTransporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>


<script type="text/javascript">
	<?php 
		foreach ($usuarios as $u) {
			if($u -> getEstado() !=2){
				echo "$('#cambiarEstado" . $u -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/usuario/cambiarEstadoUsuarioAjax.php") . "&id=" . $u -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $u -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $u -> getId() . "').attr('class') == 'fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('class', 'fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $u -> getId() . "').attr('title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $u -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $u -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t}\n";
				echo "});\n";

				
				        
			}
		}
	?>
</script>

<?php
require 'pdf/class.pdf.php';
require 'pdf/class.ezpdf.php';


$pdf = new Cezpdf('LETTER');
$pdf -> selectFont('pdf/fonts/Helvetica.afm');
$pdf -> ezSetCmMargins(1, 1, 2, 1);
$pdf -> setColor(0,0,0);
$tletra1=18;
$tletra=12;
$texto ="Historial del parqueadero del ".$_GET["fecha1"]. (isset($_GET["fecha2"])?" hasta " . $_GET["fecha2"]:""). "\n";
$pdf -> ezText($texto, $tletra1, array('justification' => 'center'));

$cols = array('num'=>'No', 'fechaIngreso'=>'Fecha Ingreso', 'horaIngreso'=>'Hora Ingreso', 'fechaSalida'=>'Fecha Salida',
                'horaSalida'=>'Hora Salida', 'transporte'=>'Transporte','usuario'=>'Usuario','parqueadero'=>'Parqueadero');
$fecha1= $_GET['fecha1'];
$fecha2;
if(isset($_GET["fecha2"])){
    $fecha2 = $_GET["fecha2"];
}else{
    $fecha2 = null;
}
$orden = "ASC";
$historial = new HistorialParqueadero();
$historiales = $historial ->  buscarLogsUsers($_GET["id"],$fecha1,$fecha2,$orden);
$transporte = new Transporte();
$parqueadero = new Parqueadero();
$usuario = new Usuario();
$tipoTrasporte = new Tipo();
$data = array();
$i = 1;
foreach($historiales as $h){
    $transporte = new Transporte($h -> getTransporte());
    $transporte -> consultar();
    $usuario = new Usuario($transporte -> getIdusuario());
    $usuario -> consultar();
    $parqueadero = new Parqueadero($h -> getIdparqueadero());
    $parqueadero -> consultar();

    
    $data[$i-1] = array('num'=>$i, 'fechaIngreso'=>$h -> getFechaIngreso(), 'horaIngreso'=>$h -> getHoraIngreso(), 'fechaSalida'=>($h -> getFechaSalida()==""?"No aplica":$h -> getFechaSalida()),
     'horaSalida'=>($h -> getHoraSalida()==""?"No aplica":$h -> getHoraSalida()), 'transporte'=>$transporte -> getSerial(),
    'usuario'=>$usuario -> getNombre(). "\n".$usuario ->getApellido(),'parqueadero'=>$parqueadero -> getNumero());
    $i++;
    unset($transporte,$usuario,$parqueadero);
}

   $colss = array('num'=>array('justification'=>'left'), 'fechaIngreso'=>array('justification'=>'left'),
   'horaIngreso'=>array('justification'=>'left'), 'fechaSalida'=>array('justification'=>'left'),
   'horaSalida'=>array('justification'=>'left'), 'transporte'=>array('justification'=>'left'),
   'usuario'=>array('justification'=>'left'),'parqueadero'=>array('justification'=>'left'));

   $conf = array('evenColumns' => 1, 'maxWidth' => 50, 'shadeHeadingCol' => array(0.6, 0.6, 0.5), 'shaded' => 1, 'shadeCol' => array(0.95, 0.95, 0.95), 'shadeCol2' => array(0.85, 0.85, 0.85), 'xPos' => 'right', 'xOrientation' => 'left', 'gridlines' => 50, 'cols' => array('num' => array('bgcolor' => array(1, 1, 0))));
//esta es la primera tabla
   $pdf->ezTable($data, $cols, "", $conf);
$pdf -> ezStream();

//ob_end_flush();
?>
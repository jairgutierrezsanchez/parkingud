<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosUsers">Elija qué sección desea Consultar</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerHistorialUser">
    <div class="containerHistorialUser-infoEP">
        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialParqueaderoUsers.php")."&idTipo=2"?>">
            <div class="card containerHistorialUser-infoEP-elegirP mr-5">
                <i class='bx bx-history'></i>  
                <div class="card-footer bg-transparent ">Ver Historial</div>
            </div>
        </a> 
        <a id="noSub" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialActualUser.php")?>">
            <div class="card containerHistorialUser-infoEP-elegirP mr-5">
                <i class='bx bxs-parking'></i>  
                <div class="card-footer bg-transparent ">Historial Actual Parqueadero</div>
            </div>
        </a>
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
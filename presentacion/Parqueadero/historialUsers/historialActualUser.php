<?php
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';

$historial = new HistorialParqueadero();
$historiales = $historial->consultarActual($_SESSION["id"]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">
    <title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosUsers">Transportes en Parqueadero - Tiempo Real</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="historialActualUser">
    <section class="historialActualUser-arribaHAU">
        <?php if($historiales ==null){
            echo "<div class='alert alert-danger' role='alert'>No tienes transportes en el parqueadero</div>";
            
        } else {
            foreach ($historiales as $h) {
                $p = new Parqueadero($h->getIdparqueadero());
                $p->consultar();
                $t = new Transporte($h->getTransporte());
                $t->consultar();
                $t->consultarFotosVehiculo();
                $m = new Marca();
                ?>
            <div class="historialActualUser-arribaHAU-cardHAU mr-5">
                <section class="historialActualUser-arribaHAU-cardHAU-arribaSection">
                    <img id="idFotoHAU" src="<?php echo $t->getFotoTransporte(); ?>" class="historialActualUser-arribaHAU-cardHAU-arribaSection-fotoHAU" alt="...">
                </section>
                <section class="historialActualUser-arribaHAU-cardHAU-abajoSection">
                    <section class="historialActualUser-arribaHAU-cardHAU-abajoSection-infoSection">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th class="thHAU" width="40%">Fecha Ingreso</th>
                                    <td><?php echo $h->getFechaIngreso() ?></td>
                                </tr>
                                <tr>
                                    <th class="thHAU" width="30%">Hora Ingreso</th>
                                    <td><?php echo $h->getHoraIngreso() ?></td>
                                </tr>
                                <tr>
                                    <th class="thHAU" width="30%">Parqueadero</th>
                                    <td><?php echo $p->getNumero() ?></td>
                                </tr>   
                            </tbody>
                        </table>
                    </section>
                    <section class="historialActualUser-arribaHAU-cardHAU-abajoSection-botonSection mb-3">
                        <div class="historialActualUser-arribaHAU-cardHAU-abajoSection-botonSection-cardInfo">
                            <div class="historialActualUser-arribaHAU-cardHAU-abajoSection-botonSection-cardInfo-cajitaInfo">
                                <div class="historialActualUser-arribaHAU-cardHAU-abajoSection-botonSection-cardInfo-cajitaInfo-botones mt-3">
                                    <a class="historialActualUser-arribaHAU-cardHAU-abajoSection-botonSection-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/consultarTransporte.php") . "&Tipo=" . ($t->getIdtipo()==2?"Bici":"Moto") ?>">Detalles Transporte</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        <?php }
        }?>
    </section>
    <section class="historialActualUser-abajoHAU">
        <div class="historialActualUser-abajoHAU-cardInfo">
            <div class="historialActualUser-abajoHAU-cardInfo-cajitaInfo">
                <div class="historialActualUser-abajoHAU-cardInfo-cajitaInfo-botones mt-3">
                    <a class="historialActualUser-abajoHAU-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/elegirOpcionUsers.php")?>">Volver</a>
                </div>
            </div>
        </div>
    </section>
</div>
    
</body>
</html>
<?php
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';

$historial = new HistorialParqueadero();
$fechaIngreso = "";
$orden = "ASC";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosOtros/cssOtros/stylesOtros.css">

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Historial Usuario en Parqueadero</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionHistorialAdmin">
        <section class="sectionHistorialAdmin-arriba">
            <section  class="sectionHistorialAdmin-arriba-sectionFechas">
                <section class="sectionHistorialAdmin-arriba-sectionFechas-izquierda">
                    <div class="sectionHistorialAdmin-arriba-sectionFechas-izquierda-elegirFecha">
                        <label class="sectionHistorialAdmin-arriba-sectionFechas-izquierda-elegirFecha-label">Elegir tipo de filtro</label>
                        <select class="sectionHistorialAdmin-arriba-sectionFechas-izquierda-elegirFecha-fecha" id="fecha" name="idTipoIdentificacion" onChange="traerFecha2()">
                            <option value="1">Rango entre fechas</option>
                            <option value="2" selected>Fecha Única</option>
                        </select>
                    </div>
                </section>

                <section class="sectionHistorialAdmin-arriba-sectionFechas-derecha">
                    <div class="sectionHistorialAdmin-arriba-sectionFechas-derecha-fila">
                        <label class="sectionHistorialAdmin-arriba-sectionFechas-derecha-fila-label" for="date">Fecha Inicial</label>
                        <div class="input-group date sectionHistorialAdmin-arriba-sectionFechas-derecha-fila-divFecha" id="datepicker">
                            <input id="fecha1" type="text" class="form-control" autocomplete="off">
                            <span class="input-group-append">
                                <span class="input-group-text bg-white d-block">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="sectionHistorialAdmin-arriba-sectionFechas-derecha-fila" id="segundaFecha">
                    
                    </div>
                </section>
            </section>
            <section class="sectionHistorialAdmin-arriba-sectionBotones">
                <section class="sectionHistorialAdmin-arriba-sectionBotones-sectionIzq">
                    <div class="sectionHistorialAdmin-arriba-sectionBotones-sectionIzq-sectionAction">
                        <div class="sectionHistorialAdmin-arriba-sectionBotones-sectionIzq-sectionAction">
                            <div class="sectionHistorialAdmin-arriba-sectionBotones-sectionIzq-sectionAction-bt">
                                <a class="sectionHistorialAdmin-arriba-sectionBotones-sectionIzq-sectionAction-bt-linkCard" id="buscar" name="buscar" onClick="generarTabla()">Buscar</a>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="sectionHistorialAdmin-arriba-sectionBotones-sectionDer">
                        <button type="submit" class="sectionHistorialAdmin-arriba-sectionBotones-sectionDer-botoon" name="generarInforme" id= "generarInforme" onclick="generarInforme()">Generar Informe</button>
                </section>
            </section>
        </section>
        <section class="sectionHistorialAdmin-cardInfo">
            <div id="TablaInforme" class="sectionHistorialAdmin-cardInfo-mitad">

            </div>
        </section> 
        <section class="sectionHistorialAdmin-buttonBack">
            <div class="sectionHistorialAdmin-buttonBack-botonn">
				<div class="sectionHistorialAdmin-buttonBack-botonn-botoness">
					<a class="sectionHistorialAdmin-buttonBack-botonn-botoness-linkCardd" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/elegirOpcionUsers.php")?>">Volver</a>
				</div>
		    </div>
        </section>
    </div>
    <script type="text/javascript">
    function generarTabla() {
        var ruta ="";
        if($("#fecha1").val()!=""){
                var ruta ="indexAjax.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/historialParqueaderoUsersAjax.php")."&idp=".$_SESSION["id"];?>"+ 
                          "&fecha1=" + $("#fecha1").val() +
                          ($("#fecha").val()==1 ?"&fecha2=" + $("#fecha2").val():"") + 
                          ($("#id").val()!=""?"&id=" + $("#id").val():"");
                $("#TablaInforme").load(ruta);
        }else{
            alert("Seleccione una fecha inicial para generar informes");
        }
    }
    </script>
    <script type="text/javascript">
        function generarInforme() {
        var ruta ="";
        if($("#fecha1").val()!=""){
                var ruta ="indexAjax.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/generarReporteFacturaUser.php");?>"+ 
                          "&fecha1=" + $("#fecha1").val() +
                          ($("#fecha").val()==1 ?"&fecha2=" + $("#fecha2").val():"") + 
                          "&id=" + "&id=<?php echo $_SESSION["id"];?>";
                window.open(ruta, '_blank');
        }else{
            alert("Complete todos los datos para generar el pdf con el");
        }
        
    }
    </script>
    
     <script type="text/javascript">
    function traerFecha2() {
                var ruta ="indexAjax.php?pid=<?php echo base64_encode("presentacion/Parqueadero/historialUsers/traerFecha2UsersAjax.php");?>"+ 
                          "&fecha=" + $("#fecha").val();
            $("#segundaFecha").load(ruta);
       
    }
    </script>
    <script type="text/javascript">
        var today = new Date();
        $("#datepicker").datepicker({ 
            format: 'yyyy-mm-dd',
            startDate: new Date(2019,12,31),
            endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
        });
    </script>

    <script type="text/javascript">
        function date2(){
            if($("#fecha1").val()!=""){
                var jsDate = $('#datepicker').datepicker('getDate');
                $('#datepicker2').datepicker('destroy');
                $("#datepicker2").datepicker({ 
                format: 'yyyy-mm-dd',
                startDate: new Date(jsDate.getFullYear(),jsDate.getMonth(), jsDate.getDate()+1),
                endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
                });
            }else{
                $('#datepicker2').datepicker('destroy');
            }
            
        }
    </script>

</body>

</html>
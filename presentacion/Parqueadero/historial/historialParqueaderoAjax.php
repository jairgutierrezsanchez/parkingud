<?php
    $fecha1= $_GET['fecha1'];
	$fecha2= "";
	if(isset($_GET["fecha2"])){
		$fecha2 = $_GET["fecha2"];
	}
    $codigo = "";
	if(isset($_GET["codigo"])){
		$codigo = $_GET["codigo"];
	}
    $orden = "ASC";
    $historial = new HistorialParqueadero();
    $historiales = $historial ->  buscarLogs($codigo,$fecha1,$fecha2,$orden);
    if(count($historiales)>0){
?>

    <div class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH">
		<table class="table sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax">
			<thead class="table-light sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax"> 
				<tr class="table sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-trAjax">
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-trAjax-fecha">Fecha Ingreso</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-fecha">Hora Ingreso</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-fecha">Fecha Salida</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-fecha">Hora Salida</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-serial">Serial</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-parking">Parqueadero</th>
					<th scope="col" class="sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-theadAjax-name">Propietario</th>

				</tr>
			</thead>
			<tbody class="table sectionHistorialAdmin-cardInfo-mitad-tablaAjaxH-heyAjax-tbAjaxCelador">
				<?php
				
					$t = new Transporte();
					$u = new Usuario();
					$p = new Parqueadero();
					foreach ($historiales as $h) {
						$t->consultarPorId($h->getTransporte());
						$u->consultarNombreYApellido($t->getIdusuario());
						$p -> consultarPorId($h ->getIdParqueadero());
						echo "<tr>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-trAjax-fechaaa'>" . $h ->getFechaIngreso() . "</td>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-fecha'>" . $h ->getHoraIngreso() . "</td>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-fecha'>" . ($h ->getFechaSalida()==""?"No aplica":$h ->getFechaSalida()) . "</td>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-fecha'>" . ($h ->getHoraSalida()==""?"No aplica":$h ->getHoraSalida())  . "</td>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-serial'>" . $t -> getSerial() . "</td>";
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-parking'>" . $p-> getNumero() . "</td>"; 
						echo "<td class='sectionHistorialAdmin-mitad-tablaAjax-heyAjax-name'>" . $u -> getNombre(). "<br>" .$u -> getApellido() . "</td>"; 
						echo "</tr>";
					}
						echo "<caption style='margin-left:15px'>" . count($historiales) . " registros encontrados</caption>"?>
			</tbody>
		</table>
	</div>

    <?php } else { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            No se encontraron resultados
        </div>
    <?php } ?>
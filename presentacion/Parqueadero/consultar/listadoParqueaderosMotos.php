<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}


$atributo = "";
$direccion = "";
$filas = 8;
$pag = 1;
if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}
$parqueadero = new Parqueadero();
$parqueaderos = $parqueadero->consultarTodos($atributo, $direccion, $filas, $pag, 3);
$totalFilas = $parqueadero->consultarTotalFilas(3);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
	<title>Consultar Parqueaderos</title>
</head>
<body>
<h1 class="titulosAdmin">Listado de Parqueaderos de Motocicletas</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
	<div class="containerListadoUsuariosAdmin">
		<div class="containerListadoUsuariosAdmin-consulta">
			<div class="containerListadoUsuariosAdmin-consulta-info">
				<div class="containerListadoUsuariosAdmin-consulta-info-arca">
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-arriba">
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-arriba-cantidad">
							<select class="form-select" id="filas">
								<option value="4" <?php if($filas==4) echo "selected"?>>4</option>
								<option value="8" <?php if($filas==8) echo "selected"?>>8</option>
							</select>
						</div>
					</section>
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-mitad mt-2">
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla">
							<table class="table">
								<thead class="table-light"> 
									<tr class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead">
										<th scope="col" id="thN">#</th>
										<th scope="col" id="thNombreCompleto">Nombre Completo</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead-capacidad">Capacidad</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead-capacidad">Puestos Ocupados Actualmente</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$pos = (isset($_GET["pag"]) && $_GET["pag"]!=1?((($pag-1)*$filas)+1):1);

										foreach ($parqueaderos as $p) {
											echo "<tr>";
											echo "<td id='thN'>" . $pos ++ . "</td>";
											echo "<td id='thNombreCompleto' >" . $p->getNumero() . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead-capacidad'>" . $p->getPuestosMaximos() . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead-capacidad'>" . $p->getPuestosOcupados() . "</td>"; 
											
										}
									echo"<caption id='captionRegistros'> ".count($parqueaderos)." registros encontrados</caption>"?>
								</tbody>
							</table>
						</div>	
					</section>
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-abajo">
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-center">
								<?php 
									$numPags = intval($totalFilas/$filas);
									if($totalFilas%$filas != 0){
										$numPags++;
									}							
									echo ($pag!=1)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Parqueadero/consultar/listadoParqueaderosMotos.php") . "&pag=" . ($pag-1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span aria-hidden='true' style='color:black;'>Anterior</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&laquo;</li></a>";
									for($i=1; $i<=$numPags; $i++){
										echo "<li class='page-item " . (($pag==$i)?"active":"") . "'>" . (($pag!=$i)?"<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Parqueadero/consultar/listadoParqueaderosMotos.php") . "&pag=" . $i . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'>" . $i . "</a>":"<a class='page-link'>" . $i . "</a>") . "</li>";
									}
									echo ($pag!=$numPags)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Parqueadero/consultar/listadoParqueaderosMotos.php") . "&pag=" . ($pag+1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span id='sgte' aria-hidden='true'>Siguiente</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&raquo;</a></li>";
								?>							
							</ul>
						</nav>
					</section>
				</div>
			</div>
		</div>
		<div class="containerListadoUsuariosAdmin-consulta-infoVolver mt-2">
			<div class="containerListadoUsuariosAdmin-consulta-infoVolver-botones mt-1">
				<a class="containerListadoUsuariosAdmin-consulta-infoVolver-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/consultar/consultarTransportesParqueadero.php")  ?>">Volver</a>
			</div>
		</div>
	</div>


<!--SCRIPTS ADICIONALES-->


<script>
$("#filas").change(function() {
	var filas = $("#filas").val(); 
	var url = "index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/consultar/listadoParqueaderosMotos.php") ?>&filas=" + filas;
	<?php if($atributo!="" && $direccion!="") { ?>
	url += "&atributo=<?php echo $atributo ?>&direccion=<?php echo $direccion ?>";	
	<?php } ?>
	location.replace(url);  	
});
</script>
	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
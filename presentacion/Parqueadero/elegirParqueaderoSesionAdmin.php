<?php 
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Consultar Parqueaderos</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerCardMotoBiciAdmin">
    <section class="containerCardMotoBiciAdmin-infoCMB">
        <a id="noSubo" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php")."&idTipo=2"?>">
            <div class="card containerCardMotoBiciAdmin-infoCMB-elegir mr-5">
                <i class="fas fa-bicycle"></i>  
                <div class="card-footer bg-transparent">Parqueadero de Bicicleta</div>
            </div>
        </a>
            
        <a id="noSubo" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php")."&idTipo=3"?>">
            <div class="card containerCardMotoBiciAdmin-infoCMB-elegir ml-5">
                <i class="fas fa-motorcycle"></i>  
                <div class="card-footer bg-transparent">Parqueadero de Moto</div>
            </div>
        </a>
    </section>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php
ob_start();
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
    include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
    include 'presentacion/footer.php';
}

$parqueadero = new Parqueadero($_GET["idParqueadero"]);
$parqueadero -> consultar();

if(isset($_POST["actualizar"])){
    $numero = $_POST["numero"];

    $parqueadero = new Parqueadero($_GET["idParqueadero"], $numero, "", "", "", 2);
    $parqueadero -> actualizarNombreParqueadero();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title></title>
</head>
<body>    

<h1 class="titulosAdmin">Actualizar nombre Parqueadero</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionActualizarAdmin">
        <section class="sectionActualizarAdmin-arriba">
            <div class="sectionActualizarAdmin-arriba-actualizar">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Parqueadero/editarNombreParqueadero.php")."&idParqueadero=".$_GET["idParqueadero"]."&idTipo=2"?> method="post">
                    <section class="sectionActualizarAdmin-arriba-actualizar-sectionForm">
                        <div class="row">
                            <div class="form-group col-md-4 sectionActualizarAdmin-arriba-actualizar-sectionForm-label">
                                <label for="exampleFormControlInput1">Nombre</label>
                                <input type="text" name="numero" class="form-control" id="exampleFormControlInput1" 
                                placeholder="Escribe el nombre" value="<?php echo $parqueadero->getNumero(); ?>" require>
                            </div>
                        </div>
                    </section>
                    <section class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert">
                        <div class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear">
                            <section class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">

                                    <?php if (isset($_POST["actualizar"])) { ?>
                                        <div class="alert alert-success alert-dismissible fade show sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta" 
                                        role="alert">Nnombre Parqueadero Actualizado Éxitosamente.</div>						
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button class="sectionActualizarAdmin-arriba-actualizar-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario" name="actualizar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </div>
        </section>
        <section class="sectionActualizarAdmin-abajo">
            <div class="sectionActualizarAdmin-abajo-cardInfo">
                <div class="sectionActualizarAdmin-abajo-cardInfo-cajitaInfo">
                    <div class="sectionActualizarAdmin-abajo-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionActualizarAdmin-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/consultar/listadoParqueaderosBicis.php")."&idTipo=2"?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
    </div>



 

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
</body>

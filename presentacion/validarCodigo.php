<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
$error = 5;
$codigo = 0;
if(isset($_GET["reenvio"]) && $_GET["reenvio"]= true){
    
     $usuario = new Usuario($_GET["id"]);
     $usuario -> consultar();
      $mail = new PHPMailer(true);
      $codigo = rand(100000,999999);
      try {
          //Server settings
          $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
          $mail->isSMTP();                                            //Send using SMTP
          $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
          $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
          $mail->Username   = 'parqueadero.udistrital@gmail.com';                     //SMTP username
          $mail->Password   = 'gcvtsckqrsvddlsr';                               //SMTP password
          $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
          $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
          //Recipients
          $mail->setFrom('parqueadero.udistrital@gmail.com', 'Codigo Parqueadero Universidad Distrital Francisco José de Caldas');
          $mail->addAddress($usuario -> getCorreo(), $usuario -> getNombre(), $usuario -> getApellido());     //Add a recipient            //Name is optiona
          //Attachments
          //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
          //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

          //Content
          
          $mail->isHTML(true);                                  //Set email format to HTML
          $mail->Subject = 'Administracion Parqueadero UDistrital';
          $mail->CharSet= 'UTF-8';
          $mail->Body    = '<h1>Parqueadero UDistrital</h1><br><p>A continuacion encontrara el codigo que tiene que ingresar en la aplicación del parqueadero: </p><b>'.$codigo.'</b>';
          $mail->AltBody = 'Codigo Verificacion UDistrital';
          $mail->send();
      } catch (Exception $e) {
      }
      $usuario = new Usuario($_GET["id"]);
      $usuario -> actualizarCodigoVerificacion($codigo);
      $error = 2;
      }
              
       

      

if(isset($_POST["validar"])){
    $codigo = $_POST["codigoVerificacion"];
    $usuario = new Usuario($_GET["id"]);
    $usuario -> consultar();
    if($usuario -> getCodigoVerificacion()==$codigo){
        $usuario -> cambiarEstado(2);
        $error=0;
        header("Location: index.php?pid=" . base64_encode("presentacion/login.php"). "&nos=true&error=5");
    }else{
        $error=1;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssRegistro/styleRegistro.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="fondoRegistro">
        <div class="cardFormulario">
            <div class="card">
                <div class="card-title">
                    <h5>Parqueadero Universidad Distrital <br> Francisco José de Caldas</h5>
                </div>
                <div class="card-body">
                    <?php 
                      if($error == 0){
                      ?>
                    <div class="alert alert-success" role="alert">
                        Usuario validado exitosamente.
                    </div>
                    <?php } else if($error == 1) { ?>
                    <div class="alert alert-danger" role="alert">
                        codigo incorrecto
                    </div>
                    <?php }else if($error == 2) { ?>
                    <div class="alert alert-success" role="alert">
                        Codigo enviado nuevamente
                    </div>  
                    <?php } ?>
                    <form class="formlario"
                        action=<?php echo "index.php?pid=" .base64_encode("presentacion/validarCodigo.php")."&nos=true&id=".$_GET["id"] ?>
                        method="post">
                        <div class="mb-3">
                            <input type="number" name="codigoVerificacion" class="form-control" id="codigoVerificacion"
                                onKeyDown="if(this.value.length==6 && event.keyCode!=8) return false;" min="000000"
                                max="999999" placeholder="Ingrese su codigo de verificación">
                        </div>

                        <button type="submit" name="validar" class="boton mb-2">Validar</button>
                        <div class="hipervinculo">
                            <a id="linkVC" href="index.php?pid=<?php echo base64_encode("presentacion/validarCodigo.php")."&nos=true&id=".$_GET["id"] ."&reenvio=true"?>" name="reenviar">Solicitar un nuevo código</a>
                        </div>
                        <div class="hipervinculo">
                            <a href="index.php">Volver al inicio.</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>


</body>

</html>
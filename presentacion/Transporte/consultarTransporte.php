<?php
$usuario= new Usuario($_SESSION['id']);
$usuario->consultar();
include 'presentacion/usuario/menuUsuario.php';
$transporte = new Transporte();
if($_GET["Tipo"]=="Moto"){
	$transportes = $transporte ->  consultarSoloMotos($_SESSION['id']);
}else{
	$transportes = $transporte -> consultarSoloBicis($_SESSION['id']);
}


include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Consultar Usuario</title>
</head>
<body>
<h1 class="titulosUsers"><?php echo "Consultar Transporte ". $_GET["Tipo"];?> </h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionCTUser">
        <section class="sectionCTUser-arriba">
            <div class="sectionCTUser-arriba-cardd">
                <div class="sectionCTUser-arriba-cardd-cajita">
                    <?php 
						foreach ($transportes as $t)
						{
					?>
							<section class="sectionCTUser-arriba-cardd-cajita-pin">
								<div class="sectionCTUser-arriba-cardd-cajita-pin-fotito">
									<img src="<?php echo $t -> getFotoTransporte();?>" class="card-img-top sectionCTUser-arriba-cardd-cajita-pin-fotito-imageTransport" alt="...">
								</div>
							</section>
							<section class="sectionCTUser-arriba-cardd-cajita-link">
								<div class="sectionCTUser-arriba-cardd-cajita-link-boton">
									<a class="sectionCTUser-arriba-cardd-cajita-link-boton-por" id="noSubrayado" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/actualizarFotoTransporte.php")."&idTransporte=".$t-> getIdtransporte()."&Tipo=". $_GET["Tipo"]?>">Actualizar Foto Transporte</a>
								</div>	
							</section>
                    <?php 
						}
					?>
                </div>
                <div class="sectionCTUser-arriba-cardd-cajita">
                    <?php 
						foreach ($transportes as $t)
						{
					?>
                            <section class="sectionCTUser-arriba-cardd-cajita-pin">
								<div class="sectionCTUser-arriba-cardd-cajita-pin-fotito">
									<img src="<?php echo $t -> getFotoCartaPropiedad();?>" class="card-img-top sectionCTUser-arriba-cardd-cajita-pin-fotito-imageTransport" alt="...">
								</div>
							</section>
                            <section class="sectionCTUser-arriba-cardd-cajita-link">
								<div class="sectionCTUser-arriba-cardd-cajita-link-boton">
									<a class="sectionCTUser-arriba-cardd-cajita-link-boton-por" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/actualizarCartaPropiedad.php")."&idTransporte=" . $t -> getIdtransporte() . "&Tipo=". $_GET["Tipo"]?>">Actualizar Foto Carta De Propiedad</a>
								</div>
							</section>
                	<?php 
						}
					?>
				</div>
            </div>
        </section>
        <section class="sectionCTUser-abajo mt-2">
            <div class="sectionCTUser-abajo-cardInfo">
                <section class="sectionCTUser-abajo-cardInfo-arriba">
                    <div class="sectionCTUser-abajo-cardInfo-arriba-espacioParrafo">
                        <p class="sectionCTUser-abajo-cardInfo-arriba-espacioParrafo-parrafo">Manetener actualizados sus datos personales y los datos de sus medios de transporte, nos ayuda a tener una mayor seguridad.</p>
                    </div>
                </section>
                <section class="sectionCTUser-abajo-cardInfo-abajo">
                    <div class="sectionCTUser-abajo-cardInfo-abajo-espacioBoton">
                        <h6 class="sectionCTUser-abajo-cardInfo-abajo-espacioBoton-estadoTranspote">Estado: <?php 
                        if($t-> getEstado() == 0)
                            echo "Activo";
                        else if($t-> getEstado() == 2)
                            echo "Pendiente por activar";
                        else if($t-> getEstado() == 4)
                            echo "Rechazado";
                        ?></h6>
                    </div>
                    <div class="sectionCTUser-abajo-cardInfo-abajo-espacioBoton">
                        <a class="sectionCTUser-abajo-cardInfo-abajo-espacioBoton-linkCard"
                            href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/actualizarTransporte.php")."&tipo=". (($t -> getIdtipo()== 2)?"Bicicleta":"Moto")."&id=". $t -> getIdtransporte()  ?>">Actualizar
                            datos</a>
                    </div>
                </section>
            </div>
        </section>
        <section class="sectionCTUser-botonVolver">
            <div class="sectionCTUser-botonVolver-cardInfo">
                <div class="sectionCTUser-botonVolver-cardInfo-cajitaInfo">
                    <div class="sectionCTUser-botonVolver-cardInfo-cajitaInfo-botones mt-3">
                        <a class="sectionCTUser-botonVolver-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/elegirConsultarTransporte.php")  ?>">Volver</a>
                    </div>
                </div>
            </div>
        </section>
    </div>



    <!--SCRIPTS ADICIONALES-->
    <div class="modal fade" id="modalPaciente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="modalContent">
            </div>
        </div>
    </div>
    <script>
    var modalUsuario = document.getElementById('modalUsuario')
    exampleModal.addEventListener('show.bs.modal', function(event) {
        var button = event.relatedTarget
        var recipient = button.getAttribute('data-bs-whatever')
        var modalTitle = exampleModal.querySelector('.modal-title')
        var modalBodyInput = exampleModal.querySelector('.modal-body input')

        modalTitle.textContent = 'New message to ' + recipient
        modalBodyInput.value = recipient
    })
    </script>


    <script type="text/javascript">
    $(document).ready(function() {
        <?php foreach ($usuarios as $u) { ?>
        $("#cambiarEstado<?php echo $u -> getId(); ?>").click(function(e) {
            e.preventDefault();
            <?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/usuario/editarEstadoUsuarioAjax.php") . "&idusuario=" . $p -> getId() . "&estado=" . (($p -> getEstado() == 0)?"1":"0") . "\";\n"; ?>
            $("#estado<?php echo $p -> getId(); ?>").load(ruta);
        });
        <?php } ?>
    });
    </script>





    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
<?php
    $transporte = new Transporte();
    $transporte -> buscarTransportePendiente($_GET["filtro"],$_GET["idTipo"]);
        
    if( $transporte && $transporte -> getEstado()==2) {
?>
<table class="table parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax mt-3">
    <thead class="table-light">
        <tr>
            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoA">Foto</th>
            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombre">Propietario</th>
            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudente">Código Usuario</th>
            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-estado">Estado</th>
            <th scope="col" class="parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-servicios">Servicios</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $u = new Usuario();
            $u -> consultarNombreYApellido($transporte -> getIdUsuario());
            echo "<tr>";
            echo "<td>" . ( ($u -> getFoto()=="")?"<img class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-fotoAjax'>") . "</td>";
            echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-nombree'>" . $u->getNombre() . " " . $u->getApellido() . "</td>";
            echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-codeStudente'>" . $u->getCodigoEstudiantil() . "</td>"; 
            echo "<td class='parkingBuscarParqueadero-cuerpo-cardInfo-tablaAjax-estadoA'><span  class='fas fa-exclamation-triangle' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Pendiente por Aprobar' ></span>" . "</td>";
            echo "<td class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-services'>" . "<a class='aIconos' href='modalConsultarTransporte.php?idtransporte=" . $transporte->getIdtransporte() . "' data-bs-toggle='modal' data-bs-target='#modalConsultarTransporte' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
                <a id='cambiarEstado" . $transporte->getIdtransporte() . "' class='aIconos' href='index.php?pid=". base64_encode("presentacion/Transporte/pendientes/cambiarEstadoTransporte.php")."&idtransporte=".$transporte -> getIdtransporte()."&idTipo=".$_GET["idTipo"] . "&newEstado=0'> <span class='fas fa-check-circle' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Aprobar' > </span></a>" ."
                <a id='cambiarEstado" . $transporte->getIdtransporte() . "' class='aIconos' href='index.php?pid=". base64_encode("presentacion/Transporte/pendientes/cambiarEstadoTransporte.php")."&idtransporte=".$transporte -> getIdtransporte()."&idTipo=".$_GET["idTipo"] . "&newEstado=4'> <span class='fas fa-times-circle' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Rechazar' > </span></a>" .
                "</td>"; 
            echo "</tr>";
echo "</tr>";?>
    </tbody>
</table>

<?php }else { ?>
<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
    No se encontraron resultados

</div>
<?php } ?>




<!--SCRIPTS ADICIONALES-->

</script>
<div class="modal fade" id="modalConsultarTransporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modalContent">
        </div>
    </div>
</div>
<script>
$('body').on('show.bs.modal', '.modal', function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
</script>
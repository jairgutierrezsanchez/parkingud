<?php 
ob_start();
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
    include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    include "presentacion/menuAdministrador.php";
    include 'presentacion/footer.php';
}
$atributo = "";
$direccion = "";
$filas = 5;
$pag = 1;
if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Bicicletas Pendientes por Aprobar</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="parkingBuscarParqueadero">
        <div class="parkingBuscarParqueadero-cuerpo">
            <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero">
                <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar">
                    <div class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar"> 
                        <div class="form-group parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras">
                            <label class="parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-labelBuscar-controlOpciones">Buscar usuario:</label> 
                            <input name="filtro" id="filtro" type="number" onKeyDown="if(this.value.length==11 && event.keyCode!=8) return false;" 
                                    class="form-control parkingBuscarParqueadero-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-inputt" 
                                    placeholder="Ingrese código estudiantil">
                        </div>
                    </div>
                </div>
            </div>
            <div class="parkingBuscarParqueadero-cuerpo-cardInfo mt-3">
               <div id="resultados"></div>
            </div>
            <div class="parkingBuscarParqueadero-cuerpo-botonn">
				<div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo">
                    <div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness mt-1">
                        <a class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness-linkCardd" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Transporte/pendientes/paginaSeleccionarTrasnporte.php"):base64_encode("presentacion/Transporte/pendientes/paginaSeleccionarTrasnporte.php"))  ?>">Volver</a>
					</div>
                    <div class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness mt-1">
                        <a class="parkingBuscarParqueadero-cuerpo-botonn-cardAbajo-info-botoness-linkCardd" 
                            href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Transporte/pendientes/bicis/listadoBicisPendientes.php"):base64_encode("presentacion/Transporte/pendientes/bicis/listadoBicisPendientes.php"))."&idTipo=".$_GET["idTipo"]  ?>">Listado Pendientes</a> 
                    </div>
                </div>
		    </div>

           
        </div>
    </div>


<!--SCRIPTS ADICIONALES-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#filtro").keyup(function(){
            var tam= $("#filtro").val();
            if(tam.length==11){
                var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/Transporte/pendientes/bicis/bicisPendientesAjax.php")."&idTipo=".$_GET["idTipo"];?>&filtro="+$("#filtro").val();
                $("#resultados").load(ruta);
            }
        });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

<?php
if($_SESSION["rol"]=="celador"){
    $celador = new Celador($_SESSION["id"]);
    $celador -> consultar();
    include "presentacion/celador/menuCelador.php";
	include 'presentacion/footer.php';
}else if($_SESSION["rol"]=="administrador"){
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
	include "presentacion/menuAdministrador.php";
	include 'presentacion/footer.php';
}
$atributo = "";
$direccion = "";
$filas = 25;
$pag = 1;

if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}


$transporte = new Transporte();
$transportes = $transporte->consultarTodos($atributo, $direccion, $filas, $pag,3);
$totalFilas = $transporte->consultarTotalFilas(3);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Consultar Motos</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
	<div class="listadoConsultarCTB">
		<div class="listadoConsultarCTB-consulta">
			<div class="listadoConsultarCTB-consulta-info">
				<div class="listadoConsultarCTB-consulta-info-arca">
					<section class="listadoConsultarCTB-consulta-info-arca-arriba mt-2">
						<div class="listadoConsultarCTB-consulta-info-arca-arriba-cantidad">
							<select class="form-select" id="filas">
								<option value="25" selected <?php if($filas==25) echo "selected"?>>25</option>
								<option value="50" <?php if($filas==50) echo "selected"?>>50</option>
								<option value="75" <?php if($filas==75) echo "selected"?>>75</option>
								<option value="100" <?php if($filas==100) echo "selected"?>>100</option>
							</select>
						</div>
					</section>
					<section class="listadoConsultarCTB-consulta-info-arca-mitad mt-2">
						<div class="listadoConsultarCTB-consulta-info-arca-mitad-tabla">
							<table class="table">
								<thead class="table-light"> 
									<tr>
										<th scope="col" id="strPO">#</th>
										<th scope="col" id="strPO">Foto</th>
										<th scope="col" class="listadoConsultarCTB-consulta-info-arca-mitad-tabla-nombre">Propietario</th>
										<th scope="col" class="listadoConsultarCTB-consulta-info-arca-mitad-tabla-codeStudente">Código Estudiantil</th>
										<th scope="col" class="listadoConsultarCTB-consulta-info-arca-mitad-tabla-estado">Estado</th>
										<th scope="col" class="listadoConsultarCTB-consulta-info-arca-mitad-tabla-services">Servicios</th>
									</tr>
								</thead>
								<tbody> 
									<?php
										$u = new Usuario();
										$pos = 1;
										foreach ($transportes as $t) {
											$u -> consultarNombreYApellido($t -> getIdUsuario());
											echo "<tr>";
											echo "<td>" . $pos ++ . "</td>";
											echo "<td>" . ( ($u -> getFoto()=="")?"<img class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-fotoConsultar' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$u -> getFoto(). " class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-fotoConsultar'>") . "</td>";
											echo "<td class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-nombre'>" . $u->getNombre() . " " . $u->getApellido() . "</td>";
											echo "<td class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-codeStudente'>" . $u->getCodigoEstudiantil() . "</td>";
											echo "<td class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-estado'><span" .$t->getIdtransporte() ."'  class='fas " . ($t->getEstado()==0?"fa-check-circle":(($t->getEstado()==1)?"fa-parking":(($t->getEstado()==2)?"fa-exclamation-triangle":(($t->getEstado()==4)?"fa-times-circle":"")))) . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($t->getEstado()==0?"Disponible":($t->getEstado()==1?"En parqueadero":(($t->getEstado()==2)?"Pendiente por aporbar":(($t->getEstado()==4)?"Rechazado":"")))) . "' ></span>" . "</td>";
												
												
											echo "<td class='listadoConsultarCTB-consulta-info-arca-mitad-tabla-services'>" . "<a class='aIconos' href='modalConsultarTransporte.php?idtransporte=" . $t->getIdtransporte() . "' data-bs-toggle='modal' data-bs-target='#modalConsultarTransporte' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
														<a class='fas fa-pencil-ruler aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarTransporte-admin.php") . "&idtransporte=" . $t->getIdtransporte() ."&idtipo=Moto" .  "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>" .
												"</td>"; 
											echo "</tr>";
										}
											echo "<caption id='captionRegistros'>" . count($transportes) . " registros encontrados</caption>"?>
								</tbody>
							</table>
						</div>
					</section>
					<section class="listadoConsultarCTB-consulta-info-arca-abajo">
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-center">
								<?php 
									$numPags = intval($totalFilas/$filas);
									if($totalFilas%$filas != 0){
										$numPags++;
									}							
									echo ($pag!=1)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Transporte/consultarTodosMotos.php") . "&pag=" . ($pag-1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span aria-hidden='true' style='color:black;'>Anterior</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&laquo;</li></a>";
									for($i=1; $i<=$numPags; $i++){
										echo "<li class='page-item " . (($pag==$i)?"active":"") . "'>" . (($pag!=$i)?"<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Transporte/consultarTodosMotos.php") . "&pag=" . $i . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'>" . $i . "</a>":"<a class='page-link'>" . $i . "</a>") . "</li>";
									}
										echo ($pag!=$numPags)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/Transporte/consultarTodosMotos.php") . "&pag=" . ($pag+1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span aria-hidden='true'>Siguiente</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&raquo;</li></a>";
								?>							
							</ul>
						</nav>
					</section>
				</div>
			</div>
			<div class="listadoConsultarCTB-consulta-infoVolver">
				<div class="listadoConsultarCTB-consulta-infoVolver-botones mt-2">
					<a class="listadoConsultarCTB-consulta-infoVolver-botones-linkCard" href="index.php?pid=<?php echo ($_SESSION["rol"]=="administrador"?base64_encode("presentacion/Administrador/consultarTransportes.php"):base64_encode("presentacion/Administrador/consultarTransportes.php"))  ?>">Volver</a>
				</div>
			</div>
		</div>
	</div>
    

<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalConsultarTransporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
<script>
$("#filas").change(function() {
	var filas = $("#filas").val(); 
	var url = "index.php?pid=<?php echo base64_encode("presentacion/Transporte/consultarTodosMotos.php") ?>&filas=" + filas;
	<?php if($atributo!="" && $direccion!="") { ?>
	url += "&atributo=<?php echo $atributo ?>&direccion=<?php echo $direccion ?>";	
	<?php } ?>
	location.replace(url);  	
});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		<?php foreach ($usuarios as $u) { ?>
		$("#cambiarEstado<?php echo $u -> getId(); ?>").click(function(e){
			e.preventDefault();
			<?php echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/usuario/editarEstadoUsuarioAjax.php") . "&idusuario=" . $p -> getId() . "&estado=" . (($p -> getEstado() == 0)?"1":"0") . "\";\n"; ?>
			$("#estado<?php echo $p -> getId(); ?>").load(ruta);
		});
		<?php } ?>
	});
</script>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
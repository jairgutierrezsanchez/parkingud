<?php 
ob_start();
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$t = new Transporte("","","","","","","","",(($_GET["tipo"]=="Moto")?3:2),$_SESSION["id"]);


include "presentacion/usuario/menuUsuario.php";
include 'presentacion/footer.php';

$error = -1;
$serial = "";
$modelo = "";
$descripcion = "";
$idcolor = "";
$idtipo =  $_GET["tipo"];
$idmarca = "";
   

if(isset($_POST["registrar"])){
    $serial = $_POST["serial"];
    $modelo = $_POST["modelo"];
    $descripcion = $_POST["descripcion"];
    $idcolor = $_POST["idcolor"];
    $idmarca = $_POST["idmarca"];
    $img_vehiculo_nombre = $_FILES["imagenVehiculo"]["name"];    
    $img_vehiculo_tipo = $_FILES["imagenVehiculo"]["type"];
    $img_vehiculo_tam = $_FILES["imagenVehiculo"]["size"];
    $carta_propiedad_nombre = $_FILES["cartaPropiedad"]["name"];
    $carta_propiedad_tipo = $_FILES["cartaPropiedad"]["type"];
    $carta_propiedad_tam = $_FILES["cartaPropiedad"]["size"];
    if($img_vehiculo_tam<= 4000000 && $carta_propiedad_tam<= 4000000){
        if(strlen($img_vehiculo_nombre)<=50  && strlen($img_vehiculo_nombre) <=50 ){    
            if(($img_vehiculo_tipo == "image/jpg" || $img_vehiculo_tipo == "image/jpeg" || $img_vehiculo_tipo == "image/png") &&
                ($carta_propiedad_nombre == "image/jpg" || $carta_propiedad_tipo == "image/jpeg" || $carta_propiedad_tipo == "image/png")){
                    $rutaServidor1 = "imgFotosTransporte/" . date("Ymdhis") . "1.png";
                    $rutaServidor2 = "imgFotosTransporte/" . date("Ymdhis") . "2.png";
                    $rutaLocal1 = $_FILES["imagenVehiculo"]["tmp_name"];
                    $rutaLocal2 = $_FILES["cartaPropiedad"]["tmp_name"];
                    copy($rutaLocal1, $rutaServidor1);
                    copy($rutaLocal2, $rutaServidor2);
                    $transporte = new Transporte("", $serial, $modelo, $rutaServidor1,$rutaServidor2, $descripcion, 2, $idcolor, ($idtipo=='Moto'?3:2), $_SESSION["id"],$idmarca, 1);
                    if(!$transporte -> existeSerial()){
                        $transporte -> registrar();
                        $error = 0;
                        header("Location: index.php?pid=" . base64_encode("presentacion/Transporte/elegirTransporte.php")."&error=".$error);
                    }else{
                        $error = 1;
                    }
            }else{
                $error = 2;
            }
        }else{
            $error = 3;
        }
    }else{
        $error = 4;
    }
   
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Crear Espacio</title>
</head>
<body>
<h1 class="titulosUsers"><?php echo "Crear ". $_GET["tipo"];?></h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="createTransporteUser">
    <section class="createTransporteUser-arriba">
        <div class="createTransporteUser-arriba-crear">
            <section class="createTransporteUser-arriba-crear-sectionCrear">
                <form  action= <?php echo "index.php?pid=" . base64_encode("presentacion/Transporte/crearTransporte.php")."&tipo=" . $_GET["tipo"]?> method="post" enctype="multipart/form-data">
                    <section class="createTransporteUser-arriba-crear-sectionCrear-formCrear">
                        <div class="row mt-2">
                            <div class="form-group col-md-4 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Marca</label>
                                <select id="inputState" class="form-control" name="idmarca">
                                    <?php
                                        $marca = new Marca();
                                        $marcas = (($_GET["tipo"]=="Bicicleta")?$marca -> consultarMotosTodos():$marca -> consultarBicisTodos());
                                        foreach ($marcas as $m) {
                                            echo "<option value='" . $m->getId() . "'>" . $m->getNombre() . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Color</label>
                                <select id="inputState" class="form-control" name="idcolor" placeholder="elija un color">
                                    <?php
                                        $color = new Color();
                                        $colores = $color -> consultarTodos();
                                        foreach ($colores as $c) {
                                            echo "<option value='" . $c->getIdC() . "'>" . $c->getColor() . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState"><?php echo (($_GET["tipo"]=="Moto")?"Placa":"Serial")?></label>
                                <input type="text" name="serial" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el serial" style="text-transform:uppercase;" <?php echo (($_GET["tipo"]=="Moto")?"minlength='6' maxlength='6' size='6'":"minlength='6' maxlength='10' size='10'")?> required = "required">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-6 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Modelo</label>
                                <?php if($_GET["tipo"]=="Bicicleta"){
                                        echo "<input type='text' name='modelo' class='form-control' id='exampleFormControlInput1' placeholder='Escribe el modelo' required = 'required' value ='" . $t -> getModelo() ."'>";
                                    }else{
                                            $year = date("Y");
                                            echo "<select id='inputState' class='form-control' name='modelo'>";
                                            $year = $year+1;
                                            for($i=1995 ; $i<=$year; $i++){
                                                echo "<option value= " . $i . ">" . $i . "</option>";
                                            }
                                            echo "</select>";
                                    }?>
                            </div>
                            <div class="form-group col-md-6 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Descripción</label>
                                <input type="text" name="descripcion" class="form-control" id="exampleFormControlInput1" placeholder="Descripción" required = "required" maxlength="150">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-6 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Foto <?php $_GET["tipo"];?></label>
                                <input type="file" name="imagenVehiculo" class="form-control" id="exampleFormControlInput1" placeholder="Escribe el modelo" required = "required">
                            </div>
                            <div class="form-group col-md-6 createTransporteUser-arriba-crear-sectionCrear-formCrear-label">
                                <label for="inputState">Foto Carta de propiedad</label>
                                <input type="file" name="cartaPropiedad" class="form-control" id="exampleFormControlInput1" placeholder="Descripci�n sobre tu medio" required = "required">
                            </div>
                        </div>
                    </section>
                    <section class="createTransporteUser-arriba-crear-sectionCrear-containerSection">
                        <div class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection">
                            <section class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection-alertSection">
                                <div class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection-alertSection-alert">
                                    <?php 
                                    if(isset($_POST['registrar'])){
                                        if($error == 0){?>
                                            <div class="alert alert-success" role="alert"> Medio de transporte registrado exitosamente.</div>
                                            <?php } else if($error == 1) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <?php echo (($_GET["tipo"]=="Moto")?"La placa ".$serial. " ya existe":"El serial ".$serial. " ya existe")?>
                                                </div>
                                            <?php } else if($error == 2) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <?php echo "El tipo de las fotos solo puede ser png, jpg o jpeg"?>
                                                </div>
                                            <?php }else if($error == 3) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <?php echo "El nombre de alguna foto es muy largo"?>
                                                </div>
                                            <?php }else if($error == 4) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <?php echo "El tamaño de alguna de las imagenes es muy grande TAMAÑO MAX= 4MB"?>
                                                </div>
                                            <?php } 
                                        }?>
                                </div>
                            </section>
                            <section class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection-botonSection">
                                <div class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection-botonSection-boton">
                                    <button class="createTransporteUser-arriba-crear-sectionCrear-containerSection-divSection-botonSection-boton-botonCrearTransporte" type="submit" name="registrar">Registrar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </section>
        </div>
    </section> 
    <section class="createTransporteUser-abajo mt-2">
        <div class="createTransporteUser-abajo-cardAtras">
            <div class="createTransporteUser-abajo-cardAtras-cajitaInfo-boton">
                <a class="createTransporteUser-abajo-cardAtras-cajitaInfo-boton-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/transporte/elegirTransporte.php") ?>">Volver</a>
            </div>
        </div>
    </section>
</div>





    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
</body>
</html>
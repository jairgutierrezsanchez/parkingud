<?php
ob_start();
$exito = "";
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
include 'presentacion/usuario/menuUsuario.php';
include 'presentacion/footer.php';
$transporte = new Transporte($_GET["idTransporte"]);
$transporte -> consultarFotosVehiculo();
$fotoAntigua = $transporte -> getFotoTransporte();
$tipo = $_GET["Tipo"];
if (isset($_POST["actualizar"])) {
    // recibimos los datos de la imagen
    $nombre_foto = $_FILES['foto']['name'];
    $tipo_foto = $_FILES['foto']['type'];
    $tam_foto = $_FILES['foto']['size'];
    if ($tam_foto <= 4000000) {
        if (strlen($nombre_foto) <= 45) {
            if ($tipo_foto == "image/png" || $tipo_foto == "image/jpeg" || $tipo_foto == "image/jpg") {
                if ($usuario->getFoto()!="") {
                    unlink("C:/xampp/htdocs/parking/parkingud/" . $fotoAntigua);
                }
                $rutaServidor = "imgFotosTransporte/" . date("Ymdhis") . "1.png";
                $rutaLocal = $_FILES["foto"]["tmp_name"];
                copy($rutaLocal, $rutaServidor);
                //move_uploaded_file($_FILES['foto']['tmp_name'], $carpeta_destino . $nombre_foto);
                $transporte = new Transporte($_GET["idTransporte"], "", "", $rutaServidor, "", "", 2, "", "", $_SESSION["id"],"", ""); // Crear el atributo foto en usuario
                $transporte->actualizarFotoTransporte();
                ob_clean();
                header("Location: index.php?pid=" . base64_encode("presentacion/Transporte/actualizarFotoTransporte.php")."&idTransporte=". $_GET["idTransporte"]."&Tipo=".$tipo);
            } else {
                $exito = "El tipo de la foto solo puede ser png,jpeg y jpg";
            }
        } else {
            $exito = "El nombre de de la
						foto es muy largo.";
        }
    } else {
        $exito = "El tamano de la
						foto es muy grande.";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--FUENTES-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--FUENTES-->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <title>Actualizar Usuario</title>
</head>
<body>
<h1 class="titulosUsers">Actualizar Foto Transporte</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="cardFotoActualizarUser">
        <div class="cardFotoActualizarUser-cFA">
            <div class="cardFotoActualizarUser-cFA-info">
                <section class="cardFotoActualizarUser-cFA-info-arriba mt-1">
                    <img class="cardFotoActualizarUser-cFA-info-arriba-foto" src="/parking/parkingud/<?php echo $transporte->getFotoTransporte()?>" alt="...">
                </section>
                <section class="cardFotoActualizarUser-cFA-info-abajo">
                    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Transporte/actualizarFotoTransporte.php")."&idTransporte=". $_GET["idTransporte"]."&Tipo=".$tipo ?>
                            method="post" enctype="multipart/form-data">
                        <section class="cardFotoActualizarUser-cFA-info-abajo-formArriba mt-1">
                            <div class="form-group">
                                <input type="file" name="foto" size="30" class="form-control" placeholder="Foto" required="required">
                            </div>
                        </section>
                        <section class="cardFotoActualizarUser-cFA-info-abajo-formAbajo">
                            <section class="cardFotoActualizarUser-cFA-info-abajo-formAbajo-izq">
                                <button class="cardFotoActualizarUser-cFA-info-abajo-formAbajo-izq-btn" type="submit" name="actualizar">Actualizar</button>
                            </section>
                            <section class="cardFotoActualizarUser-cFA-info-abajo-formAbajo-der">
                                <?php if (isset($_POST["actualizar"])) { ?>
                                        <?php if($exito!="") { ?>
                                            <div class="alert alert-danger cardFotoActualizarUser-cFA-info-abajo-formAbajo-der" role="alert"><?php echo $exito ?></div>
                                        <?php }else{?>
                                            <div class="alert alert-success cardFotoActualizarUser-cFA-info-abajo-formAbajo-der-alertAFT" role="alert">foto actualizada</div>
                                        <?php }?>
                                <?php } ?>
                            </section>
                        </section>
                    </form>
                </section>
            </div>
        </div>
        <div class="cardFotoActualizarUser-botonInfo mt-3">
            <div class="cardFotoActualizarUser-botonInfo-bCI">
                <div class="cardFotoActualizarUser-botonInfo-bCI-botones">
                    <a class="cardFotoActualizarUser-botonInfo-bCI-botones-linkCard"href="<?php echo "index.php?pid=".base64_encode("presentacion/Transporte/consultarTransporte.php")."&Tipo=".$tipo?>">Volver</a>
                </div>
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
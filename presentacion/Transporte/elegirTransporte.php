<?php 
$usuario = new Usuario($_SESSION["id"]);
$usuario -> consultar();
$t = new Transporte();

include "presentacion/usuario/menuUsuario.php";
include 'presentacion/footer.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Perfil Usuario</title>
</head>
<body>
<h1 class="titulosAdmin">Por favor, elija el medio de transporte que desea registrar</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerCardMotoBiciAdmin">
    <?php 
        if(isset($_GET['error'])){
            $error= $_GET['error'];
                    if($error == 0){?>
                            <div class="alert alert-success" role="alert"> Medio de transporte registrado exitosamente.</div>
            <?php }
        }?>
    <section class="containerCardMotoBiciAdmin-infoCMB">
        <a id="noSub" href=<?php echo ($t -> consultarExistenciaTransporte($_SESSION["id"],2)==1? "'#' onClick='TransporteCreado()'":"index.php?pid=" . base64_encode("presentacion/transporte/crearTransporte.php")."&tipo=Bicicleta")?>>
            <div class="card containerCardMotoBiciAdmin-infoCMB-elegir mr-5">
                <i class="fas fa-bicycle"></i>  
                <div class="card-footer bg-transparent" >Registrar Bicicleta</div>
            </div>
        </a>
            
        <a id="noSub" href=<?php echo ($t -> consultarExistenciaTransporte($_SESSION["id"],3)==1? "'#' onClick='TransporteCreado()'":"index.php?pid=" . base64_encode("presentacion/transporte/crearTransporte.php")."&tipo=Moto")?>>
            <div class="card containerCardMotoBiciAdmin-infoCMB-elegir ml-5">
                <i class="fas fa-motorcycle"></i>  
                <div class="card-footer bg-transparent">Registrar Moto</div>
            </div>
        </a>
    </section>
</div>
<script>
function TransporteCreado(){
    alert("Ya tienes creado ese tipo de transporte!");
}
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
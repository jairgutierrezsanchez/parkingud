<?php 
$celador = new Celador($_SESSION['id']);
$celador -> consultar();
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./cssBarra/menuAdminStyles.css">
    <!-- Boxiocns CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--FUENTES-->
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--FUENTES-->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

	<title>Sesión Celador</title>
</head>
<body>
    <div class="sidebar close">
        <div class="logo-details">  
            <i class='bx bx-menu'></i>
            <span class="logo_name">UDistrital</span>
        </div>
        <ul class="nav-links">
            <li>
                <a href="index.php?pid=<?php echo base64_encode("presentacion/sesionCelador.php")?>">
                    <i class='bx bx-grid-alt'></i>
                    <span class="link_name">Inicio</span>
                </a>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/pagSeleccionUsuario.php")?>">
                        <i class='bx bx-user'></i>
                        <span class="link_name">Usuarios</span>
                    </a>
                    <i class='bx bxs-chevron-down arrow'></i>
                </div>
                <ul class="sub-menu">
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/usuario/consultarUsuarios.php")?>">Consultar Usuario</a></li>
                </ul>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/Administrador/consultarTransportes.php")?>">
                        <i class='bx bx-car'></i>
                        <span class="link_name">Transporte</span>
                    </a>
                    <i class='bx bxs-chevron-down arrow'></i>
                </div>
                <ul class="sub-menu">
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/consultarTodosBicicletas.php")?>">Consultar Bicicletas</a></li>
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Transporte/consultarTodosMotos.php")?>">Consultar Motocicletas</a></li>
                </ul>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/elegirParqueaderoSesionAdmin.php")?>">
                        <i class='bx bxs-parking'></i>
                        <span class="link_name">Parqueadero</span>
                    </a>
                    <i class='bx bxs-chevron-down arrow'></i>
                </div>
                <ul class="sub-menu">
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php")."&idTipo=2"?>">Parqueadero Bici</a></li>
                    <li><a  href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/elegirParqueaderoSeleccionado.php")."&idTipo=3"?>">Parqueadero Moto</a></li>
                    
                </ul>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/expulsar/elegirParqueaderoExpulsar.php")?>">
                        <i class='bx bx-user-x'></i>
                        <span class="link_name">Expulsar</span>
                    </a>
                    <i class='bx bxs-chevron-down arrow'></i>
                </div>
                <ul class="sub-menu">
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/expulsar/elegirParqueaderoExpulsar.php")."&idTipo=2"?>">Expulsar Bici</a></li>
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/Parqueadero/expulsar/elegirParqueaderoExpulsar.php")."&idTipo=3"?>">Expulsar Moto</a></li>
                    
                </ul>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/otros.php")?>">
                        <i class='bx bxs-collection'></i>
                        <span class="link_name">Otros</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="iocn-link">
                    <a href="index.php?pid=<?php echo base64_encode("presentacion/celador/perfilCelador.php")?>">
                        <i class="fas fa-users-cog"></i>
                        <span class="link_name">Perfil</span>
                    </a>
                    <i class='bx bxs-chevron-down arrow'></i>
                </div>
                <ul class="sub-menu">
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/celador/perfilCelador.php")?>">Ver Perfil</a></li>
                    <li><a href="index.php?pid=<?php echo base64_encode("presentacion/celador/actualizarPassworCelador.php")?>">Actualizar Contraseña</a></li>
                </ul>
            </li>
            <li>
                <div class="profile-details">
                    <div class="profile-content">
                        <img src="./<?php echo ($celador->getFoto()!=null?$celador -> getFoto():"imgFotosPerfil/imagenBase.png")?>" alt="profileImg">
                    </div>
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/celador/perfilCelador.php")?>">
                    <div class="name-job">
                        <div class="profile_name"><?php echo $celador -> getNombre() ?></div>
                            <div class="job">Celador</div>
                        
                    </div>
                    </a>
                    <a href="index.php?pid=<?php echo base64_encode("destruirSesion.php") ?>">
                        <i class='bx bx-log-out'></i></a>
                </div>
            </li>
        </ul>
    </div>



    <script type="text/javascript">
        let arrow = document.querySelectorAll(".arrow");
        for (var i = 0; i < arrow.length; i++) {
        arrow[i].addEventListener("click", (e)=>{
        let arrowParent = e.target.parentElement.parentElement;//selecting main parent of arrow
        arrowParent.classList.toggle("showMenu");
        });
        }

        let sidebar = document.querySelector(".sidebar");
        let sidebarBtn = document.querySelector(".bx-menu");
        console.log(sidebarBtn);
        sidebarBtn.addEventListener("click", ()=>{
        sidebar.classList.toggle("close");
        });
    </script>





<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar(); 


include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';
$atributo = "";
$direccion = "";
$filas = 10;
$pag = 1;
if(isset($_GET["atributo"])){
    $atributo = $_GET["atributo"];
}
if(isset($_GET["direccion"])){
    $direccion = $_GET["direccion"];
}
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}
$celador = new Celador();
$celadores = $celador->consultarTodos( $filas, $pag);
$totalFilas = $celador->consultarTotalFilas();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
	<title>Consultar Celador</title>
</head>
<body>
	<h1 class="titulosAdmin">Listado de Celadores</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
	<div class="containerListadoUsuariosAdmin">
		<div class="containerListadoUsuariosAdmin-consulta">
			<div class="containerListadoUsuariosAdmin-consulta-info">
				<div class="containerListadoUsuariosAdmin-consulta-info-arca">
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-arriba mt-2">
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-arriba-cantidad">
							<select class="form-select" id="filas">
								<option value="10" <?php if($filas==10) echo "selected"?>>10</option>
								<option value="20" <?php if($filas==20) echo "selected"?>>20</option>
							</select>
						</div>
					</section>
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-mitad mt-2">
						<div class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla">
							<table class="table">
								<thead class="table-light"> 
									<tr class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-thead">
										<th scope="col" id="thN">#</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-foto">Foto</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-nombre">Nombre Completo</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-email">Correo</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-codeStudenteA">Número de Identificación</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-estado">Estado</th>
										<th scope="col" class="containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-servicios">Servicios</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$pos = (isset($_GET["pag"]) && $_GET["pag"]!=1?((($pag-1)*$filas)+1):1);

										foreach ($celadores as $c) {
											echo "<tr>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-picA'>" . $pos ++ . "</td>";   						
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-picA'>" . ( ($c -> getFoto()=="")?"<img class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-fotoConsultar' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$c -> getFoto(). " class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-fotoConsultar'>") . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-nombreTSD' >" . $c->getNombre() . ' <br> ' . $c->getApellido() . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-emailA'>" . $c->getCorreo() . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-codeStudenteA'>" . $c->getNumeroID() . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-estadoA'><span id='estado" .$c->getId() ."'  class='fas " . ($c->getEstado()==0?"fa-times-circle":"fa-check-circle") . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($c->getEstado()==0?"Inhabilitado":"Habilitado") . "' ></span>" . "</td>";
											echo "<td class='containerListadoUsuariosAdmin-consulta-info-arca-mitad-tabla-serviciosA'>" . "<a class='aIconos' href='modalCelador.php?idCelador=" . $c->getId() . "' data-bs-toggle='modal' data-bs-target='#modalCelador' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
											<a class='fas fa-pencil-ruler aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarCelador-admin.php") . "&idcelador=" . $c->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>
											<a class='fas fa-wrench aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarPasswordCelador-admin.php") . "&idcelador=" . $c->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar Password'>  </a>"
											.($c -> getEstado()<=1?" <a id='cambiarEstado" . $c->getId() . "' class='aIconos fas " .($c->getEstado()==0? "fa-user-check": "fa-user-times"). "' href='#' data-bs-original-title='".($c->getEstado()==0?"Habilitar":"Deshabilitar"). "' data-toggle='tooltip' data-placement='left'> </a>":"") .
										"</td>";
											echo "</tr>";
										}
									echo"<caption id='captionRegistros'> ".count($celadores)." registros encontrados</caption>"?>
								</tbody>
							</table>
						</div>	
					</section>
					<section class="containerListadoUsuariosAdmin-consulta-info-arca-abajo">
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-center">
								<?php 
									$numPags = intval($totalFilas/$filas);
									if($totalFilas%$filas != 0){
										$numPags++;
									}							
									echo ($pag!=1)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/celador/consultarCelador.php") . "&pag=" . ($pag-1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span aria-hidden='true' style='color:black;'>Anterior</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&laquo;</li></a>";
									for($i=1; $i<=$numPags; $i++){
										echo "<li class='page-item " . (($pag==$i)?"active":"") . "'>" . (($pag!=$i)?"<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/celador/consultarCelador.php") . "&pag=" . $i . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'>" . $i . "</a>":"<a class='page-link'>" . $i . "</a>") . "</li>";
									}
									echo ($pag!=$numPags)?"<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/celador/consultarCelador.php") . "&pag=" . ($pag+1) . "&filas=" . $filas . (($atributo!="" && $direccion!="")?("&atributo=".$atributo."&direccion=".$direccion):"") . "'> <span id='sgte' aria-hidden='true'>Siguiente</span></a></li>" : "<li class='page-item disabled'><a class='page-link'>&raquo;</a></li>";
								?>							
							</ul>
						</nav>
					</section>
				</div>
			</div>
		</div>
		<div class="containerListadoUsuariosAdmin-consulta-infoVolver mt-2">
			<div class="containerListadoUsuariosAdmin-consulta-infoVolver-botones mt-1">
				<a class="containerListadoUsuariosAdmin-consulta-infoVolver-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionCelador.php")  ?>">Volver</a>
			</div>
		</div>
	</div>


<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalCelador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>

<script type="text/javascript">
	<?php 
		foreach ($celadores as $c) {
			if($c -> getEstado() !=2){
				echo "$('#cambiarEstado" . $c -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/celador/editarEstadoCeladorAjax.php") . "&id=" . $c -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $c -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $c -> getId() . "').attr('class') == 'aIconos fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'aIconos fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-bs-original-title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'aIconos fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-bs-original-title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $c -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('data-bs-original-title', 'Habilitado');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('data-bs-original-title', 'Inhabilitado');\n";
				echo "\t}\n";
				echo "});\n";
				
				        
			}
		}
	?>
</script>
<script>
$("#filas").change(function() {
	var filas = $("#filas").val(); 
	var url = "index.php?pid=<?php echo base64_encode("presentacion/celador/consultarCelador.php") ?>&filas=" + filas;
	<?php if($atributo!="" && $direccion!="") { ?>
	url += "&atributo=<?php echo $atributo ?>&direccion=<?php echo $direccion ?>";	
	<?php } ?>
	location.replace(url);  	
});
</script>

	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
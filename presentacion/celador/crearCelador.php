<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';

$error = -1;
$nombre = "";
$apellido = "";
$fechaNacimiento = "";
$correo = "";
$password = "";

if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $fechaNacimiento = $_POST["fechaNacimiento"];
    $correo = $_POST["correo"];
    $password = $_POST["password"];
    $idGenero = $_POST["idGenero"];

    $celador = new Celador("", $nombre, $apellido, $fechaNacimiento, $correo, $password, "", "", "", "", 0, 1, 1, $idGenero);
    if(!$celador -> existeCorreo()){
        $celador -> registrar();
        $error = 0;
    }else{
        $error = 1;
    }
}


?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
 
<body>
    <h1 class="titulosAdmin">Crear Celador</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="sectionDobleCC">
		<section class="sectionDobleCC-arriba">
			<div class="sectionDobleCC-arriba-consultarr">
                <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/celador/crearCelador.php")?> method="post">
                    <section class="sectionDobleCC-arriba-consultarr-sectionFormulario mt-2">
                        <div class="row">
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-label">
                                <label for="inputState">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="exampleFormControlInput1" placeholder="Escriba el nombre del Celador" required>
                            </div>
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-label">
                                <label for="inputState">Apellido</label>
                                <input type="text" name="apellido" class="form-control" placeholder="Escriba el apellido del Celador" required>
                            </div> 
                        </div>
                        <div class="row mt-3">   
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-label">
                                <label  for="inputState">Género</label>
                                <select  class="form-control" name="idGenero" required>
                                  <?php
                                      $genero = new Genero();
                                      $generos = $genero -> consultarTodos();
                                      foreach ($generos as $g) {
                                          echo "<option value='" . $g->getIdGenero() . "'>" . $g->getNombreGenero() . "</option>";
                                      }
                                  ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-fecha">
                                <label for="exampleFormControlInput1">Fecha de Nacimiento</label>
                                <div class="input-group date sectionDobleCC-arriba-consultarr-sectionFormulario-fecha-divFecha" id="datepicker">
                                    <input id="fecha1" type="text"  name="fechaNacimiento" class="form-control" autocomplete="off" required>
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-white d-block">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div> 
                        <div class="row mt-3">   
                            <div class="form-group col-md-12 sectionDobleCC-arriba-consultarr-sectionFormulario-labelCorreo">
                                <label for="inputState">Correo</label>
                                <input type="email" name="correo" class="form-control" placeholder="Escriba el correo del Celador" required>
                            </div>
                        </div> 
                        <div class="row mt-3">   
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-label">
                                <label for="inputState">Contraseña</label>
                                <div class="input-group"> 
                                    <input type="password" id="txtPassword1" name="password" class="form-control" placeholder="Escriba una contraseña" required>
                                    <button id="show_password1" class="btn btn-dark" type="button" onclick="mostrarPassword1()"> 
                                        <span id="Icon1" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                            <div class="form-group col-md-6 sectionDobleCC-arriba-consultarr-sectionFormulario-label">
                                <label for="inputState">Contraseña</label>
                                <div class="input-group"> 
                                    <input type="password" id="txtPassword2" name="comfirmpassword" class="form-control" placeholder="Vuelva a escribir su contraseña" required> 
                                    <button id="show_password2" class="btn btn-dark" type="button" onclick="mostrarPassword2()"> 
                                        <span id="Icon2" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div> 
                    </section>
                    <section class="sectionDobleCC-arriba-consultarr-sectionButtonAlert">
                        <div class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear mt-2">
                            <section class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionAlerta">
                                <div class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert">
                                    <?php if (isset($_POST['registrar'])) { ?>
                                        <div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionAlerta-alert-alerta"
                                            role="alert">
                                            <?php echo ($error==0) ? "Registro exitoso" : $_POST['correo'] . " ya existe"; ?>
                                            </button>
                                        </div>
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionBoton">
                                <div class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito">
                                    <button class="sectionDobleCC-arriba-consultarr-sectionButtonAlert-containerBotonCrear-sectionBoton-botonsito-botonCrearUsuario" name="registrar">Crear</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
		    </div>
		</section>
		<section class="sectionDobleCC-abajo mt-1">
			<div class="sectionDobleCC-abajo-cardInfo">
				<div class="sectionDobleCC-abajo-cardInfo-cajitaInfo-botones mt-3">
					<a class="sectionDobleCC-abajo-cardInfo-cajitaInfo-botones-linkCard" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionCelador.php")  ?>">Volver</a>
				</div>
			</div>
		</section>
    </div> 

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>  
        <script type="text/javascript">
            var today = new Date();
            $("#datepicker").datepicker({ 
                format: 'yyyy-mm-dd',
                startDate: new Date(1940,12,31),
                endDate: new Date(today.getFullYear()-10, today.getMonth(), today.getDate())
            });
        </script>  

<script type="text/javascript">
function mostrarPassword1(){
		var cambio = document.getElementById("txtPassword1");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#Icon1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#Icon1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	function mostrarPassword2(){
		var cambio = document.getElementById("txtPassword2");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#Icon2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#Icon2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
</script>

</body>
</html>
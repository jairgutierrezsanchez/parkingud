<?php 
$celador = new Celador($_SESSION["id"]);
$celador -> consultar();

include 'presentacion/celador/menuCelador.php';
include 'presentacion/footer.php';


$error = 0;
if(isset($_POST['cambiar'])){
    $celador = new Celador($_SESSION['id'], "","","","", $_POST['clavea'], "", "", "", "", "", "", "", "");
    if($_POST['claven']==$_POST['clavenc']){
        if ( $celador -> autenticarclave()){
            $celador = new Celador($_SESSION['id'], "","","","", $_POST['claven'], "", "", "", "", "", "", "", "");
            $celador-> actualizarclave();
        
        }else{
        $error = 2;
        }
    }else{
        $error=1;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Document</title>
</head>
<body> 
<h1 class="titulosUsers">Cambiar Clave</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="actualizarPassUser">
    <section class="actualizarPassUser-arriba">
        <div class="actualizarPassUser-arriba-crearPass">
            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar"> 
                <form  action= <?php echo "index.php?pid=" . base64_encode("presentacion/celador/actualizarPassworCelador.php") ?> method="post" enctype="multipart/form-data">
                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear">
                        <div class="row mt-2">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label mt-1">
                                <label for="inputState">Clave antigua</label>
                                <div class="input-group">    
                                    <input name="clavea" id="txtPassword_0" type="password" class="form-control" placeholder="Digite clave Actual" required="required">
                                    <button id="show_password_0" class="btn btn-dark" type="button" onclick="mostrarPassword_0()"> 
                                        <span id= "eye_0" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label">
                                <label for="inputState">Clave nueva</label>
                                <div class="input-group">
                                    <input name="claven" id="txtPassword_1" type="password" class="form-control" placeholder="Digite clave Nueva" required="required">
                                    <button id="show_password_1" class="btn btn-dark" type="button" onclick="mostrarPassword_1()"> 
                                        <span id= "eye_1" class="fa fa-eye-slash icon"></span> 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="form-group col-md-12 actualizarPassUser-arriba-crearPass-sectionActualizar-formCrear-label">
                                <label for="inputState">Confirmar clave</label>
                                <div class="input-group"> 
                                    <input name="clavenc" id="txtPassword_2" type="password" class="form-control" placeholder="Confirmar clave" required="required">
                                    <button id="show_password_2" class="btn btn-dark" type="button" onclick="mostrarPassword_2()"> 
                                        <span id= "eye_2" class="fa fa-eye-slash icon"></span> 
                                    </button> 
                                </div>
                            </div>
                            
                        </div>
                    </section>

                    <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection">
                        <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection">
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-alertSection-alert">
                                    <?php 
                                        if (isset($_POST['cambiar'])) { 
                                    ?>
                                        <div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show"
                                        role="alert">
                                        <?php if ($error==0) {
                                            echo "Cambio exitoso";
                                        }else if($error == 1){
                                            echo "Las claves no coinciden";
                                        }else{
                                            echo "Clave actual erronea";
                                        }?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </section>
                            <section class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection">
                                <div class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton">
                                    <button class="actualizarPassUser-arriba-crearPass-sectionActualizar-containerSection-divSection-botonSection-boton-botonCrearTransporte" type="submit" name="cambiar">Actualizar</button>
                                </div>
                            </section>
                        </div>
                    </section>
                </form>
            </section>
        </div>
        <div class="actualizarPassUser-arriba-botonBack mt-1">
            <div class="actualizarPassUser-arriba-botonBack-bCI">
                <div class="actualizarPassUser-arriba-botonBack-bCI-botones">
                    <a class="actualizarPassUser-arriba-botonBack-bCI-botones-linkCard"   
                        href="index.php?pid=<?php echo base64_encode("presentacion/celador/perfilCelador.php") ?>">
                        Volver</a>
                </div>
            </div>
        </div>
    </section>
</div>



<script type="text/javascript">
    function mostrarPassword_0(){
		var cambio = document.getElementById("txtPassword_0");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#eye_0').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#eye_0').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
    function mostrarPassword_1(){
		var cambio = document.getElementById("txtPassword_1");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#eye_1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#eye_1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
    function mostrarPassword_2(){
		var cambio = document.getElementById("txtPassword_2");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#eye_2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('#eye_2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
</script>
</body>

</html>
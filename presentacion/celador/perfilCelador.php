<?php
$celador = new Celador($_SESSION["id"]);
$celador -> consultar();
$identificacion = new Identificacion();
$genero = new Genero();

include 'presentacion/celador/menuCelador.php';
include 'presentacion/footer.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosUsuarios/cssUsuarios/stylesUsuarios.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com"> <!--FUENTES-->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> <!--FUENTES-->
	<title>Perfil Celador</title>
</head>
<body>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
<div class="containerPC">
    <div class="containerPC-cardPC">
        <section class="containerPC-cardPC-tituloPC">
            <h1 class="containerPC-cardPC-tituloPC-tituloProfile">Perfil Celador</h1>
        </section>
        <section class="containerPC-cardPC-infoPC">
            <section class="containerPC-cardPC-infoPC-sectionIzq">
                <section class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo">
                    <h5 class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo-nameCelador">
                        <?php echo $celador -> getNombre() . " " . $celador -> getApellido() ?>
                    </h5>
                    <div class="containerPC-cardPC-infoPC-sectionIzq-arribaInfo-infoCelador">
                        <div>
                            <?php echo "Correo: ". $celador -> getCorreo() ?> 
                        </div> 
                        <div>
                            <?php echo "Dirección de su hogar: ". $celador -> getDireccion() ?> 
                        </div>
                        <div>
                            <?php echo "Número de contacto: ". $celador -> getTelefono() ?> 
                        </div>
                        <div>
                            <?php $identificacion -> setId($celador -> getIdTipoIdentificacion()) ?>
                            <?php  $identificacion -> consultar() ?> 
                            <?php echo "Tipo documento: ". $identificacion -> getNombreTipo() ?>
                        </div>
                        <div>
                            <?php echo "Número documento: ". $celador -> getNumeroID() ?>
                        </div>
                        <div>
                            <?php $genero -> setId($celador -> getIdGenero()) ?>
                            <?php  $genero -> consultar() ?> 
                            <?php echo "Género: ". $genero -> getNombreGenero() ?>
                        </div>
                        <div>
                            <?php 
                            $ahora = new DateTime();
                            $nacimiento = new DateTime($celador -> getFechaNacimiento());
                            $edad = $ahora->diff($nacimiento);
                            echo "Edad ". $edad-> y; ?>
                        </div>
                    </div>
                </section>
                <section class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo mt-4">
                    <a class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo-linkCard" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/celador/actualizarcelador.php")?>">Actualizar datos</a>
                    <a class="containerPC-cardPC-infoPC-sectionIzq-abajoInfo-linkCard mt-2" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/celador/actualizarPassworCelador.php")?>">Actualizar Contraseña</a>
                </section>
            </section>
            <section class="containerPC-cardPC-infoPC-sectionDer">
                <section class="containerPC-cardPC-infoPC-sectionDer-fotoVer">
                    <a href="#!" data-bs-toggle="modal" data-bs-target="#modalVerFoto">
                        <img class="containerPC-cardPC-infoPC-sectionDer-fotoVer-fotoVerFoto" 
                                src="./<?php echo ($celador->getFoto()!=null?$celador -> getFoto():"imgFotosPerfil/imagenBase.png")?>"></img>
                    </a> 
                </section>
                <section class="containerPC-cardPC-infoPC-sectionDer-botonFoto">
                    <a class="containerPC-cardPC-infoPC-sectionDer-botonFoto-botonAFC" 
                        href="index.php?pid=<?php echo base64_encode("presentacion/celador/actualizarFotoCelador.php")?>">
                        Actualizar Foto de Perfil
                    </a>
                </section>
            </section>
        </section>
    </div>
</div>

<div tabindex="-1" aria-labelledby="modalVerFoto" aria-hidden="true" class="modal fade" id="modalVerFoto">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-contentt" id="modal-contentt" >
            <img class="containerFotoModal" id="fotoModal" src="/parking/parkingud/<?php echo $celador->getFoto()?>" alt="" >
        </div> 
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>
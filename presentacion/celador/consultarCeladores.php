<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar(); 


include "presentacion/menuAdministrador.php";
include 'presentacion/footer.php';
$atributo = "";
$direccion = "";
$filas = 5;
$pag = 1;
if(isset($_GET["pag"]) && $_GET["pag"]>0){
    $pag = $_GET["pag"];
}
if(isset($_GET["filas"])){
    $filas = $_GET["filas"];
}
$celador = new Celador();
$celadores = $celador->consultarTodos($filas, $pag);
$totalFilas = $celador->consultarTotalFilas();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./estilosAdmin/cssAdmin/stylesAdmin.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Celador</title>
</head>
<body>
<h1 class="titulosAdmin">Consultar Celadores</h1>
<div class="mostrarTextoo">
    <h4 class="mostrarTextoo-titulo"></h4>
</div>
    <div class="containerUsuariosAdmin">
        <div class="containerUsuariosAdmin-cuerpo">
			<div class="containerUsuariosAdmin-cuerpo-cardParqueadero">
                <div class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar">
                    <div class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar">
                        <div class="form-group containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras">
                            <label class="containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-labelBuscar mb-2">Buscar usuarios: </label> 
                            <input name="filtro" id="filtro" type="text" class="form-control containerUsuariosAdmin-cuerpo-cardParqueadero-buscar-inputBuscar-osiriras-inputt" placeholder="Buscar Celador por nombre o apellido">
                        </div>
                    </div>
                </div>
            </div>
            <div class="containerUsuariosAdmin-cuerpo-cardInfo mt-3 mb-3">
				<div class="containerUsuariosAdmin-cuerpo-cardInfo-tabla" id="resultados" ></div>
            </div>
            <div class="containerUsuariosAdmin-cuerpo-botonn">
				<div class="containerUsuariosAdmin-cuerpo-botonn-botoness mt-1">
					<a class="containerUsuariosAdmin-cuerpo-botonn-botoness-linkCardd" href="index.php?pid=<?php echo base64_encode("presentacion/celador/pagSeleccionCelador.php")?>">Volver</a>
					<a class="containerUsuariosAdmin-cuerpo-botonn-botoness-linkCardd" href="index.php?pid=<?php echo base64_encode("presentacion/celador/consultarCelador.php")?>">Listado de Celadores</a> 
				</div>
		    </div>
        </div>
    </div>

<!--SCRIPTS ADICIONALES-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#filtro").keyup(function(){
			var tam=$("#filtro").val().length;
            if(tam>=3){
                var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/celador/consultarCeladoresAjax.php"); ?>&filtro="+$("#filtro").val();
                $("#resultados").load(ruta);
            }
        });
    });
</script>
	


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
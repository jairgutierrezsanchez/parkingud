<?php
    $filtro = $_GET['filtro'];
    $celador = new Celador();
    $celadores = $celador -> buscar($filtro);
    if(count($celadores)>0){
?>

  
		<div class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla">
			<table class="table containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador">
				<thead class="table-dark containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax"> 
					<tr class="table containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador">
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-numero">#</th>
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-foto">Foto</th>
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-nombre">Nombre</th>	
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-email">Correo</th>
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-codeStudente">Número de ID</th>
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-estado">Estado</th>
						<th scope="col" class="containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-theadAjax-trAjaxCelador-services">Servicios</th>
					</tr>
				</thead>
				<tbody class="table containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador">
					<?php
						$pos = (isset($_GET["pag"]) && $_GET["pag"]!=1?((($pag-1)*$filas)+1):1);
						foreach ($celadores as $c) {
							echo "<tr>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-numero'>" . $pos ++ . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-foto'>" . ( ($c -> getFoto()=="")?"<img class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-fotoConsultar' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/" .$c -> getFoto(). " class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-fotoConsultar'>") . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-nombreTH'>" . $c->getNombre() . ' <br> ' . $c->getApellido() . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-emailTH'>" . $c->getCorreo() . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-codeStudenteTH'>" . $c->getNumeroID() . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-estadoTH'><span id='estado" .$c->getId() ."'  class='fas " . ($c->getEstado()==0?"fa-times-circle":"fa-check-circle") . "' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='" . ($c->getEstado()==0?"Inhabilitado":"Habilitado") . "' ></span>" . "</td>";
								echo "<td class='containerUsuariosAdmin-cuerpo-cardInfo-tabla-containerTabla-heyAjaxCelador-tbAjaxCelador-servicesTH'>" . "<a class='aIconos' href='modalCelador.php?idCelador=" . $c->getId() . "' data-bs-toggle='modal' data-bs-target='#modalCelador' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' title='Ver Detalles' > </span> </a>
										<a class='fas fa-pencil-ruler aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarCelador-admin.php") . "&idcelador=" . $c->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar'>  </a>
										<a class='fas fa-wrench aIconos' href='index.php?pid=" . base64_encode("presentacion/Administrador/actualizarPasswordCelador-admin.php") . "&idcelador=" . $c->getId() . "' data-toggle='tooltip' data-placement='left' title='Actualizar Password'>  </a>"
										.($c -> getEstado()<=1?" <a id='cambiarEstado" . $c->getId() . "' class='aIconos fas " .($c->getEstado()==0? "fa-user-check": "fa-user-times"). "' href='#' data-bs-original-title='".($c->getEstado()==0?"Habilitar":"Deshabilitar"). "' data-toggle='tooltip' data-placement='left'> </a>":"") .
									"</td>";
										echo "</tr>";
								}
							echo "<caption style='margin-left:15px'>" . count($celadores) . " registros encontrados</caption>"?>
				</tbody>
			</table>
		</div>

    <?php } else { ?>
        <div class="alert alert-danger alert-dismissible fade show containerUsuariosAdmin-cuerpo-cardInfo-tabla-alertaNoResultados" role="alert">
            No se encontraron resultados
        </div>
    <?php } ?>

<!--SCRIPTS ADICIONALES-->
<div class="modal fade" id="modalCelador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function (e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>

<script type="text/javascript">
	<?php 
		foreach ($celadores as $c) {
			if($c -> getEstado() !=2){
				echo "$('#cambiarEstado" . $c -> getId() . "').click(function() {\n";
				echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/celador/editarEstadoCeladorAjax.php") . "&id=" . $c -> getId() . "';\n";
				echo "\t$('#cambiarEstado" . $c -> getId() . "').load(url);\n";
				echo "\tif($('#cambiarEstado" . $c -> getId() . "').attr('class') == 'aIconos fas fa-user-times'){\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'aIconos fas fa-user-check');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-bs-original-title', 'Habilitar');\n";
				echo "\t}else{\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('class', 'aIconos fas fa-user-times');\n";
				echo "\t\t$('#cambiarEstado" . $c -> getId() . "').attr('data-bs-original-title', 'Desabilitar');\n";
				echo "\t}\n";
				echo "\tif($('#estado" . $c -> getId() . "').attr('class') == 'fas fa-times-circle'){\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-check-circle');\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('data-bs-original-title', 'Habilitado');\n";
				echo "\t}else{\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('class', 'fas fa-times-circle');\n";
				echo "\t\t$('#estado" . $c -> getId() . "').attr('data-bs-original-title', 'Inhabilitado');\n";
				echo "\t}\n";
				echo "});\n";

				
				        
			}
		}
	?>
</script>
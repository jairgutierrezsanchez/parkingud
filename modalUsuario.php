<?php
ob_start();
require_once 'modelo/Persona.php';
require_once 'modelo/Usuario.php';
require_once 'modelo/Proyecto.php';
require_once 'modelo/Identificacion.php';
require_once 'modelo/Genero.php';

$idusuario = $_GET['idUsuario'];
$usuario = new Usuario($idusuario);
$usuario->consultar();
$p = new Proyecto($usuario -> getIdproyecto());
$p -> consultar($usuario -> getIdproyecto());
$i = new Identificacion($usuario -> getIdTipoIdentificacion());
$i -> consultar();
$g = new Genero($usuario -> getIdGenero());
$g -> consultar();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssModal/styleModal.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Usuario</title>
</head>
<body>
	<div class="backgroundModal">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Detalles Estudiante</h5>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">
			<div class="tabla">
				<table class="table">
					<tbody>
						<tr>
							<th width="30%">Nombre</th>
							<td><?php echo $usuario->getNombre() . " " . $usuario->getApellido() ?></td>
						</tr>
						<tr>
							<th width="30%">Foto</th>
							<td><?php echo $usuario -> getFoto()==""?"<img id='fotoUsuarioModal' src=/parking/parkingud/imgFotosPerfil/imagenBase.png>":"<img src=/parking/parkingud/".$usuario -> getFoto(). " id='fotoConsultarUsuariao'>"; ?></td>
						</tr>
						<tr>
							<th width="30%">Correo</th>
							<td><?php echo $usuario->getCorreo() ?></td>
						</tr>
						<tr>
							<th width="30%">Código Estudiantil</th>
							<td><?php echo $usuario->getCodigoEstudiantil() ?></td>
						</tr>
						<tr>
							<th width="30%">Dirección Vivienda</th>
							<td><?php echo $usuario->getDireccion() ?></td>
						</tr>
						<tr>
							<th width="30%">Teléfono de Contacto</th>
							<td><?php echo $usuario->getTelefono() ?></td>
						</tr>
						<tr>
							<th width="30%">Tipo de Identificación</th>
							<td><?php echo $i->getNombreTipo() ?></td>
						</tr>
						<tr>
							<th width="30%">Número ID</th>
							<td><?php echo $usuario->getNumeroID() ?></td>
						</tr>
						<tr>
							<th width="30%">Proyecto al que pertenece</th>
							<td><?php echo $p->getNombre() ?></td>
						</tr>
						<tr>
							<th width="30%">Edad</th>
							<td>
								<div>
									<?php 
									$ahora = new DateTime();
									$nacimiento = new DateTime($usuario -> getFechaNacimiento());
									$edad = $ahora->diff($nacimiento);
									echo $edad-> y . " años" ?>
								</div>
							</td>
						</tr>
						<tr>
							<th width="30%">Género</th>
							<td><?php echo $g->getNombreGenero() ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

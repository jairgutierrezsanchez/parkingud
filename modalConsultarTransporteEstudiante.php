<?php


$usuario= new Usuario($_SESSION['id']);
$usuario->consultar();
include 'presentacion/usuario/menuUsuario.php';

require_once 'modelo/Persona.php';
require_once 'modelo/Usuario.php';
require_once 'modelo/Transporte.php';

$idtransporte = $_GET['idtransporte'];
$transporte = new Transporte($idtransporte);
$transporte->consultarTodos();
$u = new Usuario();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./cssModal/styleModal.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<title>Consultar Transporte</title>
</head>
<body>
	<div class="backgroundModal">
					<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Detalles Transporte</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<div class="tabla">
							<table class="table">
								<tbody>
									<tr>
										<th width="20%">Propietario</th>
										<td><?php echo $u -> getNombre(); ?></td>
									</tr>		
									<tr>
										<th width="20%">Serial</th>
										<td><?php echo $transporte -> getSerial(); ?></td>
									</tr>
                                    <tr>
										<th width="20%">Modelo</th>
										<td><?php echo $transporte -> getModelo(); ?></td>
									</tr>
									<tr>
										<th width="20%">Foto Transporte</th>
										<td><?php echo $transporte -> getFotoTransporte()==""?"<img src=/proyectoparqueadero/img/profile.png>":"<img src=/IPSUD/fotos/".$transporte -> getFotoTransporte(). "/>"; ?></td>
									</tr>
                                    <tr>
										<th width="20%">Foto de la Carta de Propiedad</th>
										<td><?php echo $transporte -> getFotoCartaPropiedad()==""?"<img src=/proyectoparqueadero/img/profile.png>":"<img src=/IPSUD/fotos/".$transporte -> getFotoCartaPropiedad(). "/>"; ?></td>
									</tr>
                                    <tr>
										<th width="20%">Descripcion</th>
										<td><?php echo $transporte -> getDescripcion(); ?></td>
									</tr>
                                    <tr>
										<th width="20%">Color</th>
										<td><?php echo $transporte -> getIdcolor(); ?></td>
									</tr>
                                    <tr>
										<th width="20%">Tipo</th>
										<td><?php echo $transporte -> getIdtipo(); ?></td>
									</tr>
                                    <tr>
										<th width="20%">Marca</th>
										<td><?php echo $transporte -> getMarca(); ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
	</div> 

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>